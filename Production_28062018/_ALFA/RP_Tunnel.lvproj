﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="ALFA_RP_State_test.lvsc" Type="LVStatechart" URL="../StateMachineFpga/ALFA_RP_State_test.lvsc"/>
		<Item Name="CalibToFile.vi" Type="VI" URL="../lib/CalibToFile.vi"/>
		<Item Name="CreateConstant.vi" Type="VI" URL="../CreateConstant.vi"/>
		<Item Name="test3ticks.vi" Type="VI" URL="../lib/test3ticks.vi"/>
		<Item Name="testLog.vi" Type="VI" URL="../lib/testLog.vi"/>
		<Item Name="testLogAlfa.vi" Type="VI" URL="../testLogAlfa.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
			</Item>
			<Item Name="Log Queue.vi" Type="VI" URL="../lib/Log Queue.vi"/>
			<Item Name="LogEvents.vi" Type="VI" URL="../lib/LogEvents.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="RT PXI Target" Type="RT PXI Chassis">
		<Property Name="alias.name" Type="Str">RT PXI Target</Property>
		<Property Name="alias.value" Type="Str">172.18.48.207</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,PharLap;CPU,x86;</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">3</Property>
		<Property Name="host.TargetOSID" Type="UInt">15</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/ALFA_RPCS.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">true</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">Listen 8000

NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
WorkerLimit 10
InactivityTimeout 60

LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule

#
# Pipeline Definition
#

SetConnector netConnector

AddHandler LVAuth
AddHandler LVRFP

AddHandler fileHandler ""

AddOutputFilter chunkFilter


</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="lib" Type="Folder">
			<Item Name="Motor_Drive" Type="Folder">
				<Item Name="Motor_Drive_RP.lvlib" Type="Library" URL="../lib/Motor_Drive_RP/Motor_Drive_RP.lvlib"/>
			</Item>
			<Item Name="subVIs" Type="Folder">
				<Item Name="LogEvents.vi" Type="VI" URL="../lib/LogEvents.vi"/>
				<Item Name="TestPosLim.vi" Type="VI" URL="../lib/TestPosLim.vi"/>
				<Item Name="RPSwitchesCalc.vi" Type="VI" URL="../lib/RPSwitchesCalc.vi"/>
				<Item Name="LogToScreen.vi" Type="VI" URL="../lib/LogToScreen.vi"/>
				<Item Name="Log Queue.vi" Type="VI" URL="../lib/Log Queue.vi"/>
			</Item>
			<Item Name="DIM" Type="Folder">
				<Item Name="DLL" Type="Folder">
					<Item Name="dim.dll" Type="Document" URL="../lib/DIM/DLL/dim.dll"/>
					<Item Name="TOTEM_DIM.dll" Type="Document" URL="../lib/DIM/DLL/TOTEM_DIM.dll"/>
				</Item>
				<Item Name="lib" Type="Folder">
					<Item Name="totem_lib_DIM.lvlib" Type="Library" URL="../lib/DIM/lib/totem_lib_DIM.lvlib"/>
				</Item>
			</Item>
		</Item>
		<Item Name="TypeDef" Type="Folder">
			<Item Name="RPState.ctl" Type="VI" URL="../StateMachineFpga/RPState.ctl"/>
			<Item Name="UpdateCtrl.ctl" Type="VI" URL="../UpdateCtrl.ctl"/>
		</Item>
		<Item Name="Ctrl" Type="Folder">
			<Item Name="2DArray.ctl" Type="VI" URL="../2DArray.ctl"/>
		</Item>
		<Item Name="test" Type="Folder">
			<Item Name="testexe.vi" Type="VI" URL="../lib/Motor_Drive_RP/testexe.vi"/>
		</Item>
		<Item Name="Totem_MotorCtrl_RT.vi" Type="VI" URL="../Totem_MotorCtrl_RT.vi"/>
		<Item Name="Totem_MotorCtrl_RT_testA7L1.vi" Type="VI" URL="../Totem_MotorCtrl_RT_testA7L1.vi"/>
		<Item Name="MotorCtrl M1-4" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{01A9EA42-079F-464F-9ABC-214A3399A0D2}resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=bool{01F5069C-95F5-4316-A746-F70CE63CE2CB}resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=bool{04667980-387C-45BD-BB8A-F0F2F6B4C3D9}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{04C3B8A7-6FA3-4F5B-8A4F-9F0DD3E75505}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{05AD4111-936E-42BC-8054-1923619B1654}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{083B58C6-93FB-4C6B-A0AF-A35CED081656}resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=bool{09762964-5173-4387-A623-1DBDEDDD0700}resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=bool{0B81E91F-B13E-48BB-9E84-FA2CADD98782}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0DF3EE82-9B77-476B-AEFC-6CB0E3C9B93A}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1305B48C-D228-450B-A93F-B2CCED6D4ADF}resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=bool{145E1805-427D-445E-972B-9E9845F196A8}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{162751A1-3675-4F11-9FFB-CC96452D3BF4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{16D9126B-FEED-409B-AFB0-71ACD0CBF33B}resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=bool{1AB94997-24FC-48CE-A70D-4987EACFBE87}resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=bool{1DB78413-AD41-4551-A6AA-F9E1CC6403CA}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{1FD75D55-56B0-43CB-A5EA-6BA1CB21E1BB}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{233561B4-51A7-440A-8281-2E4708295E64}resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=bool{25B51371-BD73-4836-86C4-2DCD3640BCD1}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{2625D640-871C-4AAE-A948-00168F3AFF6E}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{278B2AFA-8A17-4850-8378-5A0A0B80F72D}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2C39CEF1-E2FF-41B3-AF16-84EEF5855E02}resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool{2FDBBD9F-20DE-4E80-AE40-A7D830CBD819}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{30416893-AC8B-4F74-A65F-13D4A4E72A85}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{317C66D1-AE93-4775-BB0F-5B19DC592B75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{3228B482-2B23-4B0B-AEB0-F4F03A6F4BB5}resource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=bool{3436E786-A2CF-48C2-A169-AEAC340F2835}resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=bool{34814E3F-262C-4474-BA3B-6822AFCF34E9}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{38CC7417-5475-4B24-A7D4-0166F9604A79}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{38D5AD57-2CED-4E3D-A0E4-3BE1A27EA38E}resource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=bool{391C79B7-B710-429F-8C10-185D95001B27}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{39DA5525-701B-4848-8D51-6A6E647DDAF5}resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=bool{3AE31B6D-DBDA-4712-A9A8-E3E315E3BA0C}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{3CC9448E-4CA9-4458-A45D-B38DAEB26418}resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8{3E86EB6E-B567-4D12-872B-F49B143DB6A4}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{3F605400-7FC2-4F9D-BF34-FA0B8F1B9B44}resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8{409FBB6D-8BA0-4A0C-AADF-CA5C74D0FDF3}resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=bool{47711F6F-3464-4B4F-A81D-FC4576125CF0}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{4BEEA604-2A0B-4908-A1BE-FBDEF6CCC6CB}resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=bool{4FEC30F2-34CF-4CBE-B574-19225178E99A}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{4FEEBEF6-3393-40E4-B2D3-6180C77F7A43}resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=bool{5223D13C-5C5B-46BE-BD85-7F727026E72C}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{5AFD6454-C837-45B6-BC8F-4C58164518CB}resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=bool{5CD1A24C-A1FD-47F3-80AD-C7EC59991021}resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=bool{5CD67466-E0CA-4DB5-9227-C48BCCDB31B2}resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=bool{5E5C3E9D-9776-4B4E-85E3-094DFA8FADA4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{6104DB75-E948-4D06-BA8C-71845F8B1ADD}resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=bool{613C68C6-EA1C-4444-9ECA-A43A5EDE1806}resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool{62ACA51A-89F3-4332-B326-D25B22FF8B9F}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{649D961E-A40A-4496-B7D2-18972209CAFC}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{661D9D4F-D0A2-4C25-AEA3-74BDC572EE25}resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=bool{69E494B5-25B9-43A3-92CB-2633C376DFC5}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool{713F706D-A746-475B-A898-8F46CE142552}resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=bool{767C2BCB-0209-4C5F-8E68-42C1D261E4F1}resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=bool{76893B1E-2AA0-443C-9E5F-D90F9F398DB9}resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=bool{78C2049A-E184-44FA-A549-70C8C59E0B2D}resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=bool{7BC673D8-0C51-44A8-86F0-60785BD23EF3}resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=bool{7CF52BF0-661F-48ED-8F21-303024035833}resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{7F26C966-9086-4A5A-A55C-CF7469C03915}resource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=bool{8535DDB7-B9CE-4FFC-BA50-34F405767A3C}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{87E3CDB5-CF17-46B5-B450-60977F7BD779}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{8880D294-6932-46FD-A6C4-CFC22BAAF942}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{89A17F87-3639-4708-ABF8-FC7E2A2A2FE3}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{8EB9B1A7-5FE6-454D-8E88-5CD03AC7F2AD}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{90DBE15B-E41B-47D0-B06E-F16ECF1F47C2}resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool{90FE3DD2-CC28-41D9-8E89-642A13B6ECA7}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{954E1C40-1C3C-4AC0-95C3-BE30249F4FB5}resource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=bool{97C1349B-0209-4DB7-8656-70D32BD49B17}resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=bool{987E6BA0-DDB1-4BC4-8E54-0AAAC0CA0C5A}resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=bool{990252B3-0424-44E4-99F8-487ACA783735}resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{A2860DB0-E4E5-4B12-ACB8-CE0729AC125C}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{A2F6DCEE-E9CF-45C4-861F-B1E972D5D026}resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool{A30CE9D6-EEAA-4E1A-B16D-B7F8A1BBC334}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{A6088509-9555-424E-83B0-CD5039D63C82}resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=bool{A6C1522A-18CB-406B-AA65-702A4525B684}resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{AA7A6B99-CA56-4A89-8967-5B486BD4D6F3}resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=bool{ADF6EBE2-21F3-4EFD-BCC6-31D221C0CED9}resource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=bool{ADFBF5CC-4EAE-42AF-97A8-2DE80CD61050}resource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=bool{B5DA1A6B-0FD6-40E8-ABF0-3B1E1005D892}resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=bool{B5DEAC4E-0D3A-459C-B26C-792C63A4E9D8}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{B66A844A-EE1B-4FAD-AD60-2CF807CC3207}resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8{B67B11AE-D69F-4A11-B204-98FDAA624C96}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{B9802DCD-92F5-421A-B53A-7BFA645A5D72}resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=bool{BF1E89DB-C44D-44CB-8A81-79DA39CFF86B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{BFE62D56-CB08-49AE-BD7D-59F705169691}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3228B56-E4CB-44F5-A0E2-424A18384455}resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=bool{C37C08D1-9892-4882-8959-47414C9BBD90}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{C3A72A4A-30BB-4023-9B9E-9DD8D7824921}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{C5AD37CB-0D80-4394-94A3-FEAE523F9C0D}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{C9E6AFAA-D8F8-4A3B-BDA3-FF99871C8647}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{CA918332-B0D4-4ACA-85F4-CD32BB678EC7}resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=bool{CBB77E6D-DEB1-434D-BD29-8E08B31932BC}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{CCADB888-05C5-4586-91C9-40D66DAADB84}resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{CF95A4D7-4CD8-4762-97C6-620444DD705C}resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=bool{D21B9BFA-4782-4257-B185-26D9C86BB643}resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=bool{D4B8D988-EB97-4500-BCA1-712E22379E8B}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{D5CB729E-9D1F-4224-B259-8DCB1BB964DB}resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=bool{D8D16AC1-D001-43FE-960D-4E5A928391EC}resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32{D90DD2E8-D756-4DD6-A288-E4C8913D0F9D}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{DBC41703-4B74-4E66-B5D9-7D771B07C53B}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{DED791D2-A92C-4291-9642-4B9D0C8AC35F}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{DFA514D0-B351-4C25-A8FE-65DA1E13C472}resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8{E2587605-DB69-4671-B573-DBCCEFB9A78D}resource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=bool{E400AA33-A86A-4F23-AF06-502976EAE216}resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=bool{E47BF981-9F0C-4E8A-BA07-9966B1D5650F}resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=bool{E5C18673-06D3-4636-B120-A96E7E18DAC3}resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=bool{ECA52B1D-A49F-44A5-9E28-B86A4D36698E}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{ED63FE57-F3FD-4205-B354-E5FA17FAAA53}resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{EF7119EB-9FC2-4E92-8E59-129799DFE55F}resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=bool{F087738E-D492-4765-B1E2-5F914AEC9D58}resource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=bool{F364E9CE-2DCC-40D5-84C0-6DB86EEA0D1E}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{F68E6BD0-7631-4B48-BB15-64BBA3E6596F}resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=bool{F6950DBB-9DF7-4824-B52B-D6FD301575AD}resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=bool{F8D354D9-9F08-4DED-8831-BF33D2752986}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{F9FEE6DA-521C-4D43-9A92-ADC140DDAD86}resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=bool{FCD802DF-46E2-4257-849E-FA815C3824A6}resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=bool{FE5A6C50-13DF-469D-9A60-E6CC97AEE9DE}resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=bool{FF798525-F7FD-4801-A98F-12AF350F6C7A}resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">12VDC_Fltresource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool220VAC_Drive_Fltresource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool24V_Common_Interlocksresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool24V_Ok_M1resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool24V_Ok_M2resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool24V_Ok_M3resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool24V_Ok_M4resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool24VDC_Fltresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Home_Beam1resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolAll_RP_Home_Beam2resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolAntiColl_M1M2resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=boolAntiColl_M3M4resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=boolBack_Homeresource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolChassis1/Mod1/DI15:8resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8Chassis1/Mod1/DI23:16resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8Chassis1/Mod1/DI30resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=boolChassis1/Mod1/DI31:0resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32Chassis1/Mod1/DI31:24resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8Chassis1/Mod1/DI31resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=boolChassis1/Mod1/DI7:0resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8Chassis1/Mod1[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2/DO15:8resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO16resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO17resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO18resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO19resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO20resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO21resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO22resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO23:16resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO23resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO24resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO25resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO26resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO27resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO28resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO29resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO30resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO31:0resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Chassis1_Mod2/DO31:24resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO31resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO7:0resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M1resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=boolCopy_HomeSw_M2resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=boolCopy_HomeSw_M3resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=boolCopy_HomeSw_M4resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolDirection_M1resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M2resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M3resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M4resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDisable_M1resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolDisable_M2resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolDisable_M3resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolDisable_M4resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolDriver_Flt_M1resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=boolDriver_Flt_M2resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=boolDriver_Flt_M3resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=boolDriver_Flt_M4resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=boolInjection_Inhib_Beam1resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolInjection_Inhib_Beam2resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolM01_LVDT_Fltresource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=boolM01_OutOfLimitsresource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=boolM02_LVDT_Fltresource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=boolM02_OutOfLimitsresource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=boolM03_LVDT_Fltresource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=boolM03_OutOfLimitsresource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=boolM04_LVDT_Fltresource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=boolM04_OutOfLimitsresource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=boolM1_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16M1_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16M1_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16M2_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16M2_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16M2_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16M3_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16M3_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16M3_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16M4_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M4_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16M4_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Overrideresource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPost_Mortemresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M1resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M2resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M3resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M4resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolSafe_Beam1resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSafe_Beam2resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolSpareresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolStep_M1resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M2resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M3resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M4resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M1resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=boolStopper_In_M2resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=boolStopper_In_M3resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=boolStopper_In_M4resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=boolStopper_Out_M1resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=boolStopper_Out_M2resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=boolStopper_Out_M3resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=boolStopper_Out_M4resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=boolSw_In_M1resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=boolSw_In_M2resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolSw_In_M3resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=boolSw_In_M4resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=boolSw_Out_M1resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=boolSw_Out_M2resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=boolSw_Out_M3resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=boolSw_Out_M4resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=boolUser_Permit_Cibu_1aresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_Cibu_1bresource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolUser_Permit_Cibu_2aresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=boolUser_Permit_Cibu_2bresource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool</Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">PXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="Resource Name" Type="Str">RIO0</Property>
			<Property Name="Target Class" Type="Str">PXI-7833R</Property>
			<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="DigitalInputModule1" Type="Folder">
				<Item Name="Driver_Flt_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{16D9126B-FEED-409B-AFB0-71ACD0CBF33B}</Property>
				</Item>
				<Item Name="Driver_Flt_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{EF7119EB-9FC2-4E92-8E59-129799DFE55F}</Property>
				</Item>
				<Item Name="Driver_Flt_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FCD802DF-46E2-4257-849E-FA815C3824A6}</Property>
				</Item>
				<Item Name="Driver_Flt_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E400AA33-A86A-4F23-AF06-502976EAE216}</Property>
				</Item>
				<Item Name="24V_Ok_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2C39CEF1-E2FF-41B3-AF16-84EEF5855E02}</Property>
				</Item>
				<Item Name="24V_Ok_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A2F6DCEE-E9CF-45C4-861F-B1E972D5D026}</Property>
				</Item>
				<Item Name="24V_Ok_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{90DBE15B-E41B-47D0-B06E-F16ECF1F47C2}</Property>
				</Item>
				<Item Name="24V_Ok_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{613C68C6-EA1C-4444-9ECA-A43A5EDE1806}</Property>
				</Item>
				<Item Name="Sw_Out_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{76893B1E-2AA0-443C-9E5F-D90F9F398DB9}</Property>
				</Item>
				<Item Name="Sw_In_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{01A9EA42-079F-464F-9ABC-214A3399A0D2}</Property>
				</Item>
				<Item Name="Stopper_Out_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4FEEBEF6-3393-40E4-B2D3-6180C77F7A43}</Property>
				</Item>
				<Item Name="Stopper_In_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A6088509-9555-424E-83B0-CD5039D63C82}</Property>
				</Item>
				<Item Name="AntiColl_M1M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{09762964-5173-4387-A623-1DBDEDDD0700}</Property>
				</Item>
				<Item Name="Sw_Out_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CA918332-B0D4-4ACA-85F4-CD32BB678EC7}</Property>
				</Item>
				<Item Name="Sw_In_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FF798525-F7FD-4801-A98F-12AF350F6C7A}</Property>
				</Item>
				<Item Name="Stopper_Out_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1305B48C-D228-450B-A93F-B2CCED6D4ADF}</Property>
				</Item>
				<Item Name="Stopper_In_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E47BF981-9F0C-4E8A-BA07-9966B1D5650F}</Property>
				</Item>
				<Item Name="Sw_Out_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{AA7A6B99-CA56-4A89-8967-5B486BD4D6F3}</Property>
				</Item>
				<Item Name="Sw_In_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{233561B4-51A7-440A-8281-2E4708295E64}</Property>
				</Item>
				<Item Name="Stopper_Out_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5CD67466-E0CA-4DB5-9227-C48BCCDB31B2}</Property>
				</Item>
				<Item Name="Stopper_In_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI20</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D5CB729E-9D1F-4224-B259-8DCB1BB964DB}</Property>
				</Item>
				<Item Name="AntiColl_M3M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI21</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C3228B56-E4CB-44F5-A0E2-424A18384455}</Property>
				</Item>
				<Item Name="Sw_Out_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI22</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4BEEA604-2A0B-4908-A1BE-FBDEF6CCC6CB}</Property>
				</Item>
				<Item Name="Sw_In_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI23</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3436E786-A2CF-48C2-A169-AEAC340F2835}</Property>
				</Item>
				<Item Name="Stopper_Out_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D21B9BFA-4782-4257-B185-26D9C86BB643}</Property>
				</Item>
				<Item Name="Stopper_In_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI25</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{083B58C6-93FB-4C6B-A0AF-A35CED081656}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI26</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{661D9D4F-D0A2-4C25-AEA3-74BDC572EE25}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI27</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FE5A6C50-13DF-469D-9A60-E6CC97AEE9DE}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI28</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F6950DBB-9DF7-4824-B52B-D6FD301575AD}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI29</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{01F5069C-95F5-4316-A746-F70CE63CE2CB}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI30" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI30</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F9FEE6DA-521C-4D43-9A92-ADC140DDAD86}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI31" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI31</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E5C18673-06D3-4636-B120-A96E7E18DAC3}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI7:0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B66A844A-EE1B-4FAD-AD60-2CF807CC3207}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI15:8" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DFA514D0-B351-4C25-A8FE-65DA1E13C472}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI23:16" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3F605400-7FC2-4F9D-BF34-FA0B8F1B9B44}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI31:24" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3CC9448E-4CA9-4458-A45D-B38DAEB26418}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI31:0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D8D16AC1-D001-43FE-960D-4E5A928391EC}</Property>
				</Item>
			</Item>
			<Item Name="DigitalInputModule2" Type="Folder">
				<Item Name="24V_Common_Interlocks" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{89A17F87-3639-4708-ABF8-FC7E2A2A2FE3}</Property>
				</Item>
				<Item Name="Device_Allowed" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{62ACA51A-89F3-4332-B326-D25B22FF8B9F}</Property>
				</Item>
				<Item Name="Stable_Beam" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1FD75D55-56B0-43CB-A5EA-6BA1CB21E1BB}</Property>
				</Item>
				<Item Name="Safe_Beam1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{649D961E-A40A-4496-B7D2-18972209CAFC}</Property>
				</Item>
				<Item Name="Safe_Beam2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F364E9CE-2DCC-40D5-84C0-6DB86EEA0D1E}</Property>
				</Item>
				<Item Name="Post_Mortem" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{391C79B7-B710-429F-8C10-185D95001B27}</Property>
				</Item>
				<Item Name="User_Permit_Cibu_1a" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D4B8D988-EB97-4500-BCA1-712E22379E8B}</Property>
				</Item>
				<Item Name="User_Permit_Cibu_1b" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4FEC30F2-34CF-4CBE-B574-19225178E99A}</Property>
				</Item>
				<Item Name="Injection_Inhib_Beam1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{145E1805-427D-445E-972B-9E9845F196A8}</Property>
				</Item>
				<Item Name="Injection_Inhib_Beam2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C5AD37CB-0D80-4394-94A3-FEAE523F9C0D}</Property>
				</Item>
				<Item Name="User_Permit_Cibu_2a" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A30CE9D6-EEAA-4E1A-B16D-B7F8A1BBC334}</Property>
				</Item>
				<Item Name="User_Permit_Cibu_2b" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8EB9B1A7-5FE6-454D-8E88-5CD03AC7F2AD}</Property>
				</Item>
				<Item Name="All_RP_Home_Beam1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5223D13C-5C5B-46BE-BD85-7F727026E72C}</Property>
				</Item>
				<Item Name="All_RP_Home_Beam2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{04667980-387C-45BD-BB8A-F0F2F6B4C3D9}</Property>
				</Item>
				<Item Name="Back_Home" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A2860DB0-E4E5-4B12-ACB8-CE0729AC125C}</Property>
				</Item>
				<Item Name="Override" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3E86EB6E-B567-4D12-872B-F49B143DB6A4}</Property>
				</Item>
				<Item Name="Spare" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D90DD2E8-D756-4DD6-A288-E4C8913D0F9D}</Property>
				</Item>
				<Item Name="24VDC_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{69E494B5-25B9-43A3-92CB-2633C376DFC5}</Property>
				</Item>
				<Item Name="12VDC_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{87E3CDB5-CF17-46B5-B450-60977F7BD779}</Property>
				</Item>
				<Item Name="220VAC_Drive_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8535DDB7-B9CE-4FFC-BA50-34F405767A3C}</Property>
				</Item>
				<Item Name="M01_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3228B482-2B23-4B0B-AEB0-F4F03A6F4BB5}</Property>
				</Item>
				<Item Name="M01_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI25</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{954E1C40-1C3C-4AC0-95C3-BE30249F4FB5}</Property>
				</Item>
				<Item Name="M02_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI26</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{ADFBF5CC-4EAE-42AF-97A8-2DE80CD61050}</Property>
				</Item>
				<Item Name="M02_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI27</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F087738E-D492-4765-B1E2-5F914AEC9D58}</Property>
				</Item>
				<Item Name="M03_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI28</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{ADF6EBE2-21F3-4EFD-BCC6-31D221C0CED9}</Property>
				</Item>
				<Item Name="M03_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI29</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{38D5AD57-2CED-4E3D-A0E4-3BE1A27EA38E}</Property>
				</Item>
				<Item Name="M04_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI30</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7F26C966-9086-4A5A-A55C-CF7469C03915}</Property>
				</Item>
				<Item Name="M04_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI31</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E2587605-DB69-4671-B573-DBCCEFB9A78D}</Property>
				</Item>
			</Item>
			<Item Name="DigitalOutput" Type="Folder">
				<Item Name="Step_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{ECA52B1D-A49F-44A5-9E28-B86A4D36698E}</Property>
				</Item>
				<Item Name="Direction_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B67B11AE-D69F-4A11-B204-98FDAA624C96}</Property>
				</Item>
				<Item Name="Disable_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{25B51371-BD73-4836-86C4-2DCD3640BCD1}</Property>
				</Item>
				<Item Name="Reset_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0B81E91F-B13E-48BB-9E84-FA2CADD98782}</Property>
				</Item>
				<Item Name="Step_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{278B2AFA-8A17-4850-8378-5A0A0B80F72D}</Property>
				</Item>
				<Item Name="Direction_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0DF3EE82-9B77-476B-AEFC-6CB0E3C9B93A}</Property>
				</Item>
				<Item Name="Disable_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B5DEAC4E-0D3A-459C-B26C-792C63A4E9D8}</Property>
				</Item>
				<Item Name="Reset_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2625D640-871C-4AAE-A948-00168F3AFF6E}</Property>
				</Item>
				<Item Name="Step_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DED791D2-A92C-4291-9642-4B9D0C8AC35F}</Property>
				</Item>
				<Item Name="Direction_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{04C3B8A7-6FA3-4F5B-8A4F-9F0DD3E75505}</Property>
				</Item>
				<Item Name="Disable_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{30416893-AC8B-4F74-A65F-13D4A4E72A85}</Property>
				</Item>
				<Item Name="Reset_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C3A72A4A-30BB-4023-9B9E-9DD8D7824921}</Property>
				</Item>
				<Item Name="Step_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DBC41703-4B74-4E66-B5D9-7D771B07C53B}</Property>
				</Item>
				<Item Name="Direction_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8880D294-6932-46FD-A6C4-CFC22BAAF942}</Property>
				</Item>
				<Item Name="Disable_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{47711F6F-3464-4B4F-A81D-FC4576125CF0}</Property>
				</Item>
				<Item Name="Reset_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2FDBBD9F-20DE-4E80-AE40-A7D830CBD819}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO16" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5AFD6454-C837-45B6-BC8F-4C58164518CB}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO17" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B5DA1A6B-0FD6-40E8-ABF0-3B1E1005D892}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO18" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B9802DCD-92F5-421A-B53A-7BFA645A5D72}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO19" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1AB94997-24FC-48CE-A70D-4987EACFBE87}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO20" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO20</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{767C2BCB-0209-4C5F-8E68-42C1D261E4F1}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO21" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO21</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{713F706D-A746-475B-A898-8F46CE142552}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO22" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO22</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{97C1349B-0209-4DB7-8656-70D32BD49B17}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO23" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO23</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7BC673D8-0C51-44A8-86F0-60785BD23EF3}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO24" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5CD1A24C-A1FD-47F3-80AD-C7EC59991021}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO25" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO25</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{987E6BA0-DDB1-4BC4-8E54-0AAAC0CA0C5A}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO26" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO26</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CF95A4D7-4CD8-4762-97C6-620444DD705C}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO27" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO27</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{39DA5525-701B-4848-8D51-6A6E647DDAF5}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO28" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO28</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F68E6BD0-7631-4B48-BB15-64BBA3E6596F}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO29" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO29</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{409FBB6D-8BA0-4A0C-AADF-CA5C74D0FDF3}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO30" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO30</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{78C2049A-E184-44FA-A549-70C8C59E0B2D}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO31" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO31</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6104DB75-E948-4D06-BA8C-71845F8B1ADD}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO7:0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CCADB888-05C5-4586-91C9-40D66DAADB84}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO15:8" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{990252B3-0424-44E4-99F8-487ACA783735}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO23:16" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7CF52BF0-661F-48ED-8F21-303024035833}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO31:24" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{ED63FE57-F3FD-4205-B354-E5FA17FAAA53}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO31:0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A6C1522A-18CB-406B-AA65-702A4525B684}</Property>
				</Item>
			</Item>
			<Item Name="ResolverInOut" Type="Folder">
				<Item Name="M1_Resolver_Sin" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{162751A1-3675-4F11-9FFB-CC96452D3BF4}</Property>
				</Item>
				<Item Name="M1_Resolver_Cos" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C9E6AFAA-D8F8-4A3B-BDA3-FF99871C8647}</Property>
				</Item>
				<Item Name="M2_Resolver_Sin" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C37C08D1-9892-4882-8959-47414C9BBD90}</Property>
				</Item>
				<Item Name="M2_Resolver_Cos" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F8D354D9-9F08-4DED-8831-BF33D2752986}</Property>
				</Item>
				<Item Name="M3_Resolver_Sin" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5E5C3E9D-9776-4B4E-85E3-094DFA8FADA4}</Property>
				</Item>
				<Item Name="M3_Resolver_Cos" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{34814E3F-262C-4474-BA3B-6822AFCF34E9}</Property>
				</Item>
				<Item Name="M4_Resolver_Sin" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CBB77E6D-DEB1-434D-BD29-8E08B31932BC}</Property>
				</Item>
				<Item Name="M4_Resolver_Cos" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{38CC7417-5475-4B24-A7D4-0166F9604A79}</Property>
				</Item>
				<Item Name="M1_Resolver_Exc" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3AE31B6D-DBDA-4712-A9A8-E3E315E3BA0C}</Property>
				</Item>
				<Item Name="M2_Resolver_Exc" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{90FE3DD2-CC28-41D9-8E89-642A13B6ECA7}</Property>
				</Item>
				<Item Name="M3_Resolver_Exc" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BF1E89DB-C44D-44CB-8A81-79DA39CFF86B}</Property>
				</Item>
				<Item Name="M4_Resolver_Exc" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1DB78413-AD41-4551-A6AA-F9E1CC6403CA}</Property>
				</Item>
			</Item>
			<Item Name="RpStateMachine" Type="Folder">
				<Item Name="M1_RPVTop_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M1_RPVTop_State.lvsc"/>
				<Item Name="M2_RPVBot_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M2_RPVBot_State.lvsc"/>
				<Item Name="M3_RPVTop_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M3_RPVTop_State.lvsc"/>
				<Item Name="M4_RPVBot_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M4_RPVBot_State.lvsc"/>
				<Item Name="M01_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M01_ALFA_RP_State.lvsc"/>
				<Item Name="M02_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M02_ALFA_RP_State.lvsc"/>
				<Item Name="M03_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M03_ALFA_RP_State.lvsc"/>
				<Item Name="M04_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M04_ALFA_RP_State.lvsc"/>
			</Item>
			<Item Name="Chassis1" Type="RIO Expansion Chassis">
				<Property Name="crio.Location" Type="Str">Connector 1</Property>
				<Item Name="Chassis1/Mod1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">0</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9425</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}</Property>
				</Item>
				<Item Name="Chassis1_Mod2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9477</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{317C66D1-AE93-4775-BB0F-5B19DC592B75}</Property>
				</Item>
				<Item Name="Chassis1_Mod3" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 3</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9425</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BFE62D56-CB08-49AE-BD7D-59F705169691}</Property>
				</Item>
			</Item>
			<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{05AD4111-936E-42BC-8054-1923619B1654}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="fpga_MotorControl_Totem_7833R.vi" Type="VI" URL="../fpga_MotorControl_Totem_7833R.vi">
				<Property Name="BuildSpec" Type="Str">{0727F1EB-A94E-4AC1-A84A-72067E22DF5D}</Property>
				<Property Name="configString.guid" Type="Str">{01A9EA42-079F-464F-9ABC-214A3399A0D2}resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=bool{01F5069C-95F5-4316-A746-F70CE63CE2CB}resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=bool{04667980-387C-45BD-BB8A-F0F2F6B4C3D9}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{04C3B8A7-6FA3-4F5B-8A4F-9F0DD3E75505}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{05AD4111-936E-42BC-8054-1923619B1654}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{083B58C6-93FB-4C6B-A0AF-A35CED081656}resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=bool{09762964-5173-4387-A623-1DBDEDDD0700}resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=bool{0B81E91F-B13E-48BB-9E84-FA2CADD98782}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0DF3EE82-9B77-476B-AEFC-6CB0E3C9B93A}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1305B48C-D228-450B-A93F-B2CCED6D4ADF}resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=bool{145E1805-427D-445E-972B-9E9845F196A8}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{162751A1-3675-4F11-9FFB-CC96452D3BF4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{16D9126B-FEED-409B-AFB0-71ACD0CBF33B}resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=bool{1AB94997-24FC-48CE-A70D-4987EACFBE87}resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=bool{1DB78413-AD41-4551-A6AA-F9E1CC6403CA}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{1FD75D55-56B0-43CB-A5EA-6BA1CB21E1BB}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{233561B4-51A7-440A-8281-2E4708295E64}resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=bool{25B51371-BD73-4836-86C4-2DCD3640BCD1}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{2625D640-871C-4AAE-A948-00168F3AFF6E}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{278B2AFA-8A17-4850-8378-5A0A0B80F72D}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2C39CEF1-E2FF-41B3-AF16-84EEF5855E02}resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool{2FDBBD9F-20DE-4E80-AE40-A7D830CBD819}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{30416893-AC8B-4F74-A65F-13D4A4E72A85}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{317C66D1-AE93-4775-BB0F-5B19DC592B75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{3228B482-2B23-4B0B-AEB0-F4F03A6F4BB5}resource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=bool{3436E786-A2CF-48C2-A169-AEAC340F2835}resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=bool{34814E3F-262C-4474-BA3B-6822AFCF34E9}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{38CC7417-5475-4B24-A7D4-0166F9604A79}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{38D5AD57-2CED-4E3D-A0E4-3BE1A27EA38E}resource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=bool{391C79B7-B710-429F-8C10-185D95001B27}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{39DA5525-701B-4848-8D51-6A6E647DDAF5}resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=bool{3AE31B6D-DBDA-4712-A9A8-E3E315E3BA0C}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{3CC9448E-4CA9-4458-A45D-B38DAEB26418}resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8{3E86EB6E-B567-4D12-872B-F49B143DB6A4}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{3F605400-7FC2-4F9D-BF34-FA0B8F1B9B44}resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8{409FBB6D-8BA0-4A0C-AADF-CA5C74D0FDF3}resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=bool{47711F6F-3464-4B4F-A81D-FC4576125CF0}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{4BEEA604-2A0B-4908-A1BE-FBDEF6CCC6CB}resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=bool{4FEC30F2-34CF-4CBE-B574-19225178E99A}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{4FEEBEF6-3393-40E4-B2D3-6180C77F7A43}resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=bool{5223D13C-5C5B-46BE-BD85-7F727026E72C}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{5AFD6454-C837-45B6-BC8F-4C58164518CB}resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=bool{5CD1A24C-A1FD-47F3-80AD-C7EC59991021}resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=bool{5CD67466-E0CA-4DB5-9227-C48BCCDB31B2}resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=bool{5E5C3E9D-9776-4B4E-85E3-094DFA8FADA4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{6104DB75-E948-4D06-BA8C-71845F8B1ADD}resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=bool{613C68C6-EA1C-4444-9ECA-A43A5EDE1806}resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool{62ACA51A-89F3-4332-B326-D25B22FF8B9F}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{649D961E-A40A-4496-B7D2-18972209CAFC}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{661D9D4F-D0A2-4C25-AEA3-74BDC572EE25}resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=bool{69E494B5-25B9-43A3-92CB-2633C376DFC5}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool{713F706D-A746-475B-A898-8F46CE142552}resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=bool{767C2BCB-0209-4C5F-8E68-42C1D261E4F1}resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=bool{76893B1E-2AA0-443C-9E5F-D90F9F398DB9}resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=bool{78C2049A-E184-44FA-A549-70C8C59E0B2D}resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=bool{7BC673D8-0C51-44A8-86F0-60785BD23EF3}resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=bool{7CF52BF0-661F-48ED-8F21-303024035833}resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{7F26C966-9086-4A5A-A55C-CF7469C03915}resource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=bool{8535DDB7-B9CE-4FFC-BA50-34F405767A3C}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{87E3CDB5-CF17-46B5-B450-60977F7BD779}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{8880D294-6932-46FD-A6C4-CFC22BAAF942}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{89A17F87-3639-4708-ABF8-FC7E2A2A2FE3}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{8EB9B1A7-5FE6-454D-8E88-5CD03AC7F2AD}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{90DBE15B-E41B-47D0-B06E-F16ECF1F47C2}resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool{90FE3DD2-CC28-41D9-8E89-642A13B6ECA7}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{954E1C40-1C3C-4AC0-95C3-BE30249F4FB5}resource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=bool{97C1349B-0209-4DB7-8656-70D32BD49B17}resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=bool{987E6BA0-DDB1-4BC4-8E54-0AAAC0CA0C5A}resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=bool{990252B3-0424-44E4-99F8-487ACA783735}resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{A2860DB0-E4E5-4B12-ACB8-CE0729AC125C}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{A2F6DCEE-E9CF-45C4-861F-B1E972D5D026}resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool{A30CE9D6-EEAA-4E1A-B16D-B7F8A1BBC334}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{A6088509-9555-424E-83B0-CD5039D63C82}resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=bool{A6C1522A-18CB-406B-AA65-702A4525B684}resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{AA7A6B99-CA56-4A89-8967-5B486BD4D6F3}resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=bool{ADF6EBE2-21F3-4EFD-BCC6-31D221C0CED9}resource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=bool{ADFBF5CC-4EAE-42AF-97A8-2DE80CD61050}resource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=bool{B5DA1A6B-0FD6-40E8-ABF0-3B1E1005D892}resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=bool{B5DEAC4E-0D3A-459C-B26C-792C63A4E9D8}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{B66A844A-EE1B-4FAD-AD60-2CF807CC3207}resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8{B67B11AE-D69F-4A11-B204-98FDAA624C96}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{B9802DCD-92F5-421A-B53A-7BFA645A5D72}resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=bool{BF1E89DB-C44D-44CB-8A81-79DA39CFF86B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{BFE62D56-CB08-49AE-BD7D-59F705169691}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3228B56-E4CB-44F5-A0E2-424A18384455}resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=bool{C37C08D1-9892-4882-8959-47414C9BBD90}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{C3A72A4A-30BB-4023-9B9E-9DD8D7824921}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{C5AD37CB-0D80-4394-94A3-FEAE523F9C0D}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{C9E6AFAA-D8F8-4A3B-BDA3-FF99871C8647}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{CA918332-B0D4-4ACA-85F4-CD32BB678EC7}resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=bool{CBB77E6D-DEB1-434D-BD29-8E08B31932BC}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{CCADB888-05C5-4586-91C9-40D66DAADB84}resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{CF95A4D7-4CD8-4762-97C6-620444DD705C}resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=bool{D21B9BFA-4782-4257-B185-26D9C86BB643}resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=bool{D4B8D988-EB97-4500-BCA1-712E22379E8B}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{D5CB729E-9D1F-4224-B259-8DCB1BB964DB}resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=bool{D8D16AC1-D001-43FE-960D-4E5A928391EC}resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32{D90DD2E8-D756-4DD6-A288-E4C8913D0F9D}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{DBC41703-4B74-4E66-B5D9-7D771B07C53B}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{DED791D2-A92C-4291-9642-4B9D0C8AC35F}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{DFA514D0-B351-4C25-A8FE-65DA1E13C472}resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8{E2587605-DB69-4671-B573-DBCCEFB9A78D}resource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=bool{E400AA33-A86A-4F23-AF06-502976EAE216}resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=bool{E47BF981-9F0C-4E8A-BA07-9966B1D5650F}resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=bool{E5C18673-06D3-4636-B120-A96E7E18DAC3}resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=bool{ECA52B1D-A49F-44A5-9E28-B86A4D36698E}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{ED63FE57-F3FD-4205-B354-E5FA17FAAA53}resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{EF7119EB-9FC2-4E92-8E59-129799DFE55F}resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=bool{F087738E-D492-4765-B1E2-5F914AEC9D58}resource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=bool{F364E9CE-2DCC-40D5-84C0-6DB86EEA0D1E}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{F68E6BD0-7631-4B48-BB15-64BBA3E6596F}resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=bool{F6950DBB-9DF7-4824-B52B-D6FD301575AD}resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=bool{F8D354D9-9F08-4DED-8831-BF33D2752986}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{F9FEE6DA-521C-4D43-9A92-ADC140DDAD86}resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=bool{FCD802DF-46E2-4257-849E-FA815C3824A6}resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=bool{FE5A6C50-13DF-469D-9A60-E6CC97AEE9DE}resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=bool{FF798525-F7FD-4801-A98F-12AF350F6C7A}resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">12VDC_Fltresource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool220VAC_Drive_Fltresource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool24V_Common_Interlocksresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool24V_Ok_M1resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool24V_Ok_M2resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool24V_Ok_M3resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool24V_Ok_M4resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool24VDC_Fltresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Home_Beam1resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolAll_RP_Home_Beam2resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolAntiColl_M1M2resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=boolAntiColl_M3M4resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=boolBack_Homeresource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolChassis1/Mod1/DI15:8resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8Chassis1/Mod1/DI23:16resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8Chassis1/Mod1/DI30resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=boolChassis1/Mod1/DI31:0resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32Chassis1/Mod1/DI31:24resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8Chassis1/Mod1/DI31resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=boolChassis1/Mod1/DI7:0resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8Chassis1/Mod1[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2/DO15:8resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO16resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO17resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO18resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO19resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO20resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO21resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO22resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO23:16resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO23resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO24resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO25resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO26resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO27resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO28resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO29resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO30resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO31:0resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Chassis1_Mod2/DO31:24resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO31resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO7:0resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M1resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=boolCopy_HomeSw_M2resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=boolCopy_HomeSw_M3resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=boolCopy_HomeSw_M4resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolDirection_M1resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M2resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M3resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M4resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDisable_M1resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolDisable_M2resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolDisable_M3resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolDisable_M4resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolDriver_Flt_M1resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=boolDriver_Flt_M2resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=boolDriver_Flt_M3resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=boolDriver_Flt_M4resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=boolInjection_Inhib_Beam1resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolInjection_Inhib_Beam2resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolM01_LVDT_Fltresource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=boolM01_OutOfLimitsresource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=boolM02_LVDT_Fltresource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=boolM02_OutOfLimitsresource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=boolM03_LVDT_Fltresource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=boolM03_OutOfLimitsresource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=boolM04_LVDT_Fltresource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=boolM04_OutOfLimitsresource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=boolM1_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16M1_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16M1_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16M2_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16M2_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16M2_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16M3_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16M3_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16M3_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16M4_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M4_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16M4_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Overrideresource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPost_Mortemresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M1resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M2resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M3resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M4resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolSafe_Beam1resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSafe_Beam2resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolSpareresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolStep_M1resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M2resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M3resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M4resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M1resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=boolStopper_In_M2resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=boolStopper_In_M3resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=boolStopper_In_M4resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=boolStopper_Out_M1resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=boolStopper_Out_M2resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=boolStopper_Out_M3resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=boolStopper_Out_M4resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=boolSw_In_M1resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=boolSw_In_M2resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolSw_In_M3resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=boolSw_In_M4resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=boolSw_Out_M1resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=boolSw_Out_M2resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=boolSw_Out_M3resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=boolSw_Out_M4resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=boolUser_Permit_Cibu_1aresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_Cibu_1bresource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolUser_Permit_Cibu_2aresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=boolUser_Permit_Cibu_2bresource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_ALFA\FPGA Bitfiles\RPTunnel_MotorCtrlM1-4_fpgaMotorControl_B-dd66rzvgk.lvbitx</Property>
			</Item>
			<Item Name="fpga_MotorControl_Totem_7833R_V1.vi" Type="VI" URL="../fpga_MotorControl_Totem_7833R_V1.vi">
				<Property Name="BuildSpec" Type="Str">{A6BD3B82-032B-4EF7-8D09-7A3DBE3666D7}</Property>
				<Property Name="configString.guid" Type="Str">{01A9EA42-079F-464F-9ABC-214A3399A0D2}resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=bool{01F5069C-95F5-4316-A746-F70CE63CE2CB}resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=bool{04667980-387C-45BD-BB8A-F0F2F6B4C3D9}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{04C3B8A7-6FA3-4F5B-8A4F-9F0DD3E75505}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{05AD4111-936E-42BC-8054-1923619B1654}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{083B58C6-93FB-4C6B-A0AF-A35CED081656}resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=bool{09762964-5173-4387-A623-1DBDEDDD0700}resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=bool{0B81E91F-B13E-48BB-9E84-FA2CADD98782}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0DF3EE82-9B77-476B-AEFC-6CB0E3C9B93A}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1305B48C-D228-450B-A93F-B2CCED6D4ADF}resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=bool{145E1805-427D-445E-972B-9E9845F196A8}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{162751A1-3675-4F11-9FFB-CC96452D3BF4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{16D9126B-FEED-409B-AFB0-71ACD0CBF33B}resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=bool{1AB94997-24FC-48CE-A70D-4987EACFBE87}resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=bool{1DB78413-AD41-4551-A6AA-F9E1CC6403CA}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{1FD75D55-56B0-43CB-A5EA-6BA1CB21E1BB}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{233561B4-51A7-440A-8281-2E4708295E64}resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=bool{25B51371-BD73-4836-86C4-2DCD3640BCD1}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{2625D640-871C-4AAE-A948-00168F3AFF6E}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{278B2AFA-8A17-4850-8378-5A0A0B80F72D}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2C39CEF1-E2FF-41B3-AF16-84EEF5855E02}resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool{2FDBBD9F-20DE-4E80-AE40-A7D830CBD819}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{30416893-AC8B-4F74-A65F-13D4A4E72A85}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{317C66D1-AE93-4775-BB0F-5B19DC592B75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{3228B482-2B23-4B0B-AEB0-F4F03A6F4BB5}resource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=bool{3436E786-A2CF-48C2-A169-AEAC340F2835}resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=bool{34814E3F-262C-4474-BA3B-6822AFCF34E9}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{38CC7417-5475-4B24-A7D4-0166F9604A79}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{38D5AD57-2CED-4E3D-A0E4-3BE1A27EA38E}resource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=bool{391C79B7-B710-429F-8C10-185D95001B27}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{39DA5525-701B-4848-8D51-6A6E647DDAF5}resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=bool{3AE31B6D-DBDA-4712-A9A8-E3E315E3BA0C}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{3CC9448E-4CA9-4458-A45D-B38DAEB26418}resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8{3E86EB6E-B567-4D12-872B-F49B143DB6A4}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{3F605400-7FC2-4F9D-BF34-FA0B8F1B9B44}resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8{409FBB6D-8BA0-4A0C-AADF-CA5C74D0FDF3}resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=bool{47711F6F-3464-4B4F-A81D-FC4576125CF0}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{4BEEA604-2A0B-4908-A1BE-FBDEF6CCC6CB}resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=bool{4FEC30F2-34CF-4CBE-B574-19225178E99A}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{4FEEBEF6-3393-40E4-B2D3-6180C77F7A43}resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=bool{5223D13C-5C5B-46BE-BD85-7F727026E72C}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{5AFD6454-C837-45B6-BC8F-4C58164518CB}resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=bool{5CD1A24C-A1FD-47F3-80AD-C7EC59991021}resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=bool{5CD67466-E0CA-4DB5-9227-C48BCCDB31B2}resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=bool{5E5C3E9D-9776-4B4E-85E3-094DFA8FADA4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{6104DB75-E948-4D06-BA8C-71845F8B1ADD}resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=bool{613C68C6-EA1C-4444-9ECA-A43A5EDE1806}resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool{62ACA51A-89F3-4332-B326-D25B22FF8B9F}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{649D961E-A40A-4496-B7D2-18972209CAFC}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{661D9D4F-D0A2-4C25-AEA3-74BDC572EE25}resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=bool{69E494B5-25B9-43A3-92CB-2633C376DFC5}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool{713F706D-A746-475B-A898-8F46CE142552}resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=bool{767C2BCB-0209-4C5F-8E68-42C1D261E4F1}resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=bool{76893B1E-2AA0-443C-9E5F-D90F9F398DB9}resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=bool{78C2049A-E184-44FA-A549-70C8C59E0B2D}resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=bool{7BC673D8-0C51-44A8-86F0-60785BD23EF3}resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=bool{7CF52BF0-661F-48ED-8F21-303024035833}resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{7F26C966-9086-4A5A-A55C-CF7469C03915}resource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=bool{8535DDB7-B9CE-4FFC-BA50-34F405767A3C}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{87E3CDB5-CF17-46B5-B450-60977F7BD779}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{8880D294-6932-46FD-A6C4-CFC22BAAF942}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{89A17F87-3639-4708-ABF8-FC7E2A2A2FE3}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{8EB9B1A7-5FE6-454D-8E88-5CD03AC7F2AD}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{90DBE15B-E41B-47D0-B06E-F16ECF1F47C2}resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool{90FE3DD2-CC28-41D9-8E89-642A13B6ECA7}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{954E1C40-1C3C-4AC0-95C3-BE30249F4FB5}resource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=bool{97C1349B-0209-4DB7-8656-70D32BD49B17}resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=bool{987E6BA0-DDB1-4BC4-8E54-0AAAC0CA0C5A}resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=bool{990252B3-0424-44E4-99F8-487ACA783735}resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{A2860DB0-E4E5-4B12-ACB8-CE0729AC125C}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{A2F6DCEE-E9CF-45C4-861F-B1E972D5D026}resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool{A30CE9D6-EEAA-4E1A-B16D-B7F8A1BBC334}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{A6088509-9555-424E-83B0-CD5039D63C82}resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=bool{A6C1522A-18CB-406B-AA65-702A4525B684}resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{AA7A6B99-CA56-4A89-8967-5B486BD4D6F3}resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=bool{ADF6EBE2-21F3-4EFD-BCC6-31D221C0CED9}resource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=bool{ADFBF5CC-4EAE-42AF-97A8-2DE80CD61050}resource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=bool{B5DA1A6B-0FD6-40E8-ABF0-3B1E1005D892}resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=bool{B5DEAC4E-0D3A-459C-B26C-792C63A4E9D8}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{B66A844A-EE1B-4FAD-AD60-2CF807CC3207}resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8{B67B11AE-D69F-4A11-B204-98FDAA624C96}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{B9802DCD-92F5-421A-B53A-7BFA645A5D72}resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=bool{BF1E89DB-C44D-44CB-8A81-79DA39CFF86B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{BFE62D56-CB08-49AE-BD7D-59F705169691}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3228B56-E4CB-44F5-A0E2-424A18384455}resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=bool{C37C08D1-9892-4882-8959-47414C9BBD90}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{C3A72A4A-30BB-4023-9B9E-9DD8D7824921}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{C5AD37CB-0D80-4394-94A3-FEAE523F9C0D}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{C9E6AFAA-D8F8-4A3B-BDA3-FF99871C8647}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{CA918332-B0D4-4ACA-85F4-CD32BB678EC7}resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=bool{CBB77E6D-DEB1-434D-BD29-8E08B31932BC}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{CCADB888-05C5-4586-91C9-40D66DAADB84}resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{CF95A4D7-4CD8-4762-97C6-620444DD705C}resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=bool{D21B9BFA-4782-4257-B185-26D9C86BB643}resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=bool{D4B8D988-EB97-4500-BCA1-712E22379E8B}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{D5CB729E-9D1F-4224-B259-8DCB1BB964DB}resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=bool{D8D16AC1-D001-43FE-960D-4E5A928391EC}resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32{D90DD2E8-D756-4DD6-A288-E4C8913D0F9D}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{DBC41703-4B74-4E66-B5D9-7D771B07C53B}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{DED791D2-A92C-4291-9642-4B9D0C8AC35F}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{DFA514D0-B351-4C25-A8FE-65DA1E13C472}resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8{E2587605-DB69-4671-B573-DBCCEFB9A78D}resource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=bool{E400AA33-A86A-4F23-AF06-502976EAE216}resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=bool{E47BF981-9F0C-4E8A-BA07-9966B1D5650F}resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=bool{E5C18673-06D3-4636-B120-A96E7E18DAC3}resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=bool{ECA52B1D-A49F-44A5-9E28-B86A4D36698E}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{ED63FE57-F3FD-4205-B354-E5FA17FAAA53}resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{EF7119EB-9FC2-4E92-8E59-129799DFE55F}resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=bool{F087738E-D492-4765-B1E2-5F914AEC9D58}resource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=bool{F364E9CE-2DCC-40D5-84C0-6DB86EEA0D1E}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{F68E6BD0-7631-4B48-BB15-64BBA3E6596F}resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=bool{F6950DBB-9DF7-4824-B52B-D6FD301575AD}resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=bool{F8D354D9-9F08-4DED-8831-BF33D2752986}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{F9FEE6DA-521C-4D43-9A92-ADC140DDAD86}resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=bool{FCD802DF-46E2-4257-849E-FA815C3824A6}resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=bool{FE5A6C50-13DF-469D-9A60-E6CC97AEE9DE}resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=bool{FF798525-F7FD-4801-A98F-12AF350F6C7A}resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">12VDC_Fltresource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool220VAC_Drive_Fltresource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool24V_Common_Interlocksresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool24V_Ok_M1resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool24V_Ok_M2resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool24V_Ok_M3resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool24V_Ok_M4resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool24VDC_Fltresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Home_Beam1resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolAll_RP_Home_Beam2resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolAntiColl_M1M2resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=boolAntiColl_M3M4resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=boolBack_Homeresource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolChassis1/Mod1/DI15:8resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8Chassis1/Mod1/DI23:16resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8Chassis1/Mod1/DI30resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=boolChassis1/Mod1/DI31:0resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32Chassis1/Mod1/DI31:24resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8Chassis1/Mod1/DI31resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=boolChassis1/Mod1/DI7:0resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8Chassis1/Mod1[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2/DO15:8resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO16resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO17resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO18resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO19resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO20resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO21resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO22resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO23:16resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO23resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO24resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO25resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO26resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO27resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO28resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO29resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO30resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO31:0resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Chassis1_Mod2/DO31:24resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO31resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO7:0resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M1resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=boolCopy_HomeSw_M2resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=boolCopy_HomeSw_M3resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=boolCopy_HomeSw_M4resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolDirection_M1resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M2resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M3resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M4resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDisable_M1resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolDisable_M2resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolDisable_M3resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolDisable_M4resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolDriver_Flt_M1resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=boolDriver_Flt_M2resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=boolDriver_Flt_M3resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=boolDriver_Flt_M4resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=boolInjection_Inhib_Beam1resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolInjection_Inhib_Beam2resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolM01_LVDT_Fltresource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=boolM01_OutOfLimitsresource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=boolM02_LVDT_Fltresource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=boolM02_OutOfLimitsresource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=boolM03_LVDT_Fltresource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=boolM03_OutOfLimitsresource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=boolM04_LVDT_Fltresource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=boolM04_OutOfLimitsresource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=boolM1_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16M1_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16M1_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16M2_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16M2_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16M2_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16M3_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16M3_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16M3_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16M4_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M4_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16M4_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Overrideresource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPost_Mortemresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M1resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M2resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M3resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M4resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolSafe_Beam1resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSafe_Beam2resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolSpareresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolStep_M1resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M2resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M3resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M4resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M1resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=boolStopper_In_M2resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=boolStopper_In_M3resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=boolStopper_In_M4resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=boolStopper_Out_M1resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=boolStopper_Out_M2resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=boolStopper_Out_M3resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=boolStopper_Out_M4resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=boolSw_In_M1resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=boolSw_In_M2resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolSw_In_M3resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=boolSw_In_M4resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=boolSw_Out_M1resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=boolSw_Out_M2resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=boolSw_Out_M3resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=boolSw_Out_M4resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=boolUser_Permit_Cibu_1aresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_Cibu_1bresource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolUser_Permit_Cibu_2aresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=boolUser_Permit_Cibu_2bresource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_ALFA\FPGA Bitfiles\RPTunnel_MotorCtrlM1-4_fpgaMotorControl_eoSccLYqRgI.lvbitx</Property>
			</Item>
			<Item Name="fpga_MotorControl_Totem_7833R_V2.vi" Type="VI" URL="../fpga_MotorControl_Totem_7833R_V2.vi">
				<Property Name="BuildSpec" Type="Str">{640871B8-5A59-4B33-8B7C-789350AD994A}</Property>
				<Property Name="configString.guid" Type="Str">{01A9EA42-079F-464F-9ABC-214A3399A0D2}resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=bool{01F5069C-95F5-4316-A746-F70CE63CE2CB}resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=bool{04667980-387C-45BD-BB8A-F0F2F6B4C3D9}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{04C3B8A7-6FA3-4F5B-8A4F-9F0DD3E75505}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{05AD4111-936E-42BC-8054-1923619B1654}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{083B58C6-93FB-4C6B-A0AF-A35CED081656}resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=bool{09762964-5173-4387-A623-1DBDEDDD0700}resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=bool{0B81E91F-B13E-48BB-9E84-FA2CADD98782}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0DF3EE82-9B77-476B-AEFC-6CB0E3C9B93A}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1305B48C-D228-450B-A93F-B2CCED6D4ADF}resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=bool{145E1805-427D-445E-972B-9E9845F196A8}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{162751A1-3675-4F11-9FFB-CC96452D3BF4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{16D9126B-FEED-409B-AFB0-71ACD0CBF33B}resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=bool{1AB94997-24FC-48CE-A70D-4987EACFBE87}resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=bool{1DB78413-AD41-4551-A6AA-F9E1CC6403CA}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{1FD75D55-56B0-43CB-A5EA-6BA1CB21E1BB}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{233561B4-51A7-440A-8281-2E4708295E64}resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=bool{25B51371-BD73-4836-86C4-2DCD3640BCD1}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{2625D640-871C-4AAE-A948-00168F3AFF6E}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{278B2AFA-8A17-4850-8378-5A0A0B80F72D}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2C39CEF1-E2FF-41B3-AF16-84EEF5855E02}resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool{2FDBBD9F-20DE-4E80-AE40-A7D830CBD819}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{30416893-AC8B-4F74-A65F-13D4A4E72A85}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{317C66D1-AE93-4775-BB0F-5B19DC592B75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{3228B482-2B23-4B0B-AEB0-F4F03A6F4BB5}resource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=bool{3436E786-A2CF-48C2-A169-AEAC340F2835}resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=bool{34814E3F-262C-4474-BA3B-6822AFCF34E9}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{38CC7417-5475-4B24-A7D4-0166F9604A79}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{38D5AD57-2CED-4E3D-A0E4-3BE1A27EA38E}resource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=bool{391C79B7-B710-429F-8C10-185D95001B27}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{39DA5525-701B-4848-8D51-6A6E647DDAF5}resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=bool{3AE31B6D-DBDA-4712-A9A8-E3E315E3BA0C}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{3CC9448E-4CA9-4458-A45D-B38DAEB26418}resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8{3E86EB6E-B567-4D12-872B-F49B143DB6A4}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{3F605400-7FC2-4F9D-BF34-FA0B8F1B9B44}resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8{409FBB6D-8BA0-4A0C-AADF-CA5C74D0FDF3}resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=bool{47711F6F-3464-4B4F-A81D-FC4576125CF0}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{4BEEA604-2A0B-4908-A1BE-FBDEF6CCC6CB}resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=bool{4FEC30F2-34CF-4CBE-B574-19225178E99A}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{4FEEBEF6-3393-40E4-B2D3-6180C77F7A43}resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=bool{5223D13C-5C5B-46BE-BD85-7F727026E72C}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{5AFD6454-C837-45B6-BC8F-4C58164518CB}resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=bool{5CD1A24C-A1FD-47F3-80AD-C7EC59991021}resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=bool{5CD67466-E0CA-4DB5-9227-C48BCCDB31B2}resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=bool{5E5C3E9D-9776-4B4E-85E3-094DFA8FADA4}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{6104DB75-E948-4D06-BA8C-71845F8B1ADD}resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=bool{613C68C6-EA1C-4444-9ECA-A43A5EDE1806}resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool{62ACA51A-89F3-4332-B326-D25B22FF8B9F}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{649D961E-A40A-4496-B7D2-18972209CAFC}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{661D9D4F-D0A2-4C25-AEA3-74BDC572EE25}resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=bool{69E494B5-25B9-43A3-92CB-2633C376DFC5}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool{713F706D-A746-475B-A898-8F46CE142552}resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=bool{767C2BCB-0209-4C5F-8E68-42C1D261E4F1}resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=bool{76893B1E-2AA0-443C-9E5F-D90F9F398DB9}resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=bool{78C2049A-E184-44FA-A549-70C8C59E0B2D}resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=bool{7BC673D8-0C51-44A8-86F0-60785BD23EF3}resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=bool{7CF52BF0-661F-48ED-8F21-303024035833}resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{7F26C966-9086-4A5A-A55C-CF7469C03915}resource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=bool{8535DDB7-B9CE-4FFC-BA50-34F405767A3C}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{87E3CDB5-CF17-46B5-B450-60977F7BD779}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{8880D294-6932-46FD-A6C4-CFC22BAAF942}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{89A17F87-3639-4708-ABF8-FC7E2A2A2FE3}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{8EB9B1A7-5FE6-454D-8E88-5CD03AC7F2AD}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{90DBE15B-E41B-47D0-B06E-F16ECF1F47C2}resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool{90FE3DD2-CC28-41D9-8E89-642A13B6ECA7}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{954E1C40-1C3C-4AC0-95C3-BE30249F4FB5}resource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=bool{97C1349B-0209-4DB7-8656-70D32BD49B17}resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=bool{987E6BA0-DDB1-4BC4-8E54-0AAAC0CA0C5A}resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=bool{990252B3-0424-44E4-99F8-487ACA783735}resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{A2860DB0-E4E5-4B12-ACB8-CE0729AC125C}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{A2F6DCEE-E9CF-45C4-861F-B1E972D5D026}resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool{A30CE9D6-EEAA-4E1A-B16D-B7F8A1BBC334}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{A6088509-9555-424E-83B0-CD5039D63C82}resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=bool{A6C1522A-18CB-406B-AA65-702A4525B684}resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{AA7A6B99-CA56-4A89-8967-5B486BD4D6F3}resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=bool{ADF6EBE2-21F3-4EFD-BCC6-31D221C0CED9}resource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=bool{ADFBF5CC-4EAE-42AF-97A8-2DE80CD61050}resource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=bool{B5DA1A6B-0FD6-40E8-ABF0-3B1E1005D892}resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=bool{B5DEAC4E-0D3A-459C-B26C-792C63A4E9D8}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{B66A844A-EE1B-4FAD-AD60-2CF807CC3207}resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8{B67B11AE-D69F-4A11-B204-98FDAA624C96}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{B9802DCD-92F5-421A-B53A-7BFA645A5D72}resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=bool{BF1E89DB-C44D-44CB-8A81-79DA39CFF86B}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{BFE62D56-CB08-49AE-BD7D-59F705169691}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3228B56-E4CB-44F5-A0E2-424A18384455}resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=bool{C37C08D1-9892-4882-8959-47414C9BBD90}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{C3A72A4A-30BB-4023-9B9E-9DD8D7824921}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{C5AD37CB-0D80-4394-94A3-FEAE523F9C0D}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{C9E6AFAA-D8F8-4A3B-BDA3-FF99871C8647}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{CA918332-B0D4-4ACA-85F4-CD32BB678EC7}resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=bool{CBB77E6D-DEB1-434D-BD29-8E08B31932BC}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{CCADB888-05C5-4586-91C9-40D66DAADB84}resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{CF95A4D7-4CD8-4762-97C6-620444DD705C}resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=bool{D21B9BFA-4782-4257-B185-26D9C86BB643}resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=bool{D4B8D988-EB97-4500-BCA1-712E22379E8B}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{D5CB729E-9D1F-4224-B259-8DCB1BB964DB}resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=bool{D8D16AC1-D001-43FE-960D-4E5A928391EC}resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32{D90DD2E8-D756-4DD6-A288-E4C8913D0F9D}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{DBC41703-4B74-4E66-B5D9-7D771B07C53B}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{DED791D2-A92C-4291-9642-4B9D0C8AC35F}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{DFA514D0-B351-4C25-A8FE-65DA1E13C472}resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8{E2587605-DB69-4671-B573-DBCCEFB9A78D}resource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=bool{E400AA33-A86A-4F23-AF06-502976EAE216}resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=bool{E47BF981-9F0C-4E8A-BA07-9966B1D5650F}resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=bool{E5C18673-06D3-4636-B120-A96E7E18DAC3}resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=bool{ECA52B1D-A49F-44A5-9E28-B86A4D36698E}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{ED63FE57-F3FD-4205-B354-E5FA17FAAA53}resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{EF7119EB-9FC2-4E92-8E59-129799DFE55F}resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=bool{F087738E-D492-4765-B1E2-5F914AEC9D58}resource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=bool{F364E9CE-2DCC-40D5-84C0-6DB86EEA0D1E}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{F68E6BD0-7631-4B48-BB15-64BBA3E6596F}resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=bool{F6950DBB-9DF7-4824-B52B-D6FD301575AD}resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=bool{F8D354D9-9F08-4DED-8831-BF33D2752986}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{F9FEE6DA-521C-4D43-9A92-ADC140DDAD86}resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=bool{FCD802DF-46E2-4257-849E-FA815C3824A6}resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=bool{FE5A6C50-13DF-469D-9A60-E6CC97AEE9DE}resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=bool{FF798525-F7FD-4801-A98F-12AF350F6C7A}resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">12VDC_Fltresource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool220VAC_Drive_Fltresource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool24V_Common_Interlocksresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool24V_Ok_M1resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool24V_Ok_M2resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool24V_Ok_M3resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool24V_Ok_M4resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool24VDC_Fltresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Home_Beam1resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolAll_RP_Home_Beam2resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolAntiColl_M1M2resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=boolAntiColl_M3M4resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=boolBack_Homeresource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolChassis1/Mod1/DI15:8resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8Chassis1/Mod1/DI23:16resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8Chassis1/Mod1/DI30resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=boolChassis1/Mod1/DI31:0resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32Chassis1/Mod1/DI31:24resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8Chassis1/Mod1/DI31resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=boolChassis1/Mod1/DI7:0resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8Chassis1/Mod1[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2/DO15:8resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO16resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO17resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO18resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO19resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO20resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO21resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO22resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO23:16resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO23resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO24resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO25resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO26resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO27resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO28resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO29resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO30resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO31:0resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Chassis1_Mod2/DO31:24resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO31resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO7:0resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M1resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=boolCopy_HomeSw_M2resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=boolCopy_HomeSw_M3resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=boolCopy_HomeSw_M4resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolDirection_M1resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M2resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M3resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M4resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDisable_M1resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolDisable_M2resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolDisable_M3resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolDisable_M4resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolDriver_Flt_M1resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=boolDriver_Flt_M2resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=boolDriver_Flt_M3resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=boolDriver_Flt_M4resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=boolInjection_Inhib_Beam1resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolInjection_Inhib_Beam2resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolM01_LVDT_Fltresource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=boolM01_OutOfLimitsresource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=boolM02_LVDT_Fltresource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=boolM02_OutOfLimitsresource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=boolM03_LVDT_Fltresource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=boolM03_OutOfLimitsresource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=boolM04_LVDT_Fltresource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=boolM04_OutOfLimitsresource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=boolM1_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16M1_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16M1_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16M2_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16M2_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16M2_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16M3_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16M3_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16M3_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16M4_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M4_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16M4_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Overrideresource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPost_Mortemresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M1resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M2resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M3resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M4resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolSafe_Beam1resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSafe_Beam2resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolSpareresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolStep_M1resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M2resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M3resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M4resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M1resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=boolStopper_In_M2resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=boolStopper_In_M3resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=boolStopper_In_M4resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=boolStopper_Out_M1resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=boolStopper_Out_M2resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=boolStopper_Out_M3resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=boolStopper_Out_M4resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=boolSw_In_M1resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=boolSw_In_M2resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolSw_In_M3resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=boolSw_In_M4resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=boolSw_Out_M1resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=boolSw_Out_M2resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=boolSw_Out_M3resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=boolSw_Out_M4resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=boolUser_Permit_Cibu_1aresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_Cibu_1bresource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolUser_Permit_Cibu_2aresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=boolUser_Permit_Cibu_2bresource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_ALFA\FPGA Bitfiles\RPTunnel_MotorCtrlM1-4_fpgaMotorControl_PfhT1rkwg34.lvbitx</Property>
			</Item>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="NI_SC_LVSCCommonFiles.lvlib" Type="Library" URL="/&lt;vilib&gt;/Statechart/Common/NI_SC_LVSCCommonFiles.lvlib"/>
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
					<Item Name="SCRT Dbg Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT Dbg Rtn.vi"/>
					<Item Name="SCRT SDV Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT SDV Rtn.vi"/>
				</Item>
				<Item Name="instr.lib" Type="Folder">
					<Item Name="niInstr Basic Elements v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/niInstr Basic Elements v1 FPGA.lvlib"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="fpga_MotorControl_Totem_7833R" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_MotorControl_Totem_7833R</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPTunnel_MotorCtrlM1-4_fpgaMotorControl_B-dd66rzvgk.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_ALFA/FPGA Bitfiles/RPTunnel_MotorCtrlM1-4_fpgaMotorControl_B-dd66rzvgk.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPTunnel_MotorCtrlM1-4_fpgaMotorControl_B-dd66rzvgk.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_ALFA/RP_Tunnel.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
					<Property Name="TargetName" Type="Str">MotorCtrl M1-4</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/MotorCtrl M1-4/fpga_MotorControl_Totem_7833R.vi</Property>
				</Item>
				<Item Name="fpga_MotorControl_Totem_7833R_V1" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_MotorControl_Totem_7833R_V1</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPTunnel_MotorCtrlM1-4_fpgaMotorControl_eoSccLYqRgI.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_ALFA/FPGA Bitfiles/RPTunnel_MotorCtrlM1-4_fpgaMotorControl_eoSccLYqRgI.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPTunnel_MotorCtrlM1-4_fpgaMotorControl_eoSccLYqRgI.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_ALFA/RP_Tunnel.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
					<Property Name="TargetName" Type="Str">MotorCtrl M1-4</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/MotorCtrl M1-4/fpga_MotorControl_Totem_7833R_V1.vi</Property>
				</Item>
				<Item Name="fpga_MotorControl_Totem_7833R_V2" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_MotorControl_Totem_7833R_V2</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPTunnel_MotorCtrlM1-4_fpgaMotorControl_PfhT1rkwg34.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_ALFA/FPGA Bitfiles/RPTunnel_MotorCtrlM1-4_fpgaMotorControl_PfhT1rkwg34.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPTunnel_MotorCtrlM1-4_fpgaMotorControl_PfhT1rkwg34.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_ALFA/RP_Tunnel.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
					<Property Name="TargetName" Type="Str">MotorCtrl M1-4</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/MotorCtrl M1-4/fpga_MotorControl_Totem_7833R_V2.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="MotorCtrl M5-8" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{0052E434-3443-42D6-8E6F-AF63385896E8}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{00AD2C0B-3718-4EA4-A551-E98943FFF020}resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=bool{0122616C-F2C8-47E8-9F8C-DA330DDF5D50}resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=bool{0458348C-689B-4895-B640-9B5AE98BBF53}resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=bool{045FE124-D632-443E-BC69-7568E6BE6A2D}resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=bool{071F60D4-1C72-459E-AE78-74876F23F0B1}resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=bool{07BBDBEB-ED66-46B6-A472-9CFB461B9FAD}resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=bool{08B34D4C-71EA-46E2-A319-5924A76F8C76}resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=bool{09CDC6BD-C284-4DFB-BDF2-4F62721AE00C}resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=bool{0A51C8A2-D409-4BE5-9EF6-9BE5CAEA1B34}resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=bool{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0DFB4846-46B5-44F7-9E26-5CA2E8B8C6EF}resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32{0EFA0BDF-F66A-45AA-9643-D600633DE9EB}resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=bool{1268E293-2783-4413-86F3-2BC2D49CC32E}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{1697C2B6-B88E-4F0A-B7E7-335FDF2E68DA}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{1943D65F-FFA6-4980-8468-30C958541A5F}resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=bool{1B7F42DF-D163-40EF-A193-CF300011301B}resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=bool{1E83B95C-8659-4ED7-80A9-3208095168A2}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{20730AF7-EC19-4D43-931A-74396A97EB3D}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{20A11DDE-5A4C-4BFC-ACA0-29F3EDA6392E}resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=bool{22C73DC4-E609-4828-9081-6965C02206A8}resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=bool{26F74165-8695-45A1-A426-4230026D8A29}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{29E34220-EE50-443D-A6B7-FAD507C92910}resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=bool{2A7CEAA1-C515-4287-9D0B-20CA60599210}resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{2C66EE40-92FE-4ABB-8222-892111E5BF0C}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{2F8EFCFE-3325-4519-93BE-61EDAEB57DAE}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{317C66D1-AE93-4775-BB0F-5B19DC592B75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{33C75ADD-2F03-4EF2-826A-6BDD3D08ED75}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{388B3035-EEF1-4F2A-A940-E2868F5641A7}resource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=bool{392DA4F7-C339-4B43-9E54-7DA8E4B61866}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{3A558969-AAB9-4F3E-90B0-0E8DA8FACF37}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{3B28889E-8525-4581-A817-9151DC493D6A}resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=bool{3C0D7749-2587-4D95-B9F1-29BA7E06EA7B}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{3CC65A6A-2AC9-40A4-A1F2-4FA183421DA8}resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=bool{3FD3F2A8-D961-48F8-9CD8-71A2FB69B9E9}resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=bool{444A2713-D2A7-4457-B11B-48F2A155F17C}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{44614314-BCB7-4EDC-BB50-7957B22BA4AB}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{4844DF8E-24C6-4BD7-BF9D-E80D1866DD54}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{4C64A11E-E1C3-4E75-849B-489B96C5C9AE}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{4D502716-12D8-4148-9496-1AF7DB65D953}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{4D8C41E0-CF73-464F-8577-5FBBFD9E2A21}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{59598955-69F8-4250-AF28-5AA462525E93}resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=bool{6582604C-17B0-4A91-8BD1-D2020B05D49F}resource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=bool{65ABAE8B-1E32-4AFD-867D-428950125068}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{66AFE271-2B99-4949-AE23-D44E80D0DEDD}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{6C0212AB-322D-4240-8362-574087962EEB}resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=bool{6C39CF12-9996-495A-B1EB-2CB9E287AA24}resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=bool{6CD4D772-F60F-4A34-B525-D1AEE4A86EE2}resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8{70F032B8-C011-4D09-BA5D-C8F4C0474BEF}resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool{730162A5-78D9-43DB-9E6B-62E3F0462A42}resource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=bool{751FA68D-2064-40A4-AC3F-CB6CE37715F3}resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{775D4A5D-43C5-4CF8-89FB-102C304604D7}resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8{7A316D08-3455-481D-8BF5-AB8DB21144CC}resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=bool{7E8F8D06-BB50-42A9-AE4E-34E52E30ED5D}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{80779440-5723-46D2-835D-E20D940FB6A0}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{828485A5-65B7-40C1-9557-055A6677695E}resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=bool{82FC8550-3CD0-4091-A4F4-E2D5FA67204C}resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=bool{83581701-0304-4195-88BE-BD4221BD5A0F}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{844B3C52-BFCD-4900-9EA9-680B64A59AB9}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{87063C83-6142-4C61-ACF1-E7A3B77A9EE2}resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=bool{87FF4330-4B24-45B7-B055-B36D95251412}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{8AB615C2-50FC-41FE-A663-92751CF54C3E}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{8DF6F286-FF64-4E04-A2C5-73C9516DEE96}resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{8F175A95-2AEE-41A4-9215-EBF7FB01C1B1}resource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=bool{912E884D-332E-4AEE-A6C1-534990DBFA7A}resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8{9215D7ED-79D6-40B4-8F14-140E9C3F8A51}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{928496BD-4554-420E-943A-5F7B3033A764}resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=bool{92A383C7-8BD7-4090-A189-66CE25FA7960}resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool{92E6117F-A4A4-4FEA-BA60-613692D1E32C}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{98EDF2C1-9DF5-4200-BC64-BCC0DE24CC80}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{9AB57D24-A419-4F79-8A95-EEC729420494}resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=bool{9D6928AD-1DED-4B0B-B393-3E30DFBF5365}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{9D7B79EE-72B0-4C86-A1A1-6034560703B9}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{A179CD0C-CB14-4E41-883D-640DA8B2D62E}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{A6D179CA-6C28-4B62-ABE3-940F5C15D969}resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=bool{AAAA3149-F316-41FE-9B7A-F5949AC0531D}resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=bool{AF0A5BDF-3F59-47A9-B7CF-7AA74B7D8E0C}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{B029420B-821E-440C-B07E-8F6E8CC21174}resource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=bool{B3EE8B7B-54A4-4952-B70A-817B64D23E90}resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool{B62B65D7-FA9C-4ED9-91B1-C5BC8CF0C979}resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=bool{BAEF79B6-4AAA-4B7D-AC92-9279F19911AB}resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=bool{BFE62D56-CB08-49AE-BD7D-59F705169691}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C4E82D36-6F55-4189-A6EB-60DC28B4D69C}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{C6996D0C-FE87-4A3A-A974-DEDC37B00DA8}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{C6AA296C-1960-4AB0-93CB-C917EDA685F2}resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=bool{CA9C8584-93E7-4739-930C-F7A86AF39CB2}resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=bool{CAB89ED3-AC6F-46FC-BB24-62A01CF4F8F2}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{CD14D72B-C8A1-411F-8A2D-221B723D1AC5}resource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=bool{CE1A4DC1-B4BE-4371-B64C-4FCA5B6955F9}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{D1E54C25-1187-4BC5-A193-B25E54D30A3C}resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{D23CD8A2-90EB-4863-B741-C806DDA82716}resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=bool{D338E7C6-6640-4ED2-A3E7-8908A62A384C}resource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=bool{D524A952-5A21-46AE-921F-953855CF72C4}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{D55769F5-B485-441C-9406-0C2ED387572B}resource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=bool{D6032CB7-5BB7-4D30-819A-26D08FC5E10A}resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=bool{D7295F2E-B156-4A06-86B9-3B1D22B93256}resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=bool{D7B2306E-AC60-44CC-B8D7-CBDF80D4577F}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool{D7F7DB96-F908-4C77-8236-97EB6E1730D6}resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{DB0FF485-0131-42B3-9C96-77909E3764E9}resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=bool{DB567744-499F-4C68-ADE3-22D1546C7DF0}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{DEA655CF-BBFA-4A4B-B68C-CAF6EDF666B1}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{E0EF9574-6EFD-4F86-91A2-2486DD51829B}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{E251BBBC-0EB7-4862-B0A5-C094723FEDFB}resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool{E49B4056-E0CE-404B-9926-2AEB8F582E67}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{E76E5997-797D-4590-AA57-B8A380607797}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{E9A2BA38-A67D-49D0-8B2B-09FAF839F714}resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=bool{EC4502CB-224E-48D0-B9C3-494A7D0301F5}resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=bool{EDF4D77C-EF48-4041-B3F4-5C9AE5F056CF}resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=bool{EE27EF1E-BD08-4D9A-9A47-CABACF6E0317}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{F20D282E-B3AA-45DC-ACC1-495C472A518D}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{F352E634-3E96-4BBB-AE84-947238E5F9F5}resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=bool{F4BFB583-7234-43EE-9509-9E38F5EE7081}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{F6B6CA56-33EF-426D-85A8-3D52764B7434}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{F895825C-12E3-4D4E-92DF-30A2A6A9F453}resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=bool{FB53B742-363B-42E3-968B-0E9A32CD794A}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{FCE656A9-8DA5-49F1-BE6C-08E02D5E2664}resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8{FF20E206-0EE3-47BB-ABC3-6B17AACA6073}resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=bool{FF7F926D-36D5-4B16-B4D8-F6791CEF535C}resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">12VDC_Fltresource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool220VAC_Drive_Fltresource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool24V_Common_Interlocksresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool24V_Ok_M1resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool24V_Ok_M2resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool24V_Ok_M3resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool24V_Ok_M4resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool24VDC_Fltresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Home_Beam1resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolAll_RP_Home_Beam2resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolAntiColl_M1M2resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=boolAntiColl_M3M4resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=boolBack_Homeresource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolChassis1/Mod1/DI15:8resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8Chassis1/Mod1/DI23:16resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8Chassis1/Mod1/DI30resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=boolChassis1/Mod1/DI31:0resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32Chassis1/Mod1/DI31:24resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8Chassis1/Mod1/DI31resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=boolChassis1/Mod1/DI7:0resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8Chassis1/Mod1[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2/DO15:8resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO16resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO17resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO18resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO19resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO20resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO21resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO22resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO23:16resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO23resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO24resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO25resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO26resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO27resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO28resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO29resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO30resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO31:0resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Chassis1_Mod2/DO31:24resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO31resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO7:0resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M1resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=boolCopy_HomeSw_M2resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=boolCopy_HomeSw_M3resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=boolCopy_HomeSw_M4resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolDirection_M1resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M2resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M3resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M4resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDisable_M1resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolDisable_M2resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolDisable_M3resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolDisable_M4resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolDriver_Flt_M1resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=boolDriver_Flt_M2resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=boolDriver_Flt_M3resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=boolDriver_Flt_M4resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=boolInjection_Inhib_Beam1resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolInjection_Inhib_Beam2resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolM01_LVDT_Fltresource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=boolM01_OutOfLimitsresource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=boolM02_LVDT_Fltresource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=boolM02_OutOfLimitsresource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=boolM03_LVDT_Fltresource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=boolM03_OutOfLimitsresource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=boolM04_LVDT_Fltresource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=boolM04_OutOfLimitsresource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=boolM1_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16M1_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16M1_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16M2_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16M2_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16M2_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16M3_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16M3_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16M3_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16M4_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M4_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16M4_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Overrideresource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPost_Mortemresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M1resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M2resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M3resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M4resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolSafe_Beam1resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSafe_Beam2resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolSpareresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolStep_M1resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M2resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M3resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M4resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M1resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=boolStopper_In_M2resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=boolStopper_In_M3resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=boolStopper_In_M4resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=boolStopper_Out_M1resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=boolStopper_Out_M2resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=boolStopper_Out_M3resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=boolStopper_Out_M4resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=boolSw_In_M1resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=boolSw_In_M2resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolSw_In_M3resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=boolSw_In_M4resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=boolSw_Out_M1resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=boolSw_Out_M2resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=boolSw_Out_M3resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=boolSw_Out_M4resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=boolUser_Permit_Cibu_1aresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_Cibu_1bresource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolUser_Permit_Cibu_2aresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=boolUser_Permit_Cibu_2bresource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool</Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">PXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="Resource Name" Type="Str">RIO1</Property>
			<Property Name="Target Class" Type="Str">PXI-7833R</Property>
			<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="DigitalInputModule1" Type="Folder">
				<Item Name="Driver_Flt_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3B28889E-8525-4581-A817-9151DC493D6A}</Property>
				</Item>
				<Item Name="Driver_Flt_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1B7F42DF-D163-40EF-A193-CF300011301B}</Property>
				</Item>
				<Item Name="Driver_Flt_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9AB57D24-A419-4F79-8A95-EEC729420494}</Property>
				</Item>
				<Item Name="Driver_Flt_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{EDF4D77C-EF48-4041-B3F4-5C9AE5F056CF}</Property>
				</Item>
				<Item Name="24V_Ok_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{92A383C7-8BD7-4090-A189-66CE25FA7960}</Property>
				</Item>
				<Item Name="24V_Ok_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{70F032B8-C011-4D09-BA5D-C8F4C0474BEF}</Property>
				</Item>
				<Item Name="24V_Ok_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E251BBBC-0EB7-4862-B0A5-C094723FEDFB}</Property>
				</Item>
				<Item Name="24V_Ok_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B3EE8B7B-54A4-4952-B70A-817B64D23E90}</Property>
				</Item>
				<Item Name="Sw_Out_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0A51C8A2-D409-4BE5-9EF6-9BE5CAEA1B34}</Property>
				</Item>
				<Item Name="Sw_In_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1943D65F-FFA6-4980-8468-30C958541A5F}</Property>
				</Item>
				<Item Name="Stopper_Out_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D23CD8A2-90EB-4863-B741-C806DDA82716}</Property>
				</Item>
				<Item Name="Stopper_In_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{20A11DDE-5A4C-4BFC-ACA0-29F3EDA6392E}</Property>
				</Item>
				<Item Name="AntiColl_M1M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6C0212AB-322D-4240-8362-574087962EEB}</Property>
				</Item>
				<Item Name="Sw_Out_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{22C73DC4-E609-4828-9081-6965C02206A8}</Property>
				</Item>
				<Item Name="Sw_In_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{82FC8550-3CD0-4091-A4F4-E2D5FA67204C}</Property>
				</Item>
				<Item Name="Stopper_Out_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0458348C-689B-4895-B640-9B5AE98BBF53}</Property>
				</Item>
				<Item Name="Stopper_In_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A6D179CA-6C28-4B62-ABE3-940F5C15D969}</Property>
				</Item>
				<Item Name="Sw_Out_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DB0FF485-0131-42B3-9C96-77909E3764E9}</Property>
				</Item>
				<Item Name="Sw_In_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{EC4502CB-224E-48D0-B9C3-494A7D0301F5}</Property>
				</Item>
				<Item Name="Stopper_Out_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{828485A5-65B7-40C1-9557-055A6677695E}</Property>
				</Item>
				<Item Name="Stopper_In_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI20</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{09CDC6BD-C284-4DFB-BDF2-4F62721AE00C}</Property>
				</Item>
				<Item Name="AntiColl_M3M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI21</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F895825C-12E3-4D4E-92DF-30A2A6A9F453}</Property>
				</Item>
				<Item Name="Sw_Out_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI22</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{AAAA3149-F316-41FE-9B7A-F5949AC0531D}</Property>
				</Item>
				<Item Name="Sw_In_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI23</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D6032CB7-5BB7-4D30-819A-26D08FC5E10A}</Property>
				</Item>
				<Item Name="Stopper_Out_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CA9C8584-93E7-4739-930C-F7A86AF39CB2}</Property>
				</Item>
				<Item Name="Stopper_In_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI25</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7A316D08-3455-481D-8BF5-AB8DB21144CC}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI26</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{08B34D4C-71EA-46E2-A319-5924A76F8C76}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI27</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F352E634-3E96-4BBB-AE84-947238E5F9F5}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI28</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6C39CF12-9996-495A-B1EB-2CB9E287AA24}</Property>
				</Item>
				<Item Name="Copy_HomeSw_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI29</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3CC65A6A-2AC9-40A4-A1F2-4FA183421DA8}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI30" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI30</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{071F60D4-1C72-459E-AE78-74876F23F0B1}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI31" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI31</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{07BBDBEB-ED66-46B6-A472-9CFB461B9FAD}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI7:0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{775D4A5D-43C5-4CF8-89FB-102C304604D7}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI15:8" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{912E884D-332E-4AEE-A6C1-534990DBFA7A}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI23:16" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6CD4D772-F60F-4A34-B525-D1AEE4A86EE2}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI31:24" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FCE656A9-8DA5-49F1-BE6C-08E02D5E2664}</Property>
				</Item>
				<Item Name="Chassis1/Mod1/DI31:0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1\/Mod1/DI31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0DFB4846-46B5-44F7-9E26-5CA2E8B8C6EF}</Property>
				</Item>
			</Item>
			<Item Name="DigitalInputModule2" Type="Folder">
				<Item Name="24V_Common_Interlocks" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7E8F8D06-BB50-42A9-AE4E-34E52E30ED5D}</Property>
				</Item>
				<Item Name="Device_Allowed" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{83581701-0304-4195-88BE-BD4221BD5A0F}</Property>
				</Item>
				<Item Name="Stable_Beam" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{44614314-BCB7-4EDC-BB50-7957B22BA4AB}</Property>
				</Item>
				<Item Name="Safe_Beam1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{65ABAE8B-1E32-4AFD-867D-428950125068}</Property>
				</Item>
				<Item Name="Safe_Beam2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3C0D7749-2587-4D95-B9F1-29BA7E06EA7B}</Property>
				</Item>
				<Item Name="Post_Mortem" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{444A2713-D2A7-4457-B11B-48F2A155F17C}</Property>
				</Item>
				<Item Name="User_Permit_Cibu_1a" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4844DF8E-24C6-4BD7-BF9D-E80D1866DD54}</Property>
				</Item>
				<Item Name="User_Permit_Cibu_1b" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2C66EE40-92FE-4ABB-8222-892111E5BF0C}</Property>
				</Item>
				<Item Name="Injection_Inhib_Beam1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E76E5997-797D-4590-AA57-B8A380607797}</Property>
				</Item>
				<Item Name="Injection_Inhib_Beam2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{EE27EF1E-BD08-4D9A-9A47-CABACF6E0317}</Property>
				</Item>
				<Item Name="User_Permit_Cibu_2a" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C4E82D36-6F55-4189-A6EB-60DC28B4D69C}</Property>
				</Item>
				<Item Name="User_Permit_Cibu_2b" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8AB615C2-50FC-41FE-A663-92751CF54C3E}</Property>
				</Item>
				<Item Name="All_RP_Home_Beam1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C6996D0C-FE87-4A3A-A974-DEDC37B00DA8}</Property>
				</Item>
				<Item Name="All_RP_Home_Beam2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FB53B742-363B-42E3-968B-0E9A32CD794A}</Property>
				</Item>
				<Item Name="Back_Home" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{392DA4F7-C339-4B43-9E54-7DA8E4B61866}</Property>
				</Item>
				<Item Name="Override" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9215D7ED-79D6-40B4-8F14-140E9C3F8A51}</Property>
				</Item>
				<Item Name="Spare" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DB567744-499F-4C68-ADE3-22D1546C7DF0}</Property>
				</Item>
				<Item Name="24VDC_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D7B2306E-AC60-44CC-B8D7-CBDF80D4577F}</Property>
				</Item>
				<Item Name="12VDC_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CAB89ED3-AC6F-46FC-BB24-62A01CF4F8F2}</Property>
				</Item>
				<Item Name="220VAC_Drive_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F4BFB583-7234-43EE-9509-9E38F5EE7081}</Property>
				</Item>
				<Item Name="M01_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{388B3035-EEF1-4F2A-A940-E2868F5641A7}</Property>
				</Item>
				<Item Name="M01_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI25</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D55769F5-B485-441C-9406-0C2ED387572B}</Property>
				</Item>
				<Item Name="M02_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI26</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D338E7C6-6640-4ED2-A3E7-8908A62A384C}</Property>
				</Item>
				<Item Name="M02_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI27</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6582604C-17B0-4A91-8BD1-D2020B05D49F}</Property>
				</Item>
				<Item Name="M03_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI28</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8F175A95-2AEE-41A4-9215-EBF7FB01C1B1}</Property>
				</Item>
				<Item Name="M03_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI29</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CD14D72B-C8A1-411F-8A2D-221B723D1AC5}</Property>
				</Item>
				<Item Name="M04_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI30</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B029420B-821E-440C-B07E-8F6E8CC21174}</Property>
				</Item>
				<Item Name="M04_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod3/DI31</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{730162A5-78D9-43DB-9E6B-62E3F0462A42}</Property>
				</Item>
			</Item>
			<Item Name="DigitalOutput" Type="Folder">
				<Item Name="Step_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E49B4056-E0CE-404B-9926-2AEB8F582E67}</Property>
				</Item>
				<Item Name="Direction_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{844B3C52-BFCD-4900-9EA9-680B64A59AB9}</Property>
				</Item>
				<Item Name="Disable_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DEA655CF-BBFA-4A4B-B68C-CAF6EDF666B1}</Property>
				</Item>
				<Item Name="Reset_M1" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9D7B79EE-72B0-4C86-A1A1-6034560703B9}</Property>
				</Item>
				<Item Name="Step_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1268E293-2783-4413-86F3-2BC2D49CC32E}</Property>
				</Item>
				<Item Name="Direction_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{92E6117F-A4A4-4FEA-BA60-613692D1E32C}</Property>
				</Item>
				<Item Name="Disable_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{80779440-5723-46D2-835D-E20D940FB6A0}</Property>
				</Item>
				<Item Name="Reset_M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4D8C41E0-CF73-464F-8577-5FBBFD9E2A21}</Property>
				</Item>
				<Item Name="Step_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2F8EFCFE-3325-4519-93BE-61EDAEB57DAE}</Property>
				</Item>
				<Item Name="Direction_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9D6928AD-1DED-4B0B-B393-3E30DFBF5365}</Property>
				</Item>
				<Item Name="Disable_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{87FF4330-4B24-45B7-B055-B36D95251412}</Property>
				</Item>
				<Item Name="Reset_M3" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F20D282E-B3AA-45DC-ACC1-495C472A518D}</Property>
				</Item>
				<Item Name="Step_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F6B6CA56-33EF-426D-85A8-3D52764B7434}</Property>
				</Item>
				<Item Name="Direction_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{66AFE271-2B99-4949-AE23-D44E80D0DEDD}</Property>
				</Item>
				<Item Name="Disable_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0052E434-3443-42D6-8E6F-AF63385896E8}</Property>
				</Item>
				<Item Name="Reset_M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CE1A4DC1-B4BE-4371-B64C-4FCA5B6955F9}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO16" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{29E34220-EE50-443D-A6B7-FAD507C92910}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO17" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{00AD2C0B-3718-4EA4-A551-E98943FFF020}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO18" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C6AA296C-1960-4AB0-93CB-C917EDA685F2}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO19" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{045FE124-D632-443E-BC69-7568E6BE6A2D}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO20" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO20</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FF7F926D-36D5-4B16-B4D8-F6791CEF535C}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO21" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO21</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{928496BD-4554-420E-943A-5F7B3033A764}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO22" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO22</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B62B65D7-FA9C-4ED9-91B1-C5BC8CF0C979}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO23" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO23</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3FD3F2A8-D961-48F8-9CD8-71A2FB69B9E9}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO24" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{59598955-69F8-4250-AF28-5AA462525E93}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO25" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO25</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{87063C83-6142-4C61-ACF1-E7A3B77A9EE2}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO26" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO26</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0EFA0BDF-F66A-45AA-9643-D600633DE9EB}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO27" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO27</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E9A2BA38-A67D-49D0-8B2B-09FAF839F714}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO28" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO28</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BAEF79B6-4AAA-4B7D-AC92-9279F19911AB}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO29" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO29</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FF20E206-0EE3-47BB-ABC3-6B17AACA6073}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO30" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO30</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D7295F2E-B156-4A06-86B9-3B1D22B93256}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO31" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO31</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0122616C-F2C8-47E8-9F8C-DA330DDF5D50}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO7:0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D7F7DB96-F908-4C77-8236-97EB6E1730D6}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO15:8" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2A7CEAA1-C515-4287-9D0B-20CA60599210}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO23:16" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D1E54C25-1187-4BC5-A193-B25E54D30A3C}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO31:24" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{751FA68D-2064-40A4-AC3F-CB6CE37715F3}</Property>
				</Item>
				<Item Name="Chassis1_Mod2/DO31:0" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Chassis1_Mod2/DO31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8DF6F286-FF64-4E04-A2C5-73C9516DEE96}</Property>
				</Item>
			</Item>
			<Item Name="RpStateMachine" Type="Folder">
				<Item Name="M1_RPVTop_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M1_RPVTop_State.lvsc"/>
				<Item Name="M2_RPVBot_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M2_RPVBot_State.lvsc"/>
				<Item Name="M3_RPVTop_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M3_RPVTop_State.lvsc"/>
				<Item Name="M4_RPVBot_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M4_RPVBot_State.lvsc"/>
				<Item Name="M01_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M01_ALFA_RP_State.lvsc"/>
				<Item Name="M02_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M02_ALFA_RP_State.lvsc"/>
				<Item Name="M03_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M03_ALFA_RP_State.lvsc"/>
				<Item Name="M04_ALFA_RP_State.lvsc" Type="LVStatechart" URL="../StateMachineFpga/M04_ALFA_RP_State.lvsc"/>
			</Item>
			<Item Name="ResolverInOut" Type="Folder">
				<Item Name="M1_Resolver_Sin" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{26F74165-8695-45A1-A426-4230026D8A29}</Property>
				</Item>
				<Item Name="M1_Resolver_Cos" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{AF0A5BDF-3F59-47A9-B7CF-7AA74B7D8E0C}</Property>
				</Item>
				<Item Name="M2_Resolver_Sin" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{98EDF2C1-9DF5-4200-BC64-BCC0DE24CC80}</Property>
				</Item>
				<Item Name="M2_Resolver_Cos" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E0EF9574-6EFD-4F86-91A2-2486DD51829B}</Property>
				</Item>
				<Item Name="M3_Resolver_Sin" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1E83B95C-8659-4ED7-80A9-3208095168A2}</Property>
				</Item>
				<Item Name="M3_Resolver_Cos" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A179CD0C-CB14-4E41-883D-640DA8B2D62E}</Property>
				</Item>
				<Item Name="M4_Resolver_Sin" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3A558969-AAB9-4F3E-90B0-0E8DA8FACF37}</Property>
				</Item>
				<Item Name="M4_Resolver_Cos" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{20730AF7-EC19-4D43-931A-74396A97EB3D}</Property>
				</Item>
				<Item Name="M1_Resolver_Exc" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D524A952-5A21-46AE-921F-953855CF72C4}</Property>
				</Item>
				<Item Name="M2_Resolver_Exc" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{33C75ADD-2F03-4EF2-826A-6BDD3D08ED75}</Property>
				</Item>
				<Item Name="M3_Resolver_Exc" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1697C2B6-B88E-4F0A-B7E7-335FDF2E68DA}</Property>
				</Item>
				<Item Name="M4_Resolver_Exc" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4D502716-12D8-4148-9496-1AF7DB65D953}</Property>
				</Item>
			</Item>
			<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{4C64A11E-E1C3-4E75-849B-489B96C5C9AE}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="Chassis1" Type="RIO Expansion Chassis">
				<Property Name="crio.Location" Type="Str">Connector 1</Property>
				<Item Name="Chassis1/Mod1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">0</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9425</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}</Property>
				</Item>
				<Item Name="Chassis1_Mod2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9477</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{317C66D1-AE93-4775-BB0F-5B19DC592B75}</Property>
				</Item>
				<Item Name="Chassis1_Mod3" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Connector 1/Slot 3</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9425</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BFE62D56-CB08-49AE-BD7D-59F705169691}</Property>
				</Item>
			</Item>
			<Item Name="fpga_MotorControl_Totem_7833R.vi" Type="VI" URL="../fpga_MotorControl_Totem_7833R.vi">
				<Property Name="BuildSpec" Type="Str">{9F43DD2D-0EA3-4982-A1B3-5B57627E18EF}</Property>
				<Property Name="configString.guid" Type="Str">{0052E434-3443-42D6-8E6F-AF63385896E8}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{00AD2C0B-3718-4EA4-A551-E98943FFF020}resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=bool{0122616C-F2C8-47E8-9F8C-DA330DDF5D50}resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=bool{0458348C-689B-4895-B640-9B5AE98BBF53}resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=bool{045FE124-D632-443E-BC69-7568E6BE6A2D}resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=bool{071F60D4-1C72-459E-AE78-74876F23F0B1}resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=bool{07BBDBEB-ED66-46B6-A472-9CFB461B9FAD}resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=bool{08B34D4C-71EA-46E2-A319-5924A76F8C76}resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=bool{09CDC6BD-C284-4DFB-BDF2-4F62721AE00C}resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=bool{0A51C8A2-D409-4BE5-9EF6-9BE5CAEA1B34}resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=bool{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0DFB4846-46B5-44F7-9E26-5CA2E8B8C6EF}resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32{0EFA0BDF-F66A-45AA-9643-D600633DE9EB}resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=bool{1268E293-2783-4413-86F3-2BC2D49CC32E}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{1697C2B6-B88E-4F0A-B7E7-335FDF2E68DA}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{1943D65F-FFA6-4980-8468-30C958541A5F}resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=bool{1B7F42DF-D163-40EF-A193-CF300011301B}resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=bool{1E83B95C-8659-4ED7-80A9-3208095168A2}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{20730AF7-EC19-4D43-931A-74396A97EB3D}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{20A11DDE-5A4C-4BFC-ACA0-29F3EDA6392E}resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=bool{22C73DC4-E609-4828-9081-6965C02206A8}resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=bool{26F74165-8695-45A1-A426-4230026D8A29}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{29E34220-EE50-443D-A6B7-FAD507C92910}resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=bool{2A7CEAA1-C515-4287-9D0B-20CA60599210}resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{2C66EE40-92FE-4ABB-8222-892111E5BF0C}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{2F8EFCFE-3325-4519-93BE-61EDAEB57DAE}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{317C66D1-AE93-4775-BB0F-5B19DC592B75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{33C75ADD-2F03-4EF2-826A-6BDD3D08ED75}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{388B3035-EEF1-4F2A-A940-E2868F5641A7}resource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=bool{392DA4F7-C339-4B43-9E54-7DA8E4B61866}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{3A558969-AAB9-4F3E-90B0-0E8DA8FACF37}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{3B28889E-8525-4581-A817-9151DC493D6A}resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=bool{3C0D7749-2587-4D95-B9F1-29BA7E06EA7B}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{3CC65A6A-2AC9-40A4-A1F2-4FA183421DA8}resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=bool{3FD3F2A8-D961-48F8-9CD8-71A2FB69B9E9}resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=bool{444A2713-D2A7-4457-B11B-48F2A155F17C}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{44614314-BCB7-4EDC-BB50-7957B22BA4AB}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{4844DF8E-24C6-4BD7-BF9D-E80D1866DD54}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{4C64A11E-E1C3-4E75-849B-489B96C5C9AE}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{4D502716-12D8-4148-9496-1AF7DB65D953}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{4D8C41E0-CF73-464F-8577-5FBBFD9E2A21}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{59598955-69F8-4250-AF28-5AA462525E93}resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=bool{6582604C-17B0-4A91-8BD1-D2020B05D49F}resource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=bool{65ABAE8B-1E32-4AFD-867D-428950125068}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{66AFE271-2B99-4949-AE23-D44E80D0DEDD}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{6C0212AB-322D-4240-8362-574087962EEB}resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=bool{6C39CF12-9996-495A-B1EB-2CB9E287AA24}resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=bool{6CD4D772-F60F-4A34-B525-D1AEE4A86EE2}resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8{70F032B8-C011-4D09-BA5D-C8F4C0474BEF}resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool{730162A5-78D9-43DB-9E6B-62E3F0462A42}resource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=bool{751FA68D-2064-40A4-AC3F-CB6CE37715F3}resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{775D4A5D-43C5-4CF8-89FB-102C304604D7}resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8{7A316D08-3455-481D-8BF5-AB8DB21144CC}resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=bool{7E8F8D06-BB50-42A9-AE4E-34E52E30ED5D}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{80779440-5723-46D2-835D-E20D940FB6A0}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{828485A5-65B7-40C1-9557-055A6677695E}resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=bool{82FC8550-3CD0-4091-A4F4-E2D5FA67204C}resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=bool{83581701-0304-4195-88BE-BD4221BD5A0F}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{844B3C52-BFCD-4900-9EA9-680B64A59AB9}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{87063C83-6142-4C61-ACF1-E7A3B77A9EE2}resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=bool{87FF4330-4B24-45B7-B055-B36D95251412}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{8AB615C2-50FC-41FE-A663-92751CF54C3E}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{8DF6F286-FF64-4E04-A2C5-73C9516DEE96}resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{8F175A95-2AEE-41A4-9215-EBF7FB01C1B1}resource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=bool{912E884D-332E-4AEE-A6C1-534990DBFA7A}resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8{9215D7ED-79D6-40B4-8F14-140E9C3F8A51}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{928496BD-4554-420E-943A-5F7B3033A764}resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=bool{92A383C7-8BD7-4090-A189-66CE25FA7960}resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool{92E6117F-A4A4-4FEA-BA60-613692D1E32C}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{98EDF2C1-9DF5-4200-BC64-BCC0DE24CC80}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{9AB57D24-A419-4F79-8A95-EEC729420494}resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=bool{9D6928AD-1DED-4B0B-B393-3E30DFBF5365}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{9D7B79EE-72B0-4C86-A1A1-6034560703B9}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{A179CD0C-CB14-4E41-883D-640DA8B2D62E}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{A6D179CA-6C28-4B62-ABE3-940F5C15D969}resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=bool{AAAA3149-F316-41FE-9B7A-F5949AC0531D}resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=bool{AF0A5BDF-3F59-47A9-B7CF-7AA74B7D8E0C}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{B029420B-821E-440C-B07E-8F6E8CC21174}resource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=bool{B3EE8B7B-54A4-4952-B70A-817B64D23E90}resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool{B62B65D7-FA9C-4ED9-91B1-C5BC8CF0C979}resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=bool{BAEF79B6-4AAA-4B7D-AC92-9279F19911AB}resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=bool{BFE62D56-CB08-49AE-BD7D-59F705169691}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C4E82D36-6F55-4189-A6EB-60DC28B4D69C}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{C6996D0C-FE87-4A3A-A974-DEDC37B00DA8}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{C6AA296C-1960-4AB0-93CB-C917EDA685F2}resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=bool{CA9C8584-93E7-4739-930C-F7A86AF39CB2}resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=bool{CAB89ED3-AC6F-46FC-BB24-62A01CF4F8F2}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{CD14D72B-C8A1-411F-8A2D-221B723D1AC5}resource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=bool{CE1A4DC1-B4BE-4371-B64C-4FCA5B6955F9}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{D1E54C25-1187-4BC5-A193-B25E54D30A3C}resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{D23CD8A2-90EB-4863-B741-C806DDA82716}resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=bool{D338E7C6-6640-4ED2-A3E7-8908A62A384C}resource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=bool{D524A952-5A21-46AE-921F-953855CF72C4}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{D55769F5-B485-441C-9406-0C2ED387572B}resource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=bool{D6032CB7-5BB7-4D30-819A-26D08FC5E10A}resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=bool{D7295F2E-B156-4A06-86B9-3B1D22B93256}resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=bool{D7B2306E-AC60-44CC-B8D7-CBDF80D4577F}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool{D7F7DB96-F908-4C77-8236-97EB6E1730D6}resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{DB0FF485-0131-42B3-9C96-77909E3764E9}resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=bool{DB567744-499F-4C68-ADE3-22D1546C7DF0}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{DEA655CF-BBFA-4A4B-B68C-CAF6EDF666B1}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{E0EF9574-6EFD-4F86-91A2-2486DD51829B}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{E251BBBC-0EB7-4862-B0A5-C094723FEDFB}resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool{E49B4056-E0CE-404B-9926-2AEB8F582E67}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{E76E5997-797D-4590-AA57-B8A380607797}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{E9A2BA38-A67D-49D0-8B2B-09FAF839F714}resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=bool{EC4502CB-224E-48D0-B9C3-494A7D0301F5}resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=bool{EDF4D77C-EF48-4041-B3F4-5C9AE5F056CF}resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=bool{EE27EF1E-BD08-4D9A-9A47-CABACF6E0317}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{F20D282E-B3AA-45DC-ACC1-495C472A518D}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{F352E634-3E96-4BBB-AE84-947238E5F9F5}resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=bool{F4BFB583-7234-43EE-9509-9E38F5EE7081}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{F6B6CA56-33EF-426D-85A8-3D52764B7434}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{F895825C-12E3-4D4E-92DF-30A2A6A9F453}resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=bool{FB53B742-363B-42E3-968B-0E9A32CD794A}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{FCE656A9-8DA5-49F1-BE6C-08E02D5E2664}resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8{FF20E206-0EE3-47BB-ABC3-6B17AACA6073}resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=bool{FF7F926D-36D5-4B16-B4D8-F6791CEF535C}resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">12VDC_Fltresource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool220VAC_Drive_Fltresource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool24V_Common_Interlocksresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool24V_Ok_M1resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool24V_Ok_M2resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool24V_Ok_M3resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool24V_Ok_M4resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool24VDC_Fltresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Home_Beam1resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolAll_RP_Home_Beam2resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolAntiColl_M1M2resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=boolAntiColl_M3M4resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=boolBack_Homeresource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolChassis1/Mod1/DI15:8resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8Chassis1/Mod1/DI23:16resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8Chassis1/Mod1/DI30resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=boolChassis1/Mod1/DI31:0resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32Chassis1/Mod1/DI31:24resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8Chassis1/Mod1/DI31resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=boolChassis1/Mod1/DI7:0resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8Chassis1/Mod1[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2/DO15:8resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO16resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO17resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO18resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO19resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO20resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO21resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO22resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO23:16resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO23resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO24resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO25resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO26resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO27resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO28resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO29resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO30resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO31:0resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Chassis1_Mod2/DO31:24resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO31resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO7:0resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M1resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=boolCopy_HomeSw_M2resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=boolCopy_HomeSw_M3resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=boolCopy_HomeSw_M4resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolDirection_M1resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M2resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M3resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M4resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDisable_M1resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolDisable_M2resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolDisable_M3resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolDisable_M4resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolDriver_Flt_M1resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=boolDriver_Flt_M2resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=boolDriver_Flt_M3resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=boolDriver_Flt_M4resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=boolInjection_Inhib_Beam1resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolInjection_Inhib_Beam2resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolM01_LVDT_Fltresource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=boolM01_OutOfLimitsresource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=boolM02_LVDT_Fltresource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=boolM02_OutOfLimitsresource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=boolM03_LVDT_Fltresource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=boolM03_OutOfLimitsresource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=boolM04_LVDT_Fltresource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=boolM04_OutOfLimitsresource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=boolM1_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16M1_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16M1_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16M2_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16M2_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16M2_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16M3_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16M3_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16M3_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16M4_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M4_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16M4_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Overrideresource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPost_Mortemresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M1resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M2resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M3resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M4resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolSafe_Beam1resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSafe_Beam2resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolSpareresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolStep_M1resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M2resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M3resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M4resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M1resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=boolStopper_In_M2resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=boolStopper_In_M3resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=boolStopper_In_M4resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=boolStopper_Out_M1resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=boolStopper_Out_M2resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=boolStopper_Out_M3resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=boolStopper_Out_M4resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=boolSw_In_M1resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=boolSw_In_M2resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolSw_In_M3resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=boolSw_In_M4resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=boolSw_Out_M1resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=boolSw_Out_M2resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=boolSw_Out_M3resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=boolSw_Out_M4resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=boolUser_Permit_Cibu_1aresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_Cibu_1bresource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolUser_Permit_Cibu_2aresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=boolUser_Permit_Cibu_2bresource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_ALFA\FPGA Bitfiles\RPTunnel_MotorCtrlM5-8_fpgaMotorControl_hh6yodolIN8.lvbitx</Property>
			</Item>
			<Item Name="fpga_MotorControl_Totem_7833R_V1.vi" Type="VI" URL="../fpga_MotorControl_Totem_7833R_V1.vi">
				<Property Name="BuildSpec" Type="Str">{96EBD3D3-9E7F-4D70-832B-63F975980B83}</Property>
				<Property Name="configString.guid" Type="Str">{0052E434-3443-42D6-8E6F-AF63385896E8}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{00AD2C0B-3718-4EA4-A551-E98943FFF020}resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=bool{0122616C-F2C8-47E8-9F8C-DA330DDF5D50}resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=bool{0458348C-689B-4895-B640-9B5AE98BBF53}resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=bool{045FE124-D632-443E-BC69-7568E6BE6A2D}resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=bool{071F60D4-1C72-459E-AE78-74876F23F0B1}resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=bool{07BBDBEB-ED66-46B6-A472-9CFB461B9FAD}resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=bool{08B34D4C-71EA-46E2-A319-5924A76F8C76}resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=bool{09CDC6BD-C284-4DFB-BDF2-4F62721AE00C}resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=bool{0A51C8A2-D409-4BE5-9EF6-9BE5CAEA1B34}resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=bool{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0DFB4846-46B5-44F7-9E26-5CA2E8B8C6EF}resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32{0EFA0BDF-F66A-45AA-9643-D600633DE9EB}resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=bool{1268E293-2783-4413-86F3-2BC2D49CC32E}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{1697C2B6-B88E-4F0A-B7E7-335FDF2E68DA}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{1943D65F-FFA6-4980-8468-30C958541A5F}resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=bool{1B7F42DF-D163-40EF-A193-CF300011301B}resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=bool{1E83B95C-8659-4ED7-80A9-3208095168A2}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{20730AF7-EC19-4D43-931A-74396A97EB3D}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{20A11DDE-5A4C-4BFC-ACA0-29F3EDA6392E}resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=bool{22C73DC4-E609-4828-9081-6965C02206A8}resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=bool{26F74165-8695-45A1-A426-4230026D8A29}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{29E34220-EE50-443D-A6B7-FAD507C92910}resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=bool{2A7CEAA1-C515-4287-9D0B-20CA60599210}resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{2C66EE40-92FE-4ABB-8222-892111E5BF0C}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{2F8EFCFE-3325-4519-93BE-61EDAEB57DAE}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{317C66D1-AE93-4775-BB0F-5B19DC592B75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{33C75ADD-2F03-4EF2-826A-6BDD3D08ED75}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{388B3035-EEF1-4F2A-A940-E2868F5641A7}resource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=bool{392DA4F7-C339-4B43-9E54-7DA8E4B61866}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{3A558969-AAB9-4F3E-90B0-0E8DA8FACF37}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{3B28889E-8525-4581-A817-9151DC493D6A}resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=bool{3C0D7749-2587-4D95-B9F1-29BA7E06EA7B}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{3CC65A6A-2AC9-40A4-A1F2-4FA183421DA8}resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=bool{3FD3F2A8-D961-48F8-9CD8-71A2FB69B9E9}resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=bool{444A2713-D2A7-4457-B11B-48F2A155F17C}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{44614314-BCB7-4EDC-BB50-7957B22BA4AB}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{4844DF8E-24C6-4BD7-BF9D-E80D1866DD54}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{4C64A11E-E1C3-4E75-849B-489B96C5C9AE}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{4D502716-12D8-4148-9496-1AF7DB65D953}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{4D8C41E0-CF73-464F-8577-5FBBFD9E2A21}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{59598955-69F8-4250-AF28-5AA462525E93}resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=bool{6582604C-17B0-4A91-8BD1-D2020B05D49F}resource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=bool{65ABAE8B-1E32-4AFD-867D-428950125068}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{66AFE271-2B99-4949-AE23-D44E80D0DEDD}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{6C0212AB-322D-4240-8362-574087962EEB}resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=bool{6C39CF12-9996-495A-B1EB-2CB9E287AA24}resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=bool{6CD4D772-F60F-4A34-B525-D1AEE4A86EE2}resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8{70F032B8-C011-4D09-BA5D-C8F4C0474BEF}resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool{730162A5-78D9-43DB-9E6B-62E3F0462A42}resource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=bool{751FA68D-2064-40A4-AC3F-CB6CE37715F3}resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{775D4A5D-43C5-4CF8-89FB-102C304604D7}resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8{7A316D08-3455-481D-8BF5-AB8DB21144CC}resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=bool{7E8F8D06-BB50-42A9-AE4E-34E52E30ED5D}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{80779440-5723-46D2-835D-E20D940FB6A0}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{828485A5-65B7-40C1-9557-055A6677695E}resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=bool{82FC8550-3CD0-4091-A4F4-E2D5FA67204C}resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=bool{83581701-0304-4195-88BE-BD4221BD5A0F}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{844B3C52-BFCD-4900-9EA9-680B64A59AB9}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{87063C83-6142-4C61-ACF1-E7A3B77A9EE2}resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=bool{87FF4330-4B24-45B7-B055-B36D95251412}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{8AB615C2-50FC-41FE-A663-92751CF54C3E}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{8DF6F286-FF64-4E04-A2C5-73C9516DEE96}resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{8F175A95-2AEE-41A4-9215-EBF7FB01C1B1}resource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=bool{912E884D-332E-4AEE-A6C1-534990DBFA7A}resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8{9215D7ED-79D6-40B4-8F14-140E9C3F8A51}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{928496BD-4554-420E-943A-5F7B3033A764}resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=bool{92A383C7-8BD7-4090-A189-66CE25FA7960}resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool{92E6117F-A4A4-4FEA-BA60-613692D1E32C}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{98EDF2C1-9DF5-4200-BC64-BCC0DE24CC80}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{9AB57D24-A419-4F79-8A95-EEC729420494}resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=bool{9D6928AD-1DED-4B0B-B393-3E30DFBF5365}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{9D7B79EE-72B0-4C86-A1A1-6034560703B9}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{A179CD0C-CB14-4E41-883D-640DA8B2D62E}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{A6D179CA-6C28-4B62-ABE3-940F5C15D969}resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=bool{AAAA3149-F316-41FE-9B7A-F5949AC0531D}resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=bool{AF0A5BDF-3F59-47A9-B7CF-7AA74B7D8E0C}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{B029420B-821E-440C-B07E-8F6E8CC21174}resource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=bool{B3EE8B7B-54A4-4952-B70A-817B64D23E90}resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool{B62B65D7-FA9C-4ED9-91B1-C5BC8CF0C979}resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=bool{BAEF79B6-4AAA-4B7D-AC92-9279F19911AB}resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=bool{BFE62D56-CB08-49AE-BD7D-59F705169691}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C4E82D36-6F55-4189-A6EB-60DC28B4D69C}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{C6996D0C-FE87-4A3A-A974-DEDC37B00DA8}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{C6AA296C-1960-4AB0-93CB-C917EDA685F2}resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=bool{CA9C8584-93E7-4739-930C-F7A86AF39CB2}resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=bool{CAB89ED3-AC6F-46FC-BB24-62A01CF4F8F2}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{CD14D72B-C8A1-411F-8A2D-221B723D1AC5}resource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=bool{CE1A4DC1-B4BE-4371-B64C-4FCA5B6955F9}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{D1E54C25-1187-4BC5-A193-B25E54D30A3C}resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{D23CD8A2-90EB-4863-B741-C806DDA82716}resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=bool{D338E7C6-6640-4ED2-A3E7-8908A62A384C}resource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=bool{D524A952-5A21-46AE-921F-953855CF72C4}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{D55769F5-B485-441C-9406-0C2ED387572B}resource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=bool{D6032CB7-5BB7-4D30-819A-26D08FC5E10A}resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=bool{D7295F2E-B156-4A06-86B9-3B1D22B93256}resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=bool{D7B2306E-AC60-44CC-B8D7-CBDF80D4577F}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool{D7F7DB96-F908-4C77-8236-97EB6E1730D6}resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{DB0FF485-0131-42B3-9C96-77909E3764E9}resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=bool{DB567744-499F-4C68-ADE3-22D1546C7DF0}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{DEA655CF-BBFA-4A4B-B68C-CAF6EDF666B1}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{E0EF9574-6EFD-4F86-91A2-2486DD51829B}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{E251BBBC-0EB7-4862-B0A5-C094723FEDFB}resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool{E49B4056-E0CE-404B-9926-2AEB8F582E67}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{E76E5997-797D-4590-AA57-B8A380607797}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{E9A2BA38-A67D-49D0-8B2B-09FAF839F714}resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=bool{EC4502CB-224E-48D0-B9C3-494A7D0301F5}resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=bool{EDF4D77C-EF48-4041-B3F4-5C9AE5F056CF}resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=bool{EE27EF1E-BD08-4D9A-9A47-CABACF6E0317}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{F20D282E-B3AA-45DC-ACC1-495C472A518D}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{F352E634-3E96-4BBB-AE84-947238E5F9F5}resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=bool{F4BFB583-7234-43EE-9509-9E38F5EE7081}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{F6B6CA56-33EF-426D-85A8-3D52764B7434}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{F895825C-12E3-4D4E-92DF-30A2A6A9F453}resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=bool{FB53B742-363B-42E3-968B-0E9A32CD794A}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{FCE656A9-8DA5-49F1-BE6C-08E02D5E2664}resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8{FF20E206-0EE3-47BB-ABC3-6B17AACA6073}resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=bool{FF7F926D-36D5-4B16-B4D8-F6791CEF535C}resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">12VDC_Fltresource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool220VAC_Drive_Fltresource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool24V_Common_Interlocksresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool24V_Ok_M1resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool24V_Ok_M2resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool24V_Ok_M3resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool24V_Ok_M4resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool24VDC_Fltresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Home_Beam1resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolAll_RP_Home_Beam2resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolAntiColl_M1M2resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=boolAntiColl_M3M4resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=boolBack_Homeresource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolChassis1/Mod1/DI15:8resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8Chassis1/Mod1/DI23:16resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8Chassis1/Mod1/DI30resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=boolChassis1/Mod1/DI31:0resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32Chassis1/Mod1/DI31:24resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8Chassis1/Mod1/DI31resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=boolChassis1/Mod1/DI7:0resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8Chassis1/Mod1[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2/DO15:8resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO16resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO17resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO18resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO19resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO20resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO21resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO22resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO23:16resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO23resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO24resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO25resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO26resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO27resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO28resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO29resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO30resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO31:0resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Chassis1_Mod2/DO31:24resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO31resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO7:0resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M1resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=boolCopy_HomeSw_M2resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=boolCopy_HomeSw_M3resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=boolCopy_HomeSw_M4resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolDirection_M1resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M2resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M3resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M4resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDisable_M1resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolDisable_M2resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolDisable_M3resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolDisable_M4resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolDriver_Flt_M1resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=boolDriver_Flt_M2resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=boolDriver_Flt_M3resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=boolDriver_Flt_M4resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=boolInjection_Inhib_Beam1resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolInjection_Inhib_Beam2resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolM01_LVDT_Fltresource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=boolM01_OutOfLimitsresource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=boolM02_LVDT_Fltresource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=boolM02_OutOfLimitsresource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=boolM03_LVDT_Fltresource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=boolM03_OutOfLimitsresource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=boolM04_LVDT_Fltresource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=boolM04_OutOfLimitsresource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=boolM1_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16M1_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16M1_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16M2_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16M2_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16M2_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16M3_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16M3_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16M3_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16M4_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M4_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16M4_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Overrideresource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPost_Mortemresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M1resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M2resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M3resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M4resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolSafe_Beam1resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSafe_Beam2resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolSpareresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolStep_M1resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M2resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M3resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M4resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M1resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=boolStopper_In_M2resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=boolStopper_In_M3resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=boolStopper_In_M4resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=boolStopper_Out_M1resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=boolStopper_Out_M2resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=boolStopper_Out_M3resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=boolStopper_Out_M4resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=boolSw_In_M1resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=boolSw_In_M2resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolSw_In_M3resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=boolSw_In_M4resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=boolSw_Out_M1resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=boolSw_Out_M2resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=boolSw_Out_M3resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=boolSw_Out_M4resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=boolUser_Permit_Cibu_1aresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_Cibu_1bresource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolUser_Permit_Cibu_2aresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=boolUser_Permit_Cibu_2bresource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_ALFA\FPGA Bitfiles\RPTunnel_MotorCtrlM5-8_fpgaMotorControl_1U8Xe9Vyjqk.lvbitx</Property>
			</Item>
			<Item Name="fpga_MotorControl_Totem_7833R_V2.vi" Type="VI" URL="../fpga_MotorControl_Totem_7833R_V2.vi">
				<Property Name="BuildSpec" Type="Str">{885D379C-C3C5-4637-A470-53D8AD6C2615}</Property>
				<Property Name="configString.guid" Type="Str">{0052E434-3443-42D6-8E6F-AF63385896E8}resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=bool{00AD2C0B-3718-4EA4-A551-E98943FFF020}resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=bool{0122616C-F2C8-47E8-9F8C-DA330DDF5D50}resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=bool{0458348C-689B-4895-B640-9B5AE98BBF53}resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=bool{045FE124-D632-443E-BC69-7568E6BE6A2D}resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=bool{071F60D4-1C72-459E-AE78-74876F23F0B1}resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=bool{07BBDBEB-ED66-46B6-A472-9CFB461B9FAD}resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=bool{08B34D4C-71EA-46E2-A319-5924A76F8C76}resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=bool{09CDC6BD-C284-4DFB-BDF2-4F62721AE00C}resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=bool{0A51C8A2-D409-4BE5-9EF6-9BE5CAEA1B34}resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=bool{0D1B63B2-3A14-4CFA-89B0-C288E41E099C}[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{0DFB4846-46B5-44F7-9E26-5CA2E8B8C6EF}resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32{0EFA0BDF-F66A-45AA-9643-D600633DE9EB}resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=bool{1268E293-2783-4413-86F3-2BC2D49CC32E}resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=bool{1697C2B6-B88E-4F0A-B7E7-335FDF2E68DA}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{1943D65F-FFA6-4980-8468-30C958541A5F}resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=bool{1B7F42DF-D163-40EF-A193-CF300011301B}resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=bool{1E83B95C-8659-4ED7-80A9-3208095168A2}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{20730AF7-EC19-4D43-931A-74396A97EB3D}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{20A11DDE-5A4C-4BFC-ACA0-29F3EDA6392E}resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=bool{22C73DC4-E609-4828-9081-6965C02206A8}resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=bool{26F74165-8695-45A1-A426-4230026D8A29}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{29E34220-EE50-443D-A6B7-FAD507C92910}resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=bool{2A7CEAA1-C515-4287-9D0B-20CA60599210}resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{2C66EE40-92FE-4ABB-8222-892111E5BF0C}resource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=bool{2F8EFCFE-3325-4519-93BE-61EDAEB57DAE}resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=bool{317C66D1-AE93-4775-BB0F-5B19DC592B75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{33C75ADD-2F03-4EF2-826A-6BDD3D08ED75}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{388B3035-EEF1-4F2A-A940-E2868F5641A7}resource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=bool{392DA4F7-C339-4B43-9E54-7DA8E4B61866}resource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=bool{3A558969-AAB9-4F3E-90B0-0E8DA8FACF37}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{3B28889E-8525-4581-A817-9151DC493D6A}resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=bool{3C0D7749-2587-4D95-B9F1-29BA7E06EA7B}resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=bool{3CC65A6A-2AC9-40A4-A1F2-4FA183421DA8}resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=bool{3FD3F2A8-D961-48F8-9CD8-71A2FB69B9E9}resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=bool{444A2713-D2A7-4457-B11B-48F2A155F17C}resource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=bool{44614314-BCB7-4EDC-BB50-7957B22BA4AB}resource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=bool{4844DF8E-24C6-4BD7-BF9D-E80D1866DD54}resource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=bool{4C64A11E-E1C3-4E75-849B-489B96C5C9AE}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{4D502716-12D8-4148-9496-1AF7DB65D953}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{4D8C41E0-CF73-464F-8577-5FBBFD9E2A21}resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=bool{59598955-69F8-4250-AF28-5AA462525E93}resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=bool{6582604C-17B0-4A91-8BD1-D2020B05D49F}resource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=bool{65ABAE8B-1E32-4AFD-867D-428950125068}resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=bool{66AFE271-2B99-4949-AE23-D44E80D0DEDD}resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=bool{6C0212AB-322D-4240-8362-574087962EEB}resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=bool{6C39CF12-9996-495A-B1EB-2CB9E287AA24}resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=bool{6CD4D772-F60F-4A34-B525-D1AEE4A86EE2}resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8{70F032B8-C011-4D09-BA5D-C8F4C0474BEF}resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool{730162A5-78D9-43DB-9E6B-62E3F0462A42}resource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=bool{751FA68D-2064-40A4-AC3F-CB6CE37715F3}resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{775D4A5D-43C5-4CF8-89FB-102C304604D7}resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8{7A316D08-3455-481D-8BF5-AB8DB21144CC}resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=bool{7E8F8D06-BB50-42A9-AE4E-34E52E30ED5D}resource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool{80779440-5723-46D2-835D-E20D940FB6A0}resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=bool{828485A5-65B7-40C1-9557-055A6677695E}resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=bool{82FC8550-3CD0-4091-A4F4-E2D5FA67204C}resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=bool{83581701-0304-4195-88BE-BD4221BD5A0F}resource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=bool{844B3C52-BFCD-4900-9EA9-680B64A59AB9}resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=bool{87063C83-6142-4C61-ACF1-E7A3B77A9EE2}resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=bool{87FF4330-4B24-45B7-B055-B36D95251412}resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=bool{8AB615C2-50FC-41FE-A663-92751CF54C3E}resource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool{8DF6F286-FF64-4E04-A2C5-73C9516DEE96}resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{8F175A95-2AEE-41A4-9215-EBF7FB01C1B1}resource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=bool{912E884D-332E-4AEE-A6C1-534990DBFA7A}resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8{9215D7ED-79D6-40B4-8F14-140E9C3F8A51}resource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=bool{928496BD-4554-420E-943A-5F7B3033A764}resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=bool{92A383C7-8BD7-4090-A189-66CE25FA7960}resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool{92E6117F-A4A4-4FEA-BA60-613692D1E32C}resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=bool{98EDF2C1-9DF5-4200-BC64-BCC0DE24CC80}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{9AB57D24-A419-4F79-8A95-EEC729420494}resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=bool{9D6928AD-1DED-4B0B-B393-3E30DFBF5365}resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=bool{9D7B79EE-72B0-4C86-A1A1-6034560703B9}resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=bool{A179CD0C-CB14-4E41-883D-640DA8B2D62E}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{A6D179CA-6C28-4B62-ABE3-940F5C15D969}resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=bool{AAAA3149-F316-41FE-9B7A-F5949AC0531D}resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=bool{AF0A5BDF-3F59-47A9-B7CF-7AA74B7D8E0C}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{B029420B-821E-440C-B07E-8F6E8CC21174}resource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=bool{B3EE8B7B-54A4-4952-B70A-817B64D23E90}resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool{B62B65D7-FA9C-4ED9-91B1-C5BC8CF0C979}resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=bool{BAEF79B6-4AAA-4B7D-AC92-9279F19911AB}resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=bool{BFE62D56-CB08-49AE-BD7D-59F705169691}[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C4E82D36-6F55-4189-A6EB-60DC28B4D69C}resource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=bool{C6996D0C-FE87-4A3A-A974-DEDC37B00DA8}resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=bool{C6AA296C-1960-4AB0-93CB-C917EDA685F2}resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=bool{CA9C8584-93E7-4739-930C-F7A86AF39CB2}resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=bool{CAB89ED3-AC6F-46FC-BB24-62A01CF4F8F2}resource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool{CD14D72B-C8A1-411F-8A2D-221B723D1AC5}resource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=bool{CE1A4DC1-B4BE-4371-B64C-4FCA5B6955F9}resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=bool{D1E54C25-1187-4BC5-A193-B25E54D30A3C}resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{D23CD8A2-90EB-4863-B741-C806DDA82716}resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=bool{D338E7C6-6640-4ED2-A3E7-8908A62A384C}resource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=bool{D524A952-5A21-46AE-921F-953855CF72C4}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{D55769F5-B485-441C-9406-0C2ED387572B}resource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=bool{D6032CB7-5BB7-4D30-819A-26D08FC5E10A}resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=bool{D7295F2E-B156-4A06-86B9-3B1D22B93256}resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=bool{D7B2306E-AC60-44CC-B8D7-CBDF80D4577F}resource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool{D7F7DB96-F908-4C77-8236-97EB6E1730D6}resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{DB0FF485-0131-42B3-9C96-77909E3764E9}resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=bool{DB567744-499F-4C68-ADE3-22D1546C7DF0}resource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=bool{DEA655CF-BBFA-4A4B-B68C-CAF6EDF666B1}resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=bool{E0EF9574-6EFD-4F86-91A2-2486DD51829B}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{E251BBBC-0EB7-4862-B0A5-C094723FEDFB}resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool{E49B4056-E0CE-404B-9926-2AEB8F582E67}resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=bool{E76E5997-797D-4590-AA57-B8A380607797}resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=bool{E9A2BA38-A67D-49D0-8B2B-09FAF839F714}resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=bool{EC4502CB-224E-48D0-B9C3-494A7D0301F5}resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=bool{EDF4D77C-EF48-4041-B3F4-5C9AE5F056CF}resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=bool{EE27EF1E-BD08-4D9A-9A47-CABACF6E0317}resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=bool{F20D282E-B3AA-45DC-ACC1-495C472A518D}resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=bool{F352E634-3E96-4BBB-AE84-947238E5F9F5}resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=bool{F4BFB583-7234-43EE-9509-9E38F5EE7081}resource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool{F6B6CA56-33EF-426D-85A8-3D52764B7434}resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=bool{F895825C-12E3-4D4E-92DF-30A2A6A9F453}resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=bool{FB53B742-363B-42E3-968B-0E9A32CD794A}resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=bool{FCE656A9-8DA5-49F1-BE6C-08E02D5E2664}resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8{FF20E206-0EE3-47BB-ABC3-6B17AACA6073}resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=bool{FF7F926D-36D5-4B16-B4D8-F6791CEF535C}resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">12VDC_Fltresource=/crio_Chassis1_Mod3/DI18;0;ReadMethodType=bool220VAC_Drive_Fltresource=/crio_Chassis1_Mod3/DI19;0;ReadMethodType=bool24V_Common_Interlocksresource=/crio_Chassis1_Mod3/DI0;0;ReadMethodType=bool24V_Ok_M1resource=/crio_Chassis1\/Mod1/DI4;0;ReadMethodType=bool24V_Ok_M2resource=/crio_Chassis1\/Mod1/DI5;0;ReadMethodType=bool24V_Ok_M3resource=/crio_Chassis1\/Mod1/DI6;0;ReadMethodType=bool24V_Ok_M4resource=/crio_Chassis1\/Mod1/DI7;0;ReadMethodType=bool24VDC_Fltresource=/crio_Chassis1_Mod3/DI17;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;All_RP_Home_Beam1resource=/crio_Chassis1_Mod3/DI12;0;ReadMethodType=boolAll_RP_Home_Beam2resource=/crio_Chassis1_Mod3/DI13;0;ReadMethodType=boolAntiColl_M1M2resource=/crio_Chassis1\/Mod1/DI12;0;ReadMethodType=boolAntiColl_M3M4resource=/crio_Chassis1\/Mod1/DI21;0;ReadMethodType=boolBack_Homeresource=/crio_Chassis1_Mod3/DI14;0;ReadMethodType=boolChassis1/Mod1/DI15:8resource=/crio_Chassis1\/Mod1/DI15:8;0;ReadMethodType=u8Chassis1/Mod1/DI23:16resource=/crio_Chassis1\/Mod1/DI23:16;0;ReadMethodType=u8Chassis1/Mod1/DI30resource=/crio_Chassis1\/Mod1/DI30;0;ReadMethodType=boolChassis1/Mod1/DI31:0resource=/crio_Chassis1\/Mod1/DI31:0;0;ReadMethodType=u32Chassis1/Mod1/DI31:24resource=/crio_Chassis1\/Mod1/DI31:24;0;ReadMethodType=u8Chassis1/Mod1/DI31resource=/crio_Chassis1\/Mod1/DI31;0;ReadMethodType=boolChassis1/Mod1/DI7:0resource=/crio_Chassis1\/Mod1/DI7:0;0;ReadMethodType=u8Chassis1/Mod1[crioConfig.Begin]crio.Calibration=0,crio.Location=Connector 1/Slot 1,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod2/DO15:8resource=/crio_Chassis1_Mod2/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO16resource=/crio_Chassis1_Mod2/DO16;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO17resource=/crio_Chassis1_Mod2/DO17;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO18resource=/crio_Chassis1_Mod2/DO18;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO19resource=/crio_Chassis1_Mod2/DO19;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO20resource=/crio_Chassis1_Mod2/DO20;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO21resource=/crio_Chassis1_Mod2/DO21;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO22resource=/crio_Chassis1_Mod2/DO22;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO23:16resource=/crio_Chassis1_Mod2/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO23resource=/crio_Chassis1_Mod2/DO23;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO24resource=/crio_Chassis1_Mod2/DO24;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO25resource=/crio_Chassis1_Mod2/DO25;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO26resource=/crio_Chassis1_Mod2/DO26;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO27resource=/crio_Chassis1_Mod2/DO27;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO28resource=/crio_Chassis1_Mod2/DO28;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO29resource=/crio_Chassis1_Mod2/DO29;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO30resource=/crio_Chassis1_Mod2/DO30;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO31:0resource=/crio_Chassis1_Mod2/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Chassis1_Mod2/DO31:24resource=/crio_Chassis1_Mod2/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2/DO31resource=/crio_Chassis1_Mod2/DO31;0;ReadMethodType=bool;WriteMethodType=boolChassis1_Mod2/DO7:0resource=/crio_Chassis1_Mod2/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Chassis1_Mod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 2,crio.Type=NI 9477,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Chassis1_Mod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Connector 1/Slot 3,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Copy_HomeSw_M1resource=/crio_Chassis1\/Mod1/DI26;0;ReadMethodType=boolCopy_HomeSw_M2resource=/crio_Chassis1\/Mod1/DI27;0;ReadMethodType=boolCopy_HomeSw_M3resource=/crio_Chassis1\/Mod1/DI28;0;ReadMethodType=boolCopy_HomeSw_M4resource=/crio_Chassis1\/Mod1/DI29;0;ReadMethodType=boolDevice_Allowedresource=/crio_Chassis1_Mod3/DI1;0;ReadMethodType=boolDirection_M1resource=/crio_Chassis1_Mod2/DO1;0;ReadMethodType=bool;WriteMethodType=boolDirection_M2resource=/crio_Chassis1_Mod2/DO5;0;ReadMethodType=bool;WriteMethodType=boolDirection_M3resource=/crio_Chassis1_Mod2/DO9;0;ReadMethodType=bool;WriteMethodType=boolDirection_M4resource=/crio_Chassis1_Mod2/DO13;0;ReadMethodType=bool;WriteMethodType=boolDisable_M1resource=/crio_Chassis1_Mod2/DO2;0;ReadMethodType=bool;WriteMethodType=boolDisable_M2resource=/crio_Chassis1_Mod2/DO6;0;ReadMethodType=bool;WriteMethodType=boolDisable_M3resource=/crio_Chassis1_Mod2/DO10;0;ReadMethodType=bool;WriteMethodType=boolDisable_M4resource=/crio_Chassis1_Mod2/DO14;0;ReadMethodType=bool;WriteMethodType=boolDriver_Flt_M1resource=/crio_Chassis1\/Mod1/DI0;0;ReadMethodType=boolDriver_Flt_M2resource=/crio_Chassis1\/Mod1/DI1;0;ReadMethodType=boolDriver_Flt_M3resource=/crio_Chassis1\/Mod1/DI2;0;ReadMethodType=boolDriver_Flt_M4resource=/crio_Chassis1\/Mod1/DI3;0;ReadMethodType=boolInjection_Inhib_Beam1resource=/crio_Chassis1_Mod3/DI8;0;ReadMethodType=boolInjection_Inhib_Beam2resource=/crio_Chassis1_Mod3/DI9;0;ReadMethodType=boolM01_LVDT_Fltresource=/crio_Chassis1_Mod3/DI25;0;ReadMethodType=boolM01_OutOfLimitsresource=/crio_Chassis1_Mod3/DI24;0;ReadMethodType=boolM02_LVDT_Fltresource=/crio_Chassis1_Mod3/DI27;0;ReadMethodType=boolM02_OutOfLimitsresource=/crio_Chassis1_Mod3/DI26;0;ReadMethodType=boolM03_LVDT_Fltresource=/crio_Chassis1_Mod3/DI29;0;ReadMethodType=boolM03_OutOfLimitsresource=/crio_Chassis1_Mod3/DI28;0;ReadMethodType=boolM04_LVDT_Fltresource=/crio_Chassis1_Mod3/DI31;0;ReadMethodType=boolM04_OutOfLimitsresource=/crio_Chassis1_Mod3/DI30;0;ReadMethodType=boolM1_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16M1_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16M1_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16M2_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16M2_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16M2_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16M3_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16M3_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16M3_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16M4_Resolver_CosArbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M4_Resolver_ExcArbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16M4_Resolver_SinArbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Overrideresource=/crio_Chassis1_Mod3/DI15;0;ReadMethodType=boolPost_Mortemresource=/crio_Chassis1_Mod3/DI5;0;ReadMethodType=boolPXI-7833R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7833RFPGA_TARGET_FAMILYVIRTEX2TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Reset_M1resource=/crio_Chassis1_Mod2/DO3;0;ReadMethodType=bool;WriteMethodType=boolReset_M2resource=/crio_Chassis1_Mod2/DO7;0;ReadMethodType=bool;WriteMethodType=boolReset_M3resource=/crio_Chassis1_Mod2/DO11;0;ReadMethodType=bool;WriteMethodType=boolReset_M4resource=/crio_Chassis1_Mod2/DO15;0;ReadMethodType=bool;WriteMethodType=boolSafe_Beam1resource=/crio_Chassis1_Mod3/DI3;0;ReadMethodType=boolSafe_Beam2resource=/crio_Chassis1_Mod3/DI4;0;ReadMethodType=boolSpareresource=/crio_Chassis1_Mod3/DI16;0;ReadMethodType=boolStable_Beamresource=/crio_Chassis1_Mod3/DI2;0;ReadMethodType=boolStep_M1resource=/crio_Chassis1_Mod2/DO0;0;ReadMethodType=bool;WriteMethodType=boolStep_M2resource=/crio_Chassis1_Mod2/DO4;0;ReadMethodType=bool;WriteMethodType=boolStep_M3resource=/crio_Chassis1_Mod2/DO8;0;ReadMethodType=bool;WriteMethodType=boolStep_M4resource=/crio_Chassis1_Mod2/DO12;0;ReadMethodType=bool;WriteMethodType=boolStopper_In_M1resource=/crio_Chassis1\/Mod1/DI11;0;ReadMethodType=boolStopper_In_M2resource=/crio_Chassis1\/Mod1/DI16;0;ReadMethodType=boolStopper_In_M3resource=/crio_Chassis1\/Mod1/DI20;0;ReadMethodType=boolStopper_In_M4resource=/crio_Chassis1\/Mod1/DI25;0;ReadMethodType=boolStopper_Out_M1resource=/crio_Chassis1\/Mod1/DI10;0;ReadMethodType=boolStopper_Out_M2resource=/crio_Chassis1\/Mod1/DI15;0;ReadMethodType=boolStopper_Out_M3resource=/crio_Chassis1\/Mod1/DI19;0;ReadMethodType=boolStopper_Out_M4resource=/crio_Chassis1\/Mod1/DI24;0;ReadMethodType=boolSw_In_M1resource=/crio_Chassis1\/Mod1/DI9;0;ReadMethodType=boolSw_In_M2resource=/crio_Chassis1\/Mod1/DI14;0;ReadMethodType=boolSw_In_M3resource=/crio_Chassis1\/Mod1/DI18;0;ReadMethodType=boolSw_In_M4resource=/crio_Chassis1\/Mod1/DI23;0;ReadMethodType=boolSw_Out_M1resource=/crio_Chassis1\/Mod1/DI8;0;ReadMethodType=boolSw_Out_M2resource=/crio_Chassis1\/Mod1/DI13;0;ReadMethodType=boolSw_Out_M3resource=/crio_Chassis1\/Mod1/DI17;0;ReadMethodType=boolSw_Out_M4resource=/crio_Chassis1\/Mod1/DI22;0;ReadMethodType=boolUser_Permit_Cibu_1aresource=/crio_Chassis1_Mod3/DI6;0;ReadMethodType=boolUser_Permit_Cibu_1bresource=/crio_Chassis1_Mod3/DI7;0;ReadMethodType=boolUser_Permit_Cibu_2aresource=/crio_Chassis1_Mod3/DI10;0;ReadMethodType=boolUser_Permit_Cibu_2bresource=/crio_Chassis1_Mod3/DI11;0;ReadMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_ALFA\FPGA Bitfiles\RPTunnel_MotorCtrlM5-8_fpgaMotorControl_PYWIX7UD1vg.lvbitx</Property>
			</Item>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="NI_SC_LVSCCommonFiles.lvlib" Type="Library" URL="/&lt;vilib&gt;/Statechart/Common/NI_SC_LVSCCommonFiles.lvlib"/>
					<Item Name="SCRT Dbg Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT Dbg Rtn.vi"/>
					<Item Name="SCRT SDV Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT SDV Rtn.vi"/>
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				</Item>
				<Item Name="instr.lib" Type="Folder">
					<Item Name="niInstr Basic Elements v1 FPGA.lvlib" Type="Library" URL="/&lt;instrlib&gt;/_niInstr/Basic Elements/v1/FPGA/niInstr Basic Elements v1 FPGA.lvlib"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="fpga_MotorControl_Totem_7833R" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_MotorControl_Totem_7833R</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPTunnel_MotorCtrlM5-8_fpgaMotorControl_hh6yodolIN8.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_ALFA/FPGA Bitfiles/RPTunnel_MotorCtrlM5-8_fpgaMotorControl_hh6yodolIN8.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPTunnel_MotorCtrlM5-8_fpgaMotorControl_hh6yodolIN8.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_ALFA/RP_Tunnel.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
					<Property Name="TargetName" Type="Str">MotorCtrl M5-8</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/MotorCtrl M5-8/fpga_MotorControl_Totem_7833R.vi</Property>
				</Item>
				<Item Name="fpga_MotorControl_Totem_7833R_V1" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_MotorControl_Totem_7833R_V1</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPTunnel_MotorCtrlM5-8_fpgaMotorControl_1U8Xe9Vyjqk.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_ALFA/FPGA Bitfiles/RPTunnel_MotorCtrlM5-8_fpgaMotorControl_1U8Xe9Vyjqk.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPTunnel_MotorCtrlM5-8_fpgaMotorControl_1U8Xe9Vyjqk.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_ALFA/RP_Tunnel.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
					<Property Name="TargetName" Type="Str">MotorCtrl M5-8</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/MotorCtrl M5-8/fpga_MotorControl_Totem_7833R_V1.vi</Property>
				</Item>
				<Item Name="fpga_MotorControl_Totem_7833R_V2" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_MotorControl_Totem_7833R_V2</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPTunnel_MotorCtrlM5-8_fpgaMotorControl_PYWIX7UD1vg.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_ALFA/FPGA Bitfiles/RPTunnel_MotorCtrlM5-8_fpgaMotorControl_PYWIX7UD1vg.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPTunnel_MotorCtrlM5-8_fpgaMotorControl_PYWIX7UD1vg.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_ALFA/RP_Tunnel.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
					<Property Name="TargetName" Type="Str">MotorCtrl M5-8</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/MotorCtrl M5-8/fpga_MotorControl_Totem_7833R_V2.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="LVDT M1-4" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{0AEB03DF-C7C2-4A7C-8333-89F7AD58126A}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{143EF291-3332-44BD-991A-41B78CFB75DC}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{171755CA-B77A-4312-9177-1D61ECA42481}Arbitration=AlwaysArbitrate;resource=/Connector0/AO7;0;WriteMethodType=I16{19E1CC5F-3333-45BE-8BA0-6A5F0EC0A118}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{211AE285-9356-4599-BD95-7713AAA4B1B5}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{27F1DDAA-392C-4F1F-B8F4-11A2559CF44D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=bool{283FABE8-0660-4F4F-82A9-D035343BE977}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{2BFC63D8-829A-439E-8655-0A23DAAC647D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{2C8B5E0A-ACD4-440E-9BCD-F5839484628F}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{2E11261B-7239-41F9-9314-BE406B2D3D94}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=bool{2EBE6E82-7521-4DFA-9ABE-C69EEB9699F0}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{34744313-8291-43A1-8514-697C14D92A29}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{3C118F9C-C3F1-47D2-BED7-52A363373D55}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{3D15BD02-0A6E-432D-A453-255766C36107}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{40DB2A1F-444A-4F33-B6B0-A660B7DDE3B9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{4207F879-57CB-47E4-A4B3-E841A018D994}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{44458B63-9891-4725-8DBF-CBBE482CB6FD}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{46358265-F841-479C-801D-23D2D9B7B992}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{46B1B06E-CE3E-4325-BF81-E32FB1C460F0}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=bool{46DE0853-90C8-4E11-94F4-942D61E83E6A}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{4AC92A1E-9879-48F8-A37A-B8E1BA425FE5}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=bool{5A61C25D-C7E2-4805-86D2-05B9C3E6A5E1}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{676E7D29-4A1B-4D2B-8F05-C1DDF5DE1570}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{68AF8D76-DEE7-476B-B7EA-A28672CAC781}Arbitration=AlwaysArbitrate;resource=/Connector0/AO6;0;WriteMethodType=I16{6A7C1EF1-7E97-4DD2-9AC1-45128D0780AC}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{6C8B8A52-36D9-4CA7-8F71-D4994CAD067A}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{743C8F0B-5A75-4274-8561-C95DC804818F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{7519F613-74E1-4C12-8B00-8C6E2511E3C5}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{7676CC6D-16C2-40ED-BEF2-9A73CA0A44A9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{7A190B5E-8975-465A-BA30-DC94553DC03D}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{7D2DE74D-CB9C-41AF-8BAC-EB0D298F2AAB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{8436F5C9-BA93-40BF-AB4A-BBEE3931634C}Arbitration=AlwaysArbitrate;resource=/Connector0/AO4;0;WriteMethodType=I16{84C1750B-6D90-4916-8A7C-073703584E96}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{89C7FD02-83EB-4BCE-9A0D-104175FCB75A}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{8B707E93-7E7A-4418-B39C-CE492557F89E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=bool{8B738CAE-E3BC-4808-B85B-D8835ACAFED8}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{8CB429D0-EE11-401B-9A18-5E83E70E906C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{9A4A740B-1FD9-447C-A25E-BEDF7B4DCB7C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{A5899C51-41A8-4E78-9451-75164EF3451E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{A5F45D0D-B06D-48E7-9371-7F70370D2231}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{B1DC71C9-3E3D-426C-80E1-DFFFBED273DC}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=bool{B5C00AE7-505F-4FCD-9BCB-7278FE85C309}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{BB9B71E2-95AB-420B-B4D8-21425A44CB62}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{BDEC8C28-C3D4-480F-8D27-5BBE1446A047}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{BFA63EDC-7FC9-4D4C-983F-1233D4514216}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{C1450B95-0D24-490B-9713-C26B5E1E1F67}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{C5071E7D-798C-4F69-A0D2-DAECE5E6BD77}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{D54F9DDE-D857-405B-8319-E71A0C9E3A62}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=bool{D5594259-EFF5-49C3-8C54-390BEC1F636C}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{E0CAE7B3-0749-4536-A8A7-873E829D99E1}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{E1277CB6-4E51-49A7-B4F0-CF0CF803A1A8}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=bool{E1430503-C2F2-4835-95F9-BD89889CC819}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{E376EAE5-FBDB-4A81-8CBE-884CDC4484C2}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{E6EF7F4E-CD8E-446E-8168-E0694A8FAEF8}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{EEFF86CC-9410-41C6-86D4-1A53A4FE2EDD}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{EFBF35AB-FA81-4746-A866-EE317509FE3C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{F0DA9F84-B563-45EA-A964-DA70E48C556B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{F34D7CA5-0571-4C70-9BDD-DD21FD9DFB0D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{FADDF37F-B92D-4736-A4A9-93AE4159206F}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{FCDB0841-9F7C-4BD4-88A1-047E9F93B62A}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{FDE5B840-B577-48D0-9C8F-BEB830368F03}Arbitration=AlwaysArbitrate;resource=/Connector0/AO5;0;WriteMethodType=I16PXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Connector0/AO4Arbitration=AlwaysArbitrate;resource=/Connector0/AO4;0;WriteMethodType=I16Connector0/AO5Arbitration=AlwaysArbitrate;resource=/Connector0/AO5;0;WriteMethodType=I16Connector0/AO6Arbitration=AlwaysArbitrate;resource=/Connector0/AO6;0;WriteMethodType=I16Connector0/AO7Arbitration=AlwaysArbitrate;resource=/Connector0/AO7;0;WriteMethodType=I16Connector1/DIO10ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO11ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO12ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO13ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO14ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO15ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO16ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO17ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO18ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO19ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO20ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO21ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO22ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO23ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO24ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO25ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO26ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO27ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO28ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO29ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO30ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO31ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO32ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO33ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO34ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO35ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO36ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO37ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO38ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO39ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=boolInterlock M1M2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolInterlock M3M4ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolLvdt_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16Lvdt_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16Lvdt_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16Lvdt_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16LVDT_M01_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M02_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M03_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M04_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"Lvdt_V1_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16Lvdt_V1_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16Lvdt_V1_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16Lvdt_V1_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Lvdt_V2_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16Lvdt_V2_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16Lvdt_V2_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16Lvdt_V2_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M01_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=boolM01_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolM02_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolM02_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=boolM03_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=boolM03_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolM04_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=boolM04_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">PXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="Resource Name" Type="Str">RIO2</Property>
			<Property Name="Target Class" Type="Str">PXI-7852R</Property>
			<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="Connector1" Type="Folder">
				<Item Name="M01_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7676CC6D-16C2-40ED-BEF2-9A73CA0A44A9}</Property>
				</Item>
				<Item Name="M01_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{40DB2A1F-444A-4F33-B6B0-A660B7DDE3B9}</Property>
				</Item>
				<Item Name="M02_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{EFBF35AB-FA81-4746-A866-EE317509FE3C}</Property>
				</Item>
				<Item Name="M02_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F0DA9F84-B563-45EA-A964-DA70E48C556B}</Property>
				</Item>
				<Item Name="M03_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9A4A740B-1FD9-447C-A25E-BEDF7B4DCB7C}</Property>
				</Item>
				<Item Name="M03_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{46DE0853-90C8-4E11-94F4-942D61E83E6A}</Property>
				</Item>
				<Item Name="M04_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8CB429D0-EE11-401B-9A18-5E83E70E906C}</Property>
				</Item>
				<Item Name="M04_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7519F613-74E1-4C12-8B00-8C6E2511E3C5}</Property>
				</Item>
				<Item Name="Interlock M1M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7D2DE74D-CB9C-41AF-8BAC-EB0D298F2AAB}</Property>
				</Item>
				<Item Name="Interlock M3M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5A61C25D-C7E2-4805-86D2-05B9C3E6A5E1}</Property>
				</Item>
				<Item Name="Connector1/DIO10" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C1450B95-0D24-490B-9713-C26B5E1E1F67}</Property>
				</Item>
				<Item Name="Connector1/DIO11" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B5C00AE7-505F-4FCD-9BCB-7278FE85C309}</Property>
				</Item>
				<Item Name="Connector1/DIO12" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8B738CAE-E3BC-4808-B85B-D8835ACAFED8}</Property>
				</Item>
				<Item Name="Connector1/DIO13" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{34744313-8291-43A1-8514-697C14D92A29}</Property>
				</Item>
				<Item Name="Connector1/DIO14" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E6EF7F4E-CD8E-446E-8168-E0694A8FAEF8}</Property>
				</Item>
				<Item Name="Connector1/DIO15" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{676E7D29-4A1B-4D2B-8F05-C1DDF5DE1570}</Property>
				</Item>
				<Item Name="Connector1/DIO16" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3C118F9C-C3F1-47D2-BED7-52A363373D55}</Property>
				</Item>
				<Item Name="Connector1/DIO17" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C5071E7D-798C-4F69-A0D2-DAECE5E6BD77}</Property>
				</Item>
				<Item Name="Connector1/DIO18" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F34D7CA5-0571-4C70-9BDD-DD21FD9DFB0D}</Property>
				</Item>
				<Item Name="Connector1/DIO19" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E376EAE5-FBDB-4A81-8CBE-884CDC4484C2}</Property>
				</Item>
				<Item Name="Connector1/DIO20" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{0AEB03DF-C7C2-4A7C-8333-89F7AD58126A}</Property>
				</Item>
				<Item Name="Connector1/DIO21" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6A7C1EF1-7E97-4DD2-9AC1-45128D0780AC}</Property>
				</Item>
				<Item Name="Connector1/DIO22" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BDEC8C28-C3D4-480F-8D27-5BBE1446A047}</Property>
				</Item>
				<Item Name="Connector1/DIO23" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{743C8F0B-5A75-4274-8561-C95DC804818F}</Property>
				</Item>
				<Item Name="Connector1/DIO24" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A5899C51-41A8-4E78-9451-75164EF3451E}</Property>
				</Item>
				<Item Name="Connector1/DIO25" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FCDB0841-9F7C-4BD4-88A1-047E9F93B62A}</Property>
				</Item>
				<Item Name="Connector1/DIO26" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BFA63EDC-7FC9-4D4C-983F-1233D4514216}</Property>
				</Item>
				<Item Name="Connector1/DIO27" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO27</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{283FABE8-0660-4F4F-82A9-D035343BE977}</Property>
				</Item>
				<Item Name="Connector1/DIO28" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2BFC63D8-829A-439E-8655-0A23DAAC647D}</Property>
				</Item>
				<Item Name="Connector1/DIO29" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A5F45D0D-B06D-48E7-9371-7F70370D2231}</Property>
				</Item>
				<Item Name="Connector1/DIO30" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{211AE285-9356-4599-BD95-7713AAA4B1B5}</Property>
				</Item>
				<Item Name="Connector1/DIO31" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{89C7FD02-83EB-4BCE-9A0D-104175FCB75A}</Property>
				</Item>
				<Item Name="Connector1/DIO32" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO32</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{27F1DDAA-392C-4F1F-B8F4-11A2559CF44D}</Property>
				</Item>
				<Item Name="Connector1/DIO33" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO33</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D54F9DDE-D857-405B-8319-E71A0C9E3A62}</Property>
				</Item>
				<Item Name="Connector1/DIO34" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO34</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8B707E93-7E7A-4418-B39C-CE492557F89E}</Property>
				</Item>
				<Item Name="Connector1/DIO35" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO35</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E1277CB6-4E51-49A7-B4F0-CF0CF803A1A8}</Property>
				</Item>
				<Item Name="Connector1/DIO36" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO36</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{46B1B06E-CE3E-4325-BF81-E32FB1C460F0}</Property>
				</Item>
				<Item Name="Connector1/DIO37" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO37</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2E11261B-7239-41F9-9314-BE406B2D3D94}</Property>
				</Item>
				<Item Name="Connector1/DIO38" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO38</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B1DC71C9-3E3D-426C-80E1-DFFFBED273DC}</Property>
				</Item>
				<Item Name="Connector1/DIO39" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO39</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4AC92A1E-9879-48F8-A37A-B8E1BA425FE5}</Property>
				</Item>
			</Item>
			<Item Name="Connector0" Type="Folder">
				<Item Name="Lvdt_V1_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FADDF37F-B92D-4736-A4A9-93AE4159206F}</Property>
				</Item>
				<Item Name="Lvdt_V2_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3D15BD02-0A6E-432D-A453-255766C36107}</Property>
				</Item>
				<Item Name="Lvdt_V1_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{143EF291-3332-44BD-991A-41B78CFB75DC}</Property>
				</Item>
				<Item Name="Lvdt_V2_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2C8B5E0A-ACD4-440E-9BCD-F5839484628F}</Property>
				</Item>
				<Item Name="Lvdt_V1_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BB9B71E2-95AB-420B-B4D8-21425A44CB62}</Property>
				</Item>
				<Item Name="Lvdt_V2_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{4207F879-57CB-47E4-A4B3-E841A018D994}</Property>
				</Item>
				<Item Name="Lvdt_V1_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{EEFF86CC-9410-41C6-86D4-1A53A4FE2EDD}</Property>
				</Item>
				<Item Name="Lvdt_V2_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7A190B5E-8975-465A-BA30-DC94553DC03D}</Property>
				</Item>
				<Item Name="Lvdt_Exc_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{44458B63-9891-4725-8DBF-CBBE482CB6FD}</Property>
				</Item>
				<Item Name="Lvdt_Exc_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{84C1750B-6D90-4916-8A7C-073703584E96}</Property>
				</Item>
				<Item Name="Lvdt_Exc_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2EBE6E82-7521-4DFA-9ABE-C69EEB9699F0}</Property>
				</Item>
				<Item Name="Lvdt_Exc_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D5594259-EFF5-49C3-8C54-390BEC1F636C}</Property>
				</Item>
				<Item Name="Connector0/AO4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8436F5C9-BA93-40BF-AB4A-BBEE3931634C}</Property>
				</Item>
				<Item Name="Connector0/AO5" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FDE5B840-B577-48D0-9C8F-BEB830368F03}</Property>
				</Item>
				<Item Name="Connector0/AO6" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{68AF8D76-DEE7-476B-B7EA-A28672CAC781}</Property>
				</Item>
				<Item Name="Connector0/AO7" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{171755CA-B77A-4312-9177-1D61ECA42481}</Property>
				</Item>
			</Item>
			<Item Name="LVDT_M01_Acq" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">100</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">10</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{E0CAE7B3-0749-4536-A8A7-873E829D99E1}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000</Property>
			</Item>
			<Item Name="LVDT_M02_Acq" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">100</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">10</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{6C8B8A52-36D9-4CA7-8F71-D4994CAD067A}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000</Property>
			</Item>
			<Item Name="LVDT_M03_Acq" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">100</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">10</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{E1430503-C2F2-4835-95F9-BD89889CC819}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000</Property>
			</Item>
			<Item Name="LVDT_M04_Acq" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">100</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">10</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{19E1CC5F-3333-45BE-8BA0-6A5F0EC0A118}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000</Property>
			</Item>
			<Item Name="fpga_LVDT_Totem_4_pots.vi" Type="VI" URL="../fpga_LVDT_Totem_4_pots.vi">
				<Property Name="BuildSpec" Type="Str">{C810FE64-DEAA-48F2-957D-53F9BFEBB2B6}</Property>
				<Property Name="configString.guid" Type="Str">{018A1D64-5893-4790-9B15-AC8BBB1D8F02}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{03BF2513-2983-4B72-A3BC-C8636586A49C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{042AD147-5531-430C-8462-54CB39A6C16F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{082854A5-FEF9-4A24-9A46-6657ED7B8085}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{10A669FC-7054-47FC-B973-75636B6BC74D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{11E130C9-F03D-4B69-AB9F-D1789EEA04C9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{143546E4-0FBC-4CA5-BAF4-CFC212CF34F0}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{15F72616-7B71-4AF0-B49C-033FFAC3B0CA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{15FA701C-D44C-460F-913F-64AC3B08E9F9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=bool{16829151-C9C3-4B2A-BDC1-4B24A89AC53A}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{1A0BBE72-C03C-45BA-82F0-9EAC3292833F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{1CDCF97F-1BD0-49E2-8E32-1592AA0B074C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{1F080B89-8ADE-460A-BE7E-2381D6306CB0}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{2AE620A1-853F-4011-9F3D-DA22F1FB7700}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{2E909F24-4987-4429-8000-C7B66DA1B429}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{344372B6-AC8D-4180-A535-3D1976CCAD2E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=bool{349789DF-7D59-4D77-9F83-FDD9C7ED583A}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{373D90E9-80AA-48EE-92F5-874996897ED8}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{37A56C69-B39E-4B5A-BE96-5E3155B14036}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{4593C7E2-493D-465F-B957-8EEB0501494C}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{4DEB99CB-EE71-4A13-8648-B3C6AF45180A}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{52991AF0-6261-403E-81E1-E78361B8AFF4}Arbitration=AlwaysArbitrate;resource=/Connector0/AO7;0;WriteMethodType=I16{555C70A8-9E18-4197-AA8F-E49E8EAFCE21}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=bool{5765BF64-F02E-48A9-9D52-B8AB34AABB3F}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{5A3CE4A0-75B5-4ABF-B56B-09B57FACB55F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=bool{5FF1C677-5FE8-4A4B-B4ED-F2FFAE107BCA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{60F0C5B8-F84E-41DA-B67F-C37A945A6EA7}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{62377FEE-E775-4988-8B77-E76DBFA8A965}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{62B68977-85C7-40D1-AA79-32B61A843140}Arbitration=AlwaysArbitrate;resource=/Connector0/AO5;0;WriteMethodType=I16{63539E6A-36BD-4D9E-AD30-DDED53E2DDE3}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=bool{6E55C2D9-1D4A-4DC2-BAFF-BFF975F1EC4E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{6FB92E42-AFF8-44F5-8735-52D3D89C7A5D}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{710B344F-3A81-4A12-BE57-38DA0ABB52AB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{71E2538B-D3C9-455D-95C5-D81643100DB2}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{73794B0A-A9C7-4E88-863B-086C2EE75E8C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{7B0159FD-784F-4791-8B7F-B37DD30429EA}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{7EAE01FA-24BC-4AC2-947A-6424F9ADF3C3}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{84EE1E1D-D70D-4F50-95DB-FFF62EEB0C7C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{8545072E-98BD-4321-BF06-447FB9ACE860}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{85A32E10-21DE-40BD-88B5-15F4C2ACD836}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{89455E57-2D81-4B43-A760-10043461D236}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=bool{8FFE81AE-5657-4BDC-A2EA-00B4871AF4AE}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{9BD25185-2D21-4587-9675-328178A83753}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{A14B6569-760C-4C60-8251-A17C49A0FC5D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{B5301D95-D1A0-47D7-8DC5-968C916996DA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{BA0C20A9-FF2F-4F00-8386-7CB02EA039C0}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{C8FF9123-12DC-4CB7-9ED0-6EBBEA57EF63}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{CAF4DA7E-750B-4198-9AA4-AE387AB10896}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{D26ABE82-4A67-48CF-94F0-5CF1FD1C00E2}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{D600F749-235F-4796-A880-F23B7E715777}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{D7EB67C6-65E8-4703-9582-1E229A4EDF43}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{DA1EEC5E-7CA8-4529-B085-80C97E94B887}Arbitration=AlwaysArbitrate;resource=/Connector0/AO4;0;WriteMethodType=I16{DACC5E4F-F804-4DB9-89B3-4E936856F8D5}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{E1D57ACF-DAB1-4048-8CB1-847985E37C85}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{E7153430-9D7E-496E-A6EA-E1F2FAB166EE}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=bool{E8F2A2A3-B441-4D3F-ADE7-F9277F941DCB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{F85CC578-ED62-4F41-89D8-4EA87D93769F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{F88435FD-CA52-435C-B6FF-52BC3714F932}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=bool{FCC32C36-6E05-4EBD-9A24-78215D1DF789}Arbitration=AlwaysArbitrate;resource=/Connector0/AO6;0;WriteMethodType=I16{FDFF33E7-F08B-4628-9F57-D42BF002A2CF}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{FF7AB921-2F0F-4A06-94FB-BB1DFD75180B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Connector0/AO4Arbitration=AlwaysArbitrate;resource=/Connector0/AO4;0;WriteMethodType=I16Connector0/AO5Arbitration=AlwaysArbitrate;resource=/Connector0/AO5;0;WriteMethodType=I16Connector0/AO6Arbitration=AlwaysArbitrate;resource=/Connector0/AO6;0;WriteMethodType=I16Connector0/AO7Arbitration=AlwaysArbitrate;resource=/Connector0/AO7;0;WriteMethodType=I16Connector1/DIO10ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO11ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO12ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO13ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO14ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO15ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO16ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO17ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO18ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO19ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO20ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO21ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO22ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO23ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO24ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO25ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO26ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO27ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO28ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO29ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO30ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO31ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO32ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO33ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO34ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO35ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO36ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO37ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO38ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO39ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=boolInterlock M1M2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolInterlock M3M4ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolLvdt_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16Lvdt_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16Lvdt_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16Lvdt_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16LVDT_M01_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M02_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M03_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M04_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"Lvdt_V1_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16Lvdt_V1_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16Lvdt_V1_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16Lvdt_V1_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Lvdt_V2_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16Lvdt_V2_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16Lvdt_V2_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16Lvdt_V2_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M01_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=boolM01_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolM02_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolM02_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=boolM03_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=boolM03_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolM04_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=boolM04_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_ALFA\FPGA Bitfiles\RPTunnel_LVDTM1-4_fpgaLVDTTotem4po_e50MrmDoyMg.lvbitx</Property>
			</Item>
			<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{46358265-F841-479C-801D-23D2D9B7B992}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="IP Builder" Type="IP Builder Target">
				<Item Name="Dependencies" Type="Dependencies"/>
				<Item Name="Build Specifications" Type="Build"/>
			</Item>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="LVFixedPointOverflowPolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointOverflowPolicyTypeDef.ctl"/>
					<Item Name="FxpSim.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/FXPMathLib/sim/FxpSim.dll"/>
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="fpga_LVDT_Totem_4_pots" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_LVDT_Totem_4_pots</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPTunnel_LVDTM1-4_fpgaLVDTTotem4po_e50MrmDoyMg.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_ALFA/FPGA Bitfiles/RPTunnel_LVDTM1-4_fpgaLVDTTotem4po_e50MrmDoyMg.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPTunnel_LVDTM1-4_fpgaLVDTTotem4po_e50MrmDoyMg.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_ALFA/RP_Tunnel.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
					<Property Name="TargetName" Type="Str">LVDT M1-4</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/LVDT M1-4/fpga_LVDT_Totem_4_pots.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="LVDT M5-8" Type="FPGA Target">
			<Property Name="AutoRun" Type="Bool">false</Property>
			<Property Name="configString.guid" Type="Str">{018A1D64-5893-4790-9B15-AC8BBB1D8F02}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{03BF2513-2983-4B72-A3BC-C8636586A49C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{042AD147-5531-430C-8462-54CB39A6C16F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{082854A5-FEF9-4A24-9A46-6657ED7B8085}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{10A669FC-7054-47FC-B973-75636B6BC74D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{11E130C9-F03D-4B69-AB9F-D1789EEA04C9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{143546E4-0FBC-4CA5-BAF4-CFC212CF34F0}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{15F72616-7B71-4AF0-B49C-033FFAC3B0CA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{15FA701C-D44C-460F-913F-64AC3B08E9F9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=bool{16829151-C9C3-4B2A-BDC1-4B24A89AC53A}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{1A0BBE72-C03C-45BA-82F0-9EAC3292833F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{1CDCF97F-1BD0-49E2-8E32-1592AA0B074C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{1F080B89-8ADE-460A-BE7E-2381D6306CB0}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{2E909F24-4987-4429-8000-C7B66DA1B429}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{344372B6-AC8D-4180-A535-3D1976CCAD2E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=bool{349789DF-7D59-4D77-9F83-FDD9C7ED583A}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{373D90E9-80AA-48EE-92F5-874996897ED8}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{37A56C69-B39E-4B5A-BE96-5E3155B14036}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{4593C7E2-493D-465F-B957-8EEB0501494C}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{4DEB99CB-EE71-4A13-8648-B3C6AF45180A}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{52991AF0-6261-403E-81E1-E78361B8AFF4}Arbitration=AlwaysArbitrate;resource=/Connector0/AO7;0;WriteMethodType=I16{555C70A8-9E18-4197-AA8F-E49E8EAFCE21}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=bool{5765BF64-F02E-48A9-9D52-B8AB34AABB3F}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{5A3CE4A0-75B5-4ABF-B56B-09B57FACB55F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=bool{5FF1C677-5FE8-4A4B-B4ED-F2FFAE107BCA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{60F0C5B8-F84E-41DA-B67F-C37A945A6EA7}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{62377FEE-E775-4988-8B77-E76DBFA8A965}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{62B68977-85C7-40D1-AA79-32B61A843140}Arbitration=AlwaysArbitrate;resource=/Connector0/AO5;0;WriteMethodType=I16{63539E6A-36BD-4D9E-AD30-DDED53E2DDE3}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=bool{6E55C2D9-1D4A-4DC2-BAFF-BFF975F1EC4E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{6FB92E42-AFF8-44F5-8735-52D3D89C7A5D}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{710B344F-3A81-4A12-BE57-38DA0ABB52AB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{71E2538B-D3C9-455D-95C5-D81643100DB2}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{73794B0A-A9C7-4E88-863B-086C2EE75E8C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{7B0159FD-784F-4791-8B7F-B37DD30429EA}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{7EAE01FA-24BC-4AC2-947A-6424F9ADF3C3}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{84EE1E1D-D70D-4F50-95DB-FFF62EEB0C7C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{8545072E-98BD-4321-BF06-447FB9ACE860}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{89455E57-2D81-4B43-A760-10043461D236}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=bool{9BD25185-2D21-4587-9675-328178A83753}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{A14B6569-760C-4C60-8251-A17C49A0FC5D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{A81F3C4A-634A-45D4-A87B-2C1C5AFE8557}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{AA191632-4F7A-42F4-A10A-5F35E22BF6D4}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{B5301D95-D1A0-47D7-8DC5-968C916996DA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{BA0C20A9-FF2F-4F00-8386-7CB02EA039C0}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{C8FF9123-12DC-4CB7-9ED0-6EBBEA57EF63}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{CAF4DA7E-750B-4198-9AA4-AE387AB10896}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{D26ABE82-4A67-48CF-94F0-5CF1FD1C00E2}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{D600F749-235F-4796-A880-F23B7E715777}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{D7EB67C6-65E8-4703-9582-1E229A4EDF43}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{DA1EEC5E-7CA8-4529-B085-80C97E94B887}Arbitration=AlwaysArbitrate;resource=/Connector0/AO4;0;WriteMethodType=I16{DACC5E4F-F804-4DB9-89B3-4E936856F8D5}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{E1D57ACF-DAB1-4048-8CB1-847985E37C85}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{E7153430-9D7E-496E-A6EA-E1F2FAB166EE}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=bool{E8F2A2A3-B441-4D3F-ADE7-F9277F941DCB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{EF78D635-8514-4292-9D8E-6ACED1A86784}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{F85CC578-ED62-4F41-89D8-4EA87D93769F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{F88435FD-CA52-435C-B6FF-52BC3714F932}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=bool{FCC32C36-6E05-4EBD-9A24-78215D1DF789}Arbitration=AlwaysArbitrate;resource=/Connector0/AO6;0;WriteMethodType=I16{FDFF33E7-F08B-4628-9F57-D42BF002A2CF}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{FF7AB921-2F0F-4A06-94FB-BB1DFD75180B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Connector0/AO4Arbitration=AlwaysArbitrate;resource=/Connector0/AO4;0;WriteMethodType=I16Connector0/AO5Arbitration=AlwaysArbitrate;resource=/Connector0/AO5;0;WriteMethodType=I16Connector0/AO6Arbitration=AlwaysArbitrate;resource=/Connector0/AO6;0;WriteMethodType=I16Connector0/AO7Arbitration=AlwaysArbitrate;resource=/Connector0/AO7;0;WriteMethodType=I16Connector1/DIO10ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO11ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO12ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO13ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO14ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO15ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO16ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO17ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO18ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO19ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO20ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO21ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO22ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO23ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO24ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO25ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO26ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO27ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO28ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO29ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO30ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO31ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO32ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO33ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO34ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO35ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO36ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO37ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO38ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO39ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=boolInterlock M1M2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolInterlock M3M4ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolLvdt_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16Lvdt_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16Lvdt_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16Lvdt_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16LVDT_M01_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M02_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M03_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M04_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"Lvdt_V1_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16Lvdt_V1_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16Lvdt_V1_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16Lvdt_V1_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Lvdt_V2_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16Lvdt_V2_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16Lvdt_V2_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16Lvdt_V2_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M01_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=boolM01_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolM02_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolM02_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=boolM03_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=boolM03_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolM04_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=boolM04_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
			<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">PXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA</Property>
			<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
			<Property Name="Resource Name" Type="Str">RIO3</Property>
			<Property Name="Target Class" Type="Str">PXI-7852R</Property>
			<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
			<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
			<Item Name="Connector0" Type="Folder">
				<Item Name="Lvdt_V1_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{349789DF-7D59-4D77-9F83-FDD9C7ED583A}</Property>
				</Item>
				<Item Name="Lvdt_V2_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E1D57ACF-DAB1-4048-8CB1-847985E37C85}</Property>
				</Item>
				<Item Name="Lvdt_V1_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{16829151-C9C3-4B2A-BDC1-4B24A89AC53A}</Property>
				</Item>
				<Item Name="Lvdt_V2_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7B0159FD-784F-4791-8B7F-B37DD30429EA}</Property>
				</Item>
				<Item Name="Lvdt_V1_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{CAF4DA7E-750B-4198-9AA4-AE387AB10896}</Property>
				</Item>
				<Item Name="Lvdt_V2_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{373D90E9-80AA-48EE-92F5-874996897ED8}</Property>
				</Item>
				<Item Name="Lvdt_V1_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6FB92E42-AFF8-44F5-8735-52D3D89C7A5D}</Property>
				</Item>
				<Item Name="Lvdt_V2_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AI7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FDFF33E7-F08B-4628-9F57-D42BF002A2CF}</Property>
				</Item>
				<Item Name="Lvdt_Exc_M01" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{143546E4-0FBC-4CA5-BAF4-CFC212CF34F0}</Property>
				</Item>
				<Item Name="Lvdt_Exc_M02" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{37A56C69-B39E-4B5A-BE96-5E3155B14036}</Property>
				</Item>
				<Item Name="Lvdt_Exc_M03" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7EAE01FA-24BC-4AC2-947A-6424F9ADF3C3}</Property>
				</Item>
				<Item Name="Lvdt_Exc_M04" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5765BF64-F02E-48A9-9D52-B8AB34AABB3F}</Property>
				</Item>
				<Item Name="Connector0/AO4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DA1EEC5E-7CA8-4529-B085-80C97E94B887}</Property>
				</Item>
				<Item Name="Connector0/AO5" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{62B68977-85C7-40D1-AA79-32B61A843140}</Property>
				</Item>
				<Item Name="Connector0/AO6" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FCC32C36-6E05-4EBD-9A24-78215D1DF789}</Property>
				</Item>
				<Item Name="Connector0/AO7" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="Arbitration">
   <Value>AlwaysArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector0/AO7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{52991AF0-6261-403E-81E1-E78361B8AFF4}</Property>
				</Item>
			</Item>
			<Item Name="Connector1" Type="Folder">
				<Item Name="M01_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{DACC5E4F-F804-4DB9-89B3-4E936856F8D5}</Property>
				</Item>
				<Item Name="M01_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F85CC578-ED62-4F41-89D8-4EA87D93769F}</Property>
				</Item>
				<Item Name="M02_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5FF1C677-5FE8-4A4B-B4ED-F2FFAE107BCA}</Property>
				</Item>
				<Item Name="M02_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{710B344F-3A81-4A12-BE57-38DA0ABB52AB}</Property>
				</Item>
				<Item Name="M03_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{FF7AB921-2F0F-4A06-94FB-BB1DFD75180B}</Property>
				</Item>
				<Item Name="M03_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2E909F24-4987-4429-8000-C7B66DA1B429}</Property>
				</Item>
				<Item Name="M04_OutOfLimits" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{6E55C2D9-1D4A-4DC2-BAFF-BFF975F1EC4E}</Property>
				</Item>
				<Item Name="M04_LVDT_Flt" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E8F2A2A3-B441-4D3F-ADE7-F9277F941DCB}</Property>
				</Item>
				<Item Name="Interlock M1M2" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{71E2538B-D3C9-455D-95C5-D81643100DB2}</Property>
				</Item>
				<Item Name="Interlock M3M4" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1CDCF97F-1BD0-49E2-8E32-1592AA0B074C}</Property>
				</Item>
				<Item Name="Connector1/DIO10" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D600F749-235F-4796-A880-F23B7E715777}</Property>
				</Item>
				<Item Name="Connector1/DIO11" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{042AD147-5531-430C-8462-54CB39A6C16F}</Property>
				</Item>
				<Item Name="Connector1/DIO12" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{BA0C20A9-FF2F-4F00-8386-7CB02EA039C0}</Property>
				</Item>
				<Item Name="Connector1/DIO13" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1A0BBE72-C03C-45BA-82F0-9EAC3292833F}</Property>
				</Item>
				<Item Name="Connector1/DIO14" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{62377FEE-E775-4988-8B77-E76DBFA8A965}</Property>
				</Item>
				<Item Name="Connector1/DIO15" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{84EE1E1D-D70D-4F50-95DB-FFF62EEB0C7C}</Property>
				</Item>
				<Item Name="Connector1/DIO16" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{A14B6569-760C-4C60-8251-A17C49A0FC5D}</Property>
				</Item>
				<Item Name="Connector1/DIO17" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{73794B0A-A9C7-4E88-863B-086C2EE75E8C}</Property>
				</Item>
				<Item Name="Connector1/DIO18" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D7EB67C6-65E8-4703-9582-1E229A4EDF43}</Property>
				</Item>
				<Item Name="Connector1/DIO19" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C8FF9123-12DC-4CB7-9ED0-6EBBEA57EF63}</Property>
				</Item>
				<Item Name="Connector1/DIO20" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{03BF2513-2983-4B72-A3BC-C8636586A49C}</Property>
				</Item>
				<Item Name="Connector1/DIO21" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{1F080B89-8ADE-460A-BE7E-2381D6306CB0}</Property>
				</Item>
				<Item Name="Connector1/DIO22" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{15F72616-7B71-4AF0-B49C-033FFAC3B0CA}</Property>
				</Item>
				<Item Name="Connector1/DIO23" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9BD25185-2D21-4587-9675-328178A83753}</Property>
				</Item>
				<Item Name="Connector1/DIO24" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{082854A5-FEF9-4A24-9A46-6657ED7B8085}</Property>
				</Item>
				<Item Name="Connector1/DIO25" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{11E130C9-F03D-4B69-AB9F-D1789EEA04C9}</Property>
				</Item>
				<Item Name="Connector1/DIO26" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{018A1D64-5893-4790-9B15-AC8BBB1D8F02}</Property>
				</Item>
				<Item Name="Connector1/DIO27" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO27</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{B5301D95-D1A0-47D7-8DC5-968C916996DA}</Property>
				</Item>
				<Item Name="Connector1/DIO28" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{60F0C5B8-F84E-41DA-B67F-C37A945A6EA7}</Property>
				</Item>
				<Item Name="Connector1/DIO29" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{D26ABE82-4A67-48CF-94F0-5CF1FD1C00E2}</Property>
				</Item>
				<Item Name="Connector1/DIO30" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{8545072E-98BD-4321-BF06-447FB9ACE860}</Property>
				</Item>
				<Item Name="Connector1/DIO31" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{10A669FC-7054-47FC-B973-75636B6BC74D}</Property>
				</Item>
				<Item Name="Connector1/DIO32" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO32</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{89455E57-2D81-4B43-A760-10043461D236}</Property>
				</Item>
				<Item Name="Connector1/DIO33" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO33</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{63539E6A-36BD-4D9E-AD30-DDED53E2DDE3}</Property>
				</Item>
				<Item Name="Connector1/DIO34" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO34</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{15FA701C-D44C-460F-913F-64AC3B08E9F9}</Property>
				</Item>
				<Item Name="Connector1/DIO35" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO35</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5A3CE4A0-75B5-4ABF-B56B-09B57FACB55F}</Property>
				</Item>
				<Item Name="Connector1/DIO36" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO36</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{F88435FD-CA52-435C-B6FF-52BC3714F932}</Property>
				</Item>
				<Item Name="Connector1/DIO37" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO37</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{555C70A8-9E18-4197-AA8F-E49E8EAFCE21}</Property>
				</Item>
				<Item Name="Connector1/DIO38" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO38</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{E7153430-9D7E-496E-A6EA-E1F2FAB166EE}</Property>
				</Item>
				<Item Name="Connector1/DIO39" Type="Elemental IO">
					<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="ArbitrationForOutputEnable">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputEnable">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Connector1/DIO39</Value>
   </Attribute>
</AttributeSet>
</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{344372B6-AC8D-4180-A535-3D1976CCAD2E}</Property>
				</Item>
			</Item>
			<Item Name="LVDT_M01_Acq" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">100</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">10</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{AA191632-4F7A-42F4-A10A-5F35E22BF6D4}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000</Property>
			</Item>
			<Item Name="LVDT_M02_Acq" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">100</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">10</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{A81F3C4A-634A-45D4-A87B-2C1C5AFE8557}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000</Property>
			</Item>
			<Item Name="LVDT_M03_Acq" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">100</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">10</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{EF78D635-8514-4292-9D8E-6ACED1A86784}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000</Property>
			</Item>
			<Item Name="LVDT_M04_Acq" Type="FPGA FIFO">
				<Property Name="Actual Number of Elements" Type="UInt">100</Property>
				<Property Name="Arbitration for Read" Type="UInt">1</Property>
				<Property Name="Arbitration for Write" Type="UInt">1</Property>
				<Property Name="Control Logic" Type="UInt">0</Property>
				<Property Name="Data Type" Type="UInt">10</Property>
				<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
				<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="fifo.configured" Type="Bool">true</Property>
				<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
				<Property Name="fifo.valid" Type="Bool">true</Property>
				<Property Name="fifo.version" Type="Int">12</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{4DEB99CB-EE71-4A13-8648-B3C6AF45180A}</Property>
				<Property Name="Local" Type="Bool">false</Property>
				<Property Name="Memory Type" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
				<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
				<Property Name="Requested Number of Elements" Type="UInt">100</Property>
				<Property Name="Type" Type="UInt">0</Property>
				<Property Name="Type Descriptor" Type="Str">10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000</Property>
			</Item>
			<Item Name="fpga_LVDT_Totem_4_pots.vi" Type="VI" URL="../fpga_LVDT_Totem_4_pots.vi">
				<Property Name="BuildSpec" Type="Str">{1F029B5E-9FB3-4808-8C21-38FE2A8EE9DE}</Property>
				<Property Name="configString.guid" Type="Str">{018A1D64-5893-4790-9B15-AC8BBB1D8F02}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{03BF2513-2983-4B72-A3BC-C8636586A49C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{042AD147-5531-430C-8462-54CB39A6C16F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{082854A5-FEF9-4A24-9A46-6657ED7B8085}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{10A669FC-7054-47FC-B973-75636B6BC74D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{11E130C9-F03D-4B69-AB9F-D1789EEA04C9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{143546E4-0FBC-4CA5-BAF4-CFC212CF34F0}Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16{15F72616-7B71-4AF0-B49C-033FFAC3B0CA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{15FA701C-D44C-460F-913F-64AC3B08E9F9}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=bool{16829151-C9C3-4B2A-BDC1-4B24A89AC53A}Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16{1A0BBE72-C03C-45BA-82F0-9EAC3292833F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{1CDCF97F-1BD0-49E2-8E32-1592AA0B074C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{1F080B89-8ADE-460A-BE7E-2381D6306CB0}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{2E909F24-4987-4429-8000-C7B66DA1B429}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{344372B6-AC8D-4180-A535-3D1976CCAD2E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=bool{349789DF-7D59-4D77-9F83-FDD9C7ED583A}Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16{373D90E9-80AA-48EE-92F5-874996897ED8}Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16{37A56C69-B39E-4B5A-BE96-5E3155B14036}Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16{4593C7E2-493D-465F-B957-8EEB0501494C}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{4DEB99CB-EE71-4A13-8648-B3C6AF45180A}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{52991AF0-6261-403E-81E1-E78361B8AFF4}Arbitration=AlwaysArbitrate;resource=/Connector0/AO7;0;WriteMethodType=I16{555C70A8-9E18-4197-AA8F-E49E8EAFCE21}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=bool{5765BF64-F02E-48A9-9D52-B8AB34AABB3F}Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16{5A3CE4A0-75B5-4ABF-B56B-09B57FACB55F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=bool{5FF1C677-5FE8-4A4B-B4ED-F2FFAE107BCA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{60F0C5B8-F84E-41DA-B67F-C37A945A6EA7}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{62377FEE-E775-4988-8B77-E76DBFA8A965}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{62B68977-85C7-40D1-AA79-32B61A843140}Arbitration=AlwaysArbitrate;resource=/Connector0/AO5;0;WriteMethodType=I16{63539E6A-36BD-4D9E-AD30-DDED53E2DDE3}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=bool{6E55C2D9-1D4A-4DC2-BAFF-BFF975F1EC4E}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{6FB92E42-AFF8-44F5-8735-52D3D89C7A5D}Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16{710B344F-3A81-4A12-BE57-38DA0ABB52AB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{71E2538B-D3C9-455D-95C5-D81643100DB2}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{73794B0A-A9C7-4E88-863B-086C2EE75E8C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{7B0159FD-784F-4791-8B7F-B37DD30429EA}Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16{7EAE01FA-24BC-4AC2-947A-6424F9ADF3C3}Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16{84EE1E1D-D70D-4F50-95DB-FFF62EEB0C7C}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{8545072E-98BD-4321-BF06-447FB9ACE860}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{89455E57-2D81-4B43-A760-10043461D236}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=bool{9BD25185-2D21-4587-9675-328178A83753}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{A14B6569-760C-4C60-8251-A17C49A0FC5D}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{A81F3C4A-634A-45D4-A87B-2C1C5AFE8557}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{AA191632-4F7A-42F4-A10A-5F35E22BF6D4}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{B5301D95-D1A0-47D7-8DC5-968C916996DA}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{BA0C20A9-FF2F-4F00-8386-7CB02EA039C0}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{C8FF9123-12DC-4CB7-9ED0-6EBBEA57EF63}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{CAF4DA7E-750B-4198-9AA4-AE387AB10896}Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16{D26ABE82-4A67-48CF-94F0-5CF1FD1C00E2}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{D600F749-235F-4796-A880-F23B7E715777}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{D7EB67C6-65E8-4703-9582-1E229A4EDF43}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{DA1EEC5E-7CA8-4529-B085-80C97E94B887}Arbitration=AlwaysArbitrate;resource=/Connector0/AO4;0;WriteMethodType=I16{DACC5E4F-F804-4DB9-89B3-4E936856F8D5}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{E1D57ACF-DAB1-4048-8CB1-847985E37C85}Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16{E7153430-9D7E-496E-A6EA-E1F2FAB166EE}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=bool{E8F2A2A3-B441-4D3F-ADE7-F9277F941DCB}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{EF78D635-8514-4292-9D8E-6ACED1A86784}"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"{F85CC578-ED62-4F41-89D8-4EA87D93769F}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{F88435FD-CA52-435C-B6FF-52BC3714F932}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=bool{FCC32C36-6E05-4EBD-9A24-78215D1DF789}Arbitration=AlwaysArbitrate;resource=/Connector0/AO6;0;WriteMethodType=I16{FDFF33E7-F08B-4628-9F57-D42BF002A2CF}Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16{FF7AB921-2F0F-4A06-94FB-BB1DFD75180B}ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Connector0/AO4Arbitration=AlwaysArbitrate;resource=/Connector0/AO4;0;WriteMethodType=I16Connector0/AO5Arbitration=AlwaysArbitrate;resource=/Connector0/AO5;0;WriteMethodType=I16Connector0/AO6Arbitration=AlwaysArbitrate;resource=/Connector0/AO6;0;WriteMethodType=I16Connector0/AO7Arbitration=AlwaysArbitrate;resource=/Connector0/AO7;0;WriteMethodType=I16Connector1/DIO10ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO10;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO11ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO11;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO12ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO12;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO13ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO13;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO14ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO14;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO15ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO15;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO16ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO16;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO17ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO17;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO18ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO18;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO19ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO19;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO20ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO20;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO21ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO21;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO22ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO22;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO23ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO23;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO24ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO24;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO25ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO25;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO26ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO26;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO27ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO27;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO28ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO28;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO29ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO29;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO30ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO30;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO31ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO31;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO32ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO32;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO33ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO33;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO34ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO34;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO35ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO35;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO36ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO36;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO37ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO37;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO38ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO38;0;ReadMethodType=bool;WriteMethodType=boolConnector1/DIO39ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO39;0;ReadMethodType=bool;WriteMethodType=boolInterlock M1M2ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolInterlock M3M4ArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolLvdt_Exc_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AO0;0;WriteMethodType=I16Lvdt_Exc_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AO1;0;WriteMethodType=I16Lvdt_Exc_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AO2;0;WriteMethodType=I16Lvdt_Exc_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AO3;0;WriteMethodType=I16LVDT_M01_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M02_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M03_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"LVDT_M04_Acq"ControlLogic=0;NumberOfElements=100;Type=0;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=1;;DataType=10008000000000030009400200025631000009400200025632000012405000020000000107436C7573746572000100020000000000000000;DisableOnOverflowUnderflow=FALSE"Lvdt_V1_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI0;0;ReadMethodType=I16Lvdt_V1_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI2;0;ReadMethodType=I16Lvdt_V1_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI4;0;ReadMethodType=I16Lvdt_V1_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI6;0;ReadMethodType=I16Lvdt_V2_M01Arbitration=AlwaysArbitrate;resource=/Connector0/AI1;0;ReadMethodType=I16Lvdt_V2_M02Arbitration=AlwaysArbitrate;resource=/Connector0/AI3;0;ReadMethodType=I16Lvdt_V2_M03Arbitration=AlwaysArbitrate;resource=/Connector0/AI5;0;ReadMethodType=I16Lvdt_V2_M04Arbitration=AlwaysArbitrate;resource=/Connector0/AI7;0;ReadMethodType=I16M01_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO1;0;ReadMethodType=bool;WriteMethodType=boolM01_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolM02_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolM02_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO2;0;ReadMethodType=bool;WriteMethodType=boolM03_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO5;0;ReadMethodType=bool;WriteMethodType=boolM03_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolM04_LVDT_FltArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO7;0;ReadMethodType=bool;WriteMethodType=boolM04_OutOfLimitsArbitrationForOutputData=NeverArbitrate;ArbitrationForOutputEnable=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForOutputEnable=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/Connector1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolPXI-7852R/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSPXI_7852RFPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\_ALFA\FPGA Bitfiles\RPTunnel_LVDTM5-8_fpgaLVDTTotem4po_6LL5KF+Vbe0.lvbitx</Property>
			</Item>
			<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
				<Property Name="FPGA.PersistentID" Type="Str">{4593C7E2-493D-465F-B957-8EEB0501494C}</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000.000000;MaxFreq=40000000.000000;VariableFreq=0;NomFreq=40000000.000000;PeakPeriodJitter=250.000000;MinDutyCycle=50.000000;MaxDutyCycle=50.000000;Accuracy=100.000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
				<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
				<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
			</Item>
			<Item Name="IP Builder" Type="IP Builder Target">
				<Item Name="Dependencies" Type="Dependencies"/>
				<Item Name="Build Specifications" Type="Build"/>
			</Item>
			<Item Name="Dependencies" Type="Dependencies">
				<Item Name="vi.lib" Type="Folder">
					<Item Name="FxpSim.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/FXPMathLib/sim/FxpSim.dll"/>
					<Item Name="LVFixedPointOverflowPolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointOverflowPolicyTypeDef.ctl"/>
					<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				</Item>
			</Item>
			<Item Name="Build Specifications" Type="Build">
				<Item Name="fpga_LVDT_Totem_4_pots" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
					<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
					<Property Name="BuildSpecDecription" Type="Str"></Property>
					<Property Name="BuildSpecName" Type="Str">fpga_LVDT_Totem_4_pots</Property>
					<Property Name="Comp.BitfileName" Type="Str">RPTunnel_LVDTM5-8_fpgaLVDTTotem4po_6LL5KF+Vbe0.lvbitx</Property>
					<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
					<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
					<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
					<Property Name="Comp.Version.Build" Type="Int">0</Property>
					<Property Name="Comp.Version.Fix" Type="Int">0</Property>
					<Property Name="Comp.Version.Major" Type="Int">1</Property>
					<Property Name="Comp.Version.Minor" Type="Int">0</Property>
					<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
					<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
					<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
					<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
					<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
					<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
					<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
					<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
					<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
					<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
					<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
					<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/_ALFA/FPGA Bitfiles/RPTunnel_LVDTM5-8_fpgaLVDTTotem4po_6LL5KF+Vbe0.lvbitx</Property>
					<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/RPTunnel_LVDTM5-8_fpgaLVDTTotem4po_6LL5KF+Vbe0.lvbitx</Property>
					<Property Name="ProjectPath" Type="Path">/C/_ALFA/RP_Tunnel.lvproj</Property>
					<Property Name="RelativePath" Type="Bool">true</Property>
					<Property Name="RunWhenLoaded" Type="Bool">false</Property>
					<Property Name="SupportDownload" Type="Bool">true</Property>
					<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
					<Property Name="TargetName" Type="Str">LVDT M5-8</Property>
					<Property Name="TopLevelVI" Type="Ref">/RT PXI Target/LVDT M5-8/fpga_LVDT_Totem_4_pots.vi</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="test-rtexe.vi" Type="VI" URL="../test/test-rtexe.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="VISA Flush IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Flush IO Buffer Mask.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_Real-Time Target Support.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI_Real-Time Target Support.lvlib"/>
				<Item Name="ni_emb.dll" Type="Document" URL="/&lt;vilib&gt;/ni_emb.dll"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
			</Item>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Motor_Drive.lvlib" Type="Library" URL="../lib/Motor_Drive/Motor_Drive.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="ALFAexec" Type="{69A947D5-514E-4E75-818E-69657C0547D8}">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{7AFAA77D-5149-45BD-99A2-0B39E32C6BFF}</Property>
				<Property Name="App_INI_GUID" Type="Str">{4CA15B74-6077-418B-9A6A-2107C61314F4}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.cern.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{426EB17F-FF9E-43AA-8FB5-D4B9B824126F}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">ALFAexec</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/NI_AB_TARGETNAME/ALFAexec</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{EC81D3F5-92CF-44FC-8DDA-38D98C260547}</Property>
				<Property Name="Bld_targetDestDir" Type="Path">/c/ni-rt/startup</Property>
				<Property Name="Bld_version.build" Type="Int">10</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">ALFA_RPCS.rtexe</Property>
				<Property Name="Destination[0].path" Type="Path">/c/ni-rt/startup/ALFA_RPCS.rtexe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/c/ni-rt/startup/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">system</Property>
				<Property Name="Destination[2].path" Type="Path">/c/ni-rt/system</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].itemID" Type="Str">{297518E4-32EC-4716-895D-B4F92BECFE5C}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/RT PXI Target/Totem_MotorCtrl_RT.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/RT PXI Target/lib/DIM/lib/totem_lib_DIM.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/RT PXI Target/lib/Motor_Drive/Motor_Drive_RP.lvlib</Property>
				<Property Name="Source[3].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/RT PXI Target/lib/DIM/DLL/dim.dll</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/RT PXI Target/lib/DIM/DLL/TOTEM_DIM.dll</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[6].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[6].itemID" Type="Ref">/RT PXI Target/lib</Property>
				<Property Name="Source[6].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">7</Property>
				<Property Name="TgtF_companyName" Type="Str">cern</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">ALFAexec</Property>
				<Property Name="TgtF_internalName" Type="Str">ALFAexec</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 cern</Property>
				<Property Name="TgtF_productName" Type="Str">ALFAexec</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{D3605D44-A28A-415A-AA02-E7715743574E}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">ALFA_RPCS.rtexe</Property>
			</Item>
			<Item Name="rtexe-test" Type="{69A947D5-514E-4E75-818E-69657C0547D8}">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{170C5791-1C89-4464-A8D7-0C5FA39B42F3}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A2847E0F-012F-4A0A-8FCC-25BCD788DEED}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.description" Type="Str">http://www.cern.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{96BEE354-2659-40B8-A90F-9434549BAF5B}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">rtexe-test</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/NI_AB_TARGETNAME/rtexe-test</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{1432D9C2-14BA-40F1-B4C7-72EE91A4EF05}</Property>
				<Property Name="Bld_targetDestDir" Type="Path">/c/ni-rt/startup</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">rtexe-test.rtexe</Property>
				<Property Name="Destination[0].path" Type="Path">/c/ni-rt/startup/rtexe-test.rtexe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/c/ni-rt/startup/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{2D90436E-7179-410C-A6D8-F9135C605D5D}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/RT PXI Target/test-rtexe.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">cern</Property>
				<Property Name="TgtF_fileDescription" Type="Str">rtexe-test</Property>
				<Property Name="TgtF_internalName" Type="Str">rtexe-test</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 cern</Property>
				<Property Name="TgtF_productName" Type="Str">rtexe-test</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{728CC75E-A5AB-4D0E-8684-98656C763F6F}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">rtexe-test.rtexe</Property>
			</Item>
		</Item>
	</Item>
</Project>

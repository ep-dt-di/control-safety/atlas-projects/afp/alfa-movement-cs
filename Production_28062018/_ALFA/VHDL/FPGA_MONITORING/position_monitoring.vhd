----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:41:57 03/06/2007 
-- Design Name: 
-- Module Name:    position_minitoring - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity position_monitoring is
    Port ( clk 					: in  	STD_LOGIC;
           enable_in 			: in  	STD_LOGIC;
           reset 					: in  	STD_LOGIC;
			  step					: in  	STD_LOGIC;
			  resolver_strobe		: in  	STD_LOGIC;
			  step_lost_ack		: in  	STD_LOGIC;
			  loop_periode			: in  	STD_LOGIC_VECTOR (31 downto 0);
           controller_pos_in 	: in  	STD_LOGIC_VECTOR (31 downto 0);
           resolver_pos_in 	: in  	STD_LOGIC_VECTOR (31 downto 0);
			  threshold				: in		STD_LOGIC_VECTOR (7 downto 0);
           enable_out 			: out  	STD_LOGIC;
			  start_reading		: out  	STD_LOGIC;
			  new_data				: out  	STD_LOGIC;
			  step_lost				: out  	STD_LOGIC;
			  pos_diff				: out		STD_LOGIC_VECTOR (31 downto 0)
			 );
end position_monitoring;

architecture Behavioral of position_monitoring is

signal new_reading_cycle	: STD_LOGIC := '0';
signal main_loop_cnt			: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal start_reading_sig	: STD_LOGIC := '0';
signal new_data_sig			: STD_LOGIC := '0';
signal delay_cnt 				: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal position_diff			: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal position_diff_n		: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal position_err			: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal measure_in_progress	: STD_LOGIC := '0';
signal step_lost_sig			: STD_LOGIC := '0';
signal strobe_delayed		: STD_LOGIC := '0';
signal strobe_rising			: STD_LOGIC := '0';
signal step_delayed			: STD_LOGIC := '0';
signal step_rising			: STD_LOGIC := '0';
signal step_lost_ack_sig	: STD_LOGIC := '0';
signal pos_diff_sig_out		: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');

begin

--position error evaluation
position_diff <= controller_pos_in - resolver_pos_in;
position_diff_n <= (not position_diff)+1;
position_err <= position_diff when position_diff(31)='0' else position_diff_n;

process (clk) begin
	if clk'event and clk='1' then
		-- TODO: saturation of loop periode at about 1.5ms
		if main_loop_cnt>=loop_periode or step_rising='1' then
			main_loop_cnt <= (others=>'0');
			start_reading_sig <= '1';
			measure_in_progress <= '1';
			new_data_sig <= '0';
		else
			main_loop_cnt <= main_loop_cnt + '1';
		end if;
		if step_lost_ack = '1' then
			step_lost_ack_sig <= '1';
		end if;
		if measure_in_progress='1' and strobe_rising='1' then
			new_data_sig <= '1';
			start_reading_sig <= '0';
			measure_in_progress <= '0';
			--step lost signal generation
			if position_err >= threshold then
				step_lost_sig <= '1';
				pos_diff_sig_out <= position_diff;
			elsif step_lost_ack_sig = '1' then
				step_lost_sig <= '0';
				step_lost_ack_sig <= '0';
				pos_diff_sig_out <= (others => '0');
			end if;
		end if;
		if strobe_delayed='0' and resolver_strobe='1' then
			strobe_rising <= '1';
		else
			strobe_rising <= '0';
		end if;
		strobe_delayed <= resolver_strobe;
		if step_delayed='0' and step='1' then
			step_rising <= '1';
		else
			step_rising <= '0';
		end if;
		step_delayed <= step;
		
	end if;
end process;


enable_out 			<= enable_in;
start_reading 		<= start_reading_sig;
new_data 			<= new_data_sig;
step_lost 			<= step_lost_sig;
pos_diff				<= pos_diff_sig_out;

end Behavioral;


----------------------------------------------------------------------------------
-- Company: 		CERN
-- Engineer: 		Arnaud BRIELMANN AB-ATB-LPE
-- 
-- Create Date:   	17:37:09 01/18/2007 
-- Design Name: 		resolver
-- Module Name:    	resolver - Behavioral 
-- Project Name: 		resdec
-- Target Devices: 	xc2v3000-4fg676 on NI PXI-7833R board 
-- Tool versions: 	ISE & Core Generator v8.2.03i
-- Description: 		Process the position relative to the reference
--
-- Dependencies: 		input_filters.vhd
--							fir_ls_bp_180_fir_compiler_v2_0_xst_1_mac_fir_v5_1_xst.edn
--							angle_logic.vhd
--							translate_scaled.ngc
--							angle_glitch_filter.vhd
--							shift_register_1bit.vhd
--							output_register.vhd
--							abs2rel.vhd
--							rad2step.vhd
--							resolver.ucf
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity abs2rel is
    Port ( 	clk 			: in  STD_LOGIC;
				reset			: in  STD_LOGIC;
				sample_clk 	: in  STD_LOGIC;
				enable_in 	: in  STD_LOGIC;
				enable_out 	: out  STD_LOGIC;
				strobe_in 	: in  STD_LOGIC;
				new_ref 		: in  STD_LOGIC;
				data_in 		: in  STD_LOGIC_VECTOR (31 downto 0);
				reference	: in  STD_LOGIC_VECTOR (31 downto 0);
				strobe_out 	: out  STD_LOGIC;
				data_out 	: out  STD_LOGIC_VECTOR (31 downto 0));
end abs2rel;

architecture Behavioral of abs2rel is

signal strobe_sig		: STD_LOGIC := '0';
signal reference_sig	: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal actual_value	: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal relative_pos	: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
	

begin

actual_value <= data_in;

process (sample_clk) begin
	if sample_clk'event and sample_clk='1' then
		if reset='1' then
			reference_sig <= (others=>'0');
		elsif new_ref='1' then
			reference_sig <= actual_value - reference;
		end if;	
		if strobe_in='1' then
			relative_pos <= actual_value - reference_sig;
		end if;
		strobe_sig <= strobe_in;
	end if;
end process;

data_out <= relative_pos;
strobe_out <= strobe_sig;
enable_out <= enable_in;
	
end Behavioral;

